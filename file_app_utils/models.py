import json
import os
import shutil
import zipfile

from rocrate.rocrate import ROCrate
import uuid
from django.db import models


class AbstractFileConnection(models.Model):
    connection_uuid = models.CharField(max_length=150, default=uuid.uuid4)

    class Meta:
        abstract = True

    def __str__(self):
        return self.file_path

    def load_files(self):
        FileIndexer.indexFolder(self.connection_uuid, self.file_path)

    def read_file(self, object_id, clean=False):
        return FileIndexer.objects.get(connection_uuid=self.connection_uuid, pk=object_id).read_file(clean)


    def write_file(self, object_id, new_content):
        return FileIndexer.objects.get(connection_uuid=self.connection_uuid, pk=object_id).write_file(new_content)

    def all_files_of_model(self, model):
        for fi in FileIndexer.objects.filter(connection_uuid=self.connection_uuid, model=model):
            yield fi

    def get_models(self):
        return FileIndexer.objects.filter().order_by('model').values_list('model').distinct()

    @property
    def file_path(self):
        return os.path.join(os.path.dirname(__file__), 'repos', str(self.connection_uuid))



class FileIndexer(models.Model):
    file_path = models.CharField(max_length=255)
    path_extension = models.CharField(max_length=255, default=None, null=True)
    model = models.CharField(max_length=255)
    connection_uuid = models.CharField(max_length=150, default='')
    last_modified = models.FloatField(default='0')
    zip_path = models.CharField(max_length=255, default='')
    crate_path = models.CharField(max_length=255, default='')
    class Meta:
        unique_together = ('file_path', 'connection_uuid', 'model', 'path_extension')

    @classmethod
    def _read_ro_crate(cls, path_to_zip_file, working_dir):
        os.makedirs(working_dir, exist_ok=True)
        with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
            shutil.rmtree(working_dir, ignore_errors=True)
            zip_ref.extractall(working_dir)
        ro_path = None
        if not os.path.exists(os.path.join(working_dir, 'ro-crate-metadata.json')):
            for file in os.listdir(working_dir):
                supdir = os.path.join(working_dir, file, 'ro-crate-metadata.json')
                if os.path.exists(supdir):
                    ro_path = os.path.join(working_dir, file)
        else:
            ro_path = working_dir
        if ro_path is not None:
            return ROCrate(ro_path)
        return None

    @classmethod
    def indexFolder(cls, uuid_str: str, file_path: str):
        workin_dir = os.path.join(os.path.dirname(__file__), 'wkd', str(uuid_str))
        shutil.rmtree(workin_dir, ignore_errors=True)
        os.makedirs(workin_dir)

        for root, dirs, files in os.walk(file_path):
            git_root = str(os.path.relpath(root, file_path))
            for file in files:
                total_file_path = os.path.join(root, file)
                crate_file_path = os.path.join(workin_dir, git_root, file)
                if file.endswith('.eln') or file.endswith('.zip'):
                    crate = cls._read_ro_crate(total_file_path, crate_file_path)
                    if crate is None:
                        continue
                    for e in crate.get_entities():
                        if e.type == 'File' and e.source.suffix == '.json':
                            try:
                                with open(e.source, 'r', encoding='utf8') as f:
                                    data = json.loads(f.read())
                                    if not isinstance(data, dict):
                                        continue

                                    model_type = e.get('context', data.get('@type', data.get('type')))
                                    if model_type is not None:
                                        obj, c = cls.objects.get_or_create(file_path=str(e.source),
                                                                  path_extension=e.id,
                                                                  model=model_type,
                                                                  connection_uuid=uuid_str)
                                        obj.last_modified = os.stat(total_file_path).st_mtime
                                        obj.zip_path = total_file_path
                                        obj.crate_path = crate_file_path
                                        obj.save()
                            except:
                                pass


        all_indexer = cls.objects.filter(connection_uuid=uuid_str)
        for fi in all_indexer:
            if not os.path.exists(os.path.join(fi.file_path)):
                fi.delete()

    def read_file(self, clean=False):
        file_path = self.file_path

        if file_path.endswith('.json'):
            with open(file_path, 'r') as f:
                as_json = json.loads(f.read())
        else:
            raise ValueError('File not supported!')
        if not clean:
            as_json['id'] = self.pk
            as_json['last_update'] = self.last_modified
        return as_json

    def write_file(self, new_content):
        with open(self.file_path, 'w+') as f:
            f.write(json.dumps(new_content, indent=4))


        def zipdir(path, ziph):
            # ziph is zipfile handle
            for root, dirs, files in os.walk(path):
                for file in files:
                    ziph.write(os.path.join(root, file),
                               os.path.relpath(str(os.path.join(root, file)), path))

        with zipfile.ZipFile(self.zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
            zipdir(self.crate_path, zipf)
