Getting Started
===============

In the following we will show you how to host your own instance of the ELNdataBridge and set it up for a simple synchronization.

How to host it
--------------

Hosting an instance of ELNdataBridge is easy thanks to the provided Docker image. Below we present a Docker Compose file that should enable installation in a few steps.

.. code-block:: yaml
   :caption: docker-compose.yml
   :linenos:

   services:
     db:
       image: postgres:17
       restart: unless-stopped
       hostname: db
       expose:
         - 5432
       ports:
         - ${DJANGO_POSTGRES_PORT:-65432}:5432
       environment:
         - POSTGRES_USER=${DJANGO_POSTGRES_USER}
         - POSTGRES_PASSWORD=${DJANGO_POSTGRES_PASSWORD}
         - POSTGRES_DB=${DJANGO_POSTGRES_NAME}
       env_file:
         - ./.env
       volumes:
         - bridge_db:/var/lib/postgresql/data/
     web:
       image: mstarman/elndatabridge:0.3.25
       depends_on:
         - db
       env_file:
         - ./.env
       ports:
         - ${DJANGO_PORT:-3211}:80
   volumes:
     bridge_db:

And the necessary .env file:

.. code-block:: ini
   :caption: .env
   :linenos:

    MODE=prod
    ALLOWED_HOST=http://URL_TO_ELNdataBridge # HOST url the ELNdataBridge from outside the local host
    DJANGO_SUPERUSER_USERNAME=sample # Creates an admin user
    DJANGO_SUPERUSER_EMAIL=mr.sample@kit.edu # E-mail for the admin user
    DJANGO_SUPERUSER_PASSWORD=1234qweR! # Password for the admin user
    DJANGO_POSTGRES_NAME=bridge
    DJANGO_POSTGRES_USER=postgres
    DJANGO_POSTGRES_PASSWORD=afj134uddfGGfQA*WS24D! # IMPORTANT! generate new Password
    DJANGO_PORT=3211 # Port the ELNdataBridge can be reached

In order to host an instance, it is necessary to adjust only the variables contained within the .env file. Furthermore, it is essential to copy the two files, docker-compose.yml and .env, to the same folder of the host system. The instance can then be started using the command docker compose up. However, it is advisable to employ general measures in order to ensure the security of the hosting. For example, an HTTP web server such as nginx should be used.

Set up your first Sync Obj
--------------------------