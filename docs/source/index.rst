.. ELNdataBridge documentation master file, created by
   sphinx-quickstart on Wed Feb 12 12:38:47 2025.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ELNdataBridge documentation
===========================

Electronic Lab Notebooks (ELNs) have become an integral component of contemporary research laboratories, offering capabilities such as data management, collaboration, and the documentation of scientific experiments. However, the proliferation of diverse ELN platforms poses challenges for researchers who require seamless data exchange between different systems.In this paper, we present ELNdataBridge, a novel server-based solution designed to address this challenge by providing a flexible adapter for interfacing and synchronising data between disparate ELN platforms. ELNdataBridge employs Python APIs to interact with the underlying data structures of various ELN systems, enabling seamless transfer of information between them.The system offers a user-friendly interface that allows researchers to map and configure the transfer of single values and entry types between different ELNs, thereby facilitating interoperability and data exchange. The developed software has been shown to be both suitable and efficient, as demonstrated by a first demonstrator that enabled the exchange of data from Chemotion ELN and Herbie, and thereby the connection of information with a focus on chemistry and materials sciences.Scientific contribution: To the best of our knowledge, a method enabling the interoperable exchange of information between different ELNs, as described here, has not yet been reported. In light of the burgeoning number of scientists who are turning to ELNs and their concomitant reliance on discipline-specific platforms, this work proffers a solution to the prevailing limitations pertaining to ELN interoperability.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
