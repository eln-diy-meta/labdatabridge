# ELNdataBridge

Electronic Lab Notebooks (ELNs) have become indispensable tools for modern research laboratories, facilitating data management, collaboration, and documentation of scientific experiments. However, the proliferation of diverse ELN platforms poses challenges for researchers who need to seamlessly exchange data between different systems. In this paper, we present ELNdataBridge, a novel server-based solution designed to address this challenge by providing a flexible adapter for interfacing and synchronising data between disparate ELN platforms. ELNdataBridge leverages Python APIs to interact with the underlying data structures of various ELN systems, enabling smooth transfer of information between them. The system offers a user-friendly interface that allows researchers to map and configure the transfer of single values and entry types between different ELNs, thereby facilitating interoperability and data exchange.


## Workflow for Registering a New Platform

To ensure a smooth setup and registration of a new platform, follow the detailed steps outlined below. This guide will walk you through cloning the repository, setting up the necessary environments, and running the required commands.


1. Clone the Git Repository
   Begin by frog the ELNdataBridge repository. Then clone the forged repository containing the codebase. Open your terminal and run the following command, replacing <repository_url> with the actual URL of the Git repository:
   
   ```shell
   git clone <repository_url>
   cd <repository_name>   
   ```
   
   This will download the repository to your local machine and navigate into the project's directory.

2. Install Poetry
   Poetry is a tool for dependency management and packaging in Python. It simplifies the process of managing project dependencies and virtual environments.
   Install Poetry by running the following command:

   ```shell
   curl -sSL https://install.python-poetry.org | python3 -
   ```
   
   After installation, make sure Poetry is available in your system's PATH. You may need to restart your terminal or follow any additional instructions provided by the Poetry installer.
3. Activate a New Virtual Environment with Poetry
   Navigate to the directory of your cloned repository if you're not already there. Activate a new virtual environment using Poetry with the following commands:
   
   ```shell
   poetry shell
   ```  
    
   This command creates and activates a virtual environment specific to your project, ensuring that dependencies are isolated from your system's Python environment.

4. Install Node.js and Yarn
   Node.js and Yarn are required for managing JavaScript dependencies. Follow these steps to install them:
   Install Node.js: Download and install Node.js from the official website or use a version manager like nvm (Node Version Manager).
   Install Yarn: Yarn can be installed globally using npm, which is included with Node.js.
   
   ```shell
   npm install -g yarn
   ```
   
5. Install Python Dependencies with Poetry
   With your virtual environment activated, install the Python dependencies specified in the pyproject.toml file. Run the following command:
   

   ```shell
   poetry install
   ```

   This command reads the pyproject.toml file and installs all required dependencies for the project.
6. Install Node Packages
   Next, install the project's JavaScript dependencies using Yarn. Run the following command in your project directory:

   ```shell
   yarn install
   ``` 

   This command reads the package.json file and installs all necessary Node.js packages.
7. Load Initial Data
   Load the initial data required by your platform using Django's manage.py command. Ensure the chemotion_herbie_eln_type.json file is located in the appropriate directory within your project. Adjust the path if necessary.

   ```shell
   python manage.py loaddata chemotion_herbie_eln_type.json
   ```

   This command populates your database with the initial data necessary for the platform to function correctly.
8. Register the New Platform
   Finally, run the provided Django management command to register the new platform. This step completes the setup process.

   ```shell
   $ python manage.py add_new_platform
   The platform name you would like to add [as CamelCase]: ElnX
   /TO/PROJECT/labdatabridge/ElnAdapter/settings.py:44
   -> ToDo: FIRST add eln_x_app to the installed apps
   /TO/PROJECT/labdatabridge/eln_x_app/models.py:26
   -> ToDo: Check the properties you need to to establish a connection to ElnX
   /TO/PROJECT/labdatabridge/eln_x_app/forms.py:27
   -> ToDo: Implement a connection test
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:13
   -> ToDo: Establish Connection to ElnX
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:24
   -> ToDo: Return a list of all synchronizable models
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:39
   -> ToDo: Add load a list of all objects of given type
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:54
   -> ToDo: Load a single object and parse it to a simple JSON format
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:69
   -> ToDo: Load all objects and parse them to a simple JSON format
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:82
   -> ToDo: Save changes to ElnX
   /TO/PROJECT/labdatabridge/eln_x_app/sync_manager/eln_x.py:100
   -> ToDo: Create a new entry of a given type in ElnX
   ```

   This command creates a new Django app with all needed Source-Code elements  to register a new Platform. After all files have been created, all to dos to have a minimal integration of the platform are listed in the terminal output with links to the line of code to be edited.

   ```
   eln_x_app
   ├──Assets
   │   ├──tests
   │   │   └──eln_x_app.test.js
   │   └──src
   │         ├──eln_x_app
   │         │   └──controller
   │         │         └──settings_eln_x
   │         │               ├──settings_eln_x.scss
   │         │               ├──settings_eln_x.js
   │         │               └──settings_eln_x.html
   │         ├──…
   ├──templates
   │   ├──eln_x_app
   │         ├──sdc
   │         │   └──settings_eln_x.html
   │         └──models
   │               ├──…
   ├──sync_manager
   │   ├──__init__.py
   │   └──eln_x.py <- ToDos
   ├──admin.py
   ├──apps.py
   ├──tests.py
   ├──__init__.py
   ├──sdc_urls.py
   ├──models.py <- ToDos
   ├──views.py
   ├──forms.py <- ToDos
   └──sdc_views.py
   ```

   Most of the to dos refer to the Platform api translator (in the example above /sync_manager/eln_x.py). In models.py and forms.py, however, the connection and authentication to the platform instance are created by the user. To do this, the properties of the model must be edited and the form must be extended by a validation. The following diagram illustrates the architecture of the system based on its DB schema and explains how the model is included in the system. In the schema only herbie and chemotion platform are included.

![database schema](Assets/static/img/Data%20bank%20schema.png)