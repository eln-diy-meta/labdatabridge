from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models
from sync_tools.models import AbstractConnection

from main_app.models import ElnConnection


class ChemotionSettings(models.Model, SdcModel):
    edit_form = "chemotion_app.forms.ChemotionSettingsForm"
    create_form = "chemotion_app.forms.ChemotionSettingsForm"
    html_list_template = "chemotion_app/models/ChemotionSettings/ChemotionSettings_list.html"
    html_detail_template = "chemotion_app/models/ChemotionSettings/ChemotionSettings_details.html"

    uuid = models.CharField(max_length=255, default='0', editable=False)
    collection = models.CharField(max_length=255)
    collection_id = models.IntegerField(null=True)
    is_sync = models.BooleanField(default=False)

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        if self.uuid == '0':
            self.uuid = self.connection.uuid
        super().save(force_insert, force_update, using, update_fields)


    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


class ChemotionConnectionSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = "Id"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class ChemotionConnection(AbstractConnection, SdcModel):
    user = models.CharField(max_length=255, null=True)
    token = models.CharField(max_length=255, null=True)
    url = models.URLField(verbose_name="URL", null=True)

    chemotion_settings = models.OneToOneField(ChemotionSettings, null=True, on_delete=models.CASCADE,
                                              related_name='connection')

    def __str__(self):
        return self.url

    edit_form = "chemotion_app.forms.ChemotionConnectionForm"
    create_form = "chemotion_app.forms.ChemotionConnectionForm"
    html_list_template = "chemotion_app/models/ChemotionConnection/ChemotionConnection_list.html"
    html_detail_template = "chemotion_app/models/ChemotionConnection/ChemotionConnection_details.html"

    def save(self, *args, **kwargs):
        if self.is_connected and self.chemotion_settings is None:
            self.chemotion_settings = ChemotionSettings.objects.create(uuid=self.uuid)
        super().save(*args, **kwargs)


    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = ChemotionConnectionSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=2)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()