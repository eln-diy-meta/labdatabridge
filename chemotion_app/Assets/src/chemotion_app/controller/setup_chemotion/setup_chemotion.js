import {AbstractSDC, app} from 'sdc_client';


class SetupChemotionController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/chemotion_app/setup_chemotion/%(uuid)s"; //<setup-chemotion data-uuid=""></setup-chemotion>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(uuid,pos) {
        this.settings_model = this.newModel('ChemotionSettings', {uuid: uuid});
        this.connection_model = this.newModel('ChemotionConnection', {uuid: uuid, connection_position:pos});
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return this.settings_model.load().then(() => {
            this.select_row();
            this.find('.connection-form').append(this.connection_model.editForm(-1, () => {
                let sync_settings_pk = this.connection_model.values.sync_settings;
                this.generalSyncOptions = this.newModel('GeneralSyncOptions', {pk: sync_settings_pk});
                this.find('.sync-setting-form').append(this.generalSyncOptions.editForm(-1, () => {
                    this.update_settings_form();
                }));

                return super.willShow();
            }));
        });
    }

    onRefresh() {
        return super.onRefresh();
    }

    update_settings_form() {
        if (this.find('#id_sync_all_existing')[0].checked) {
            this.find('.form-group.id_datetime_threshold').hide();
        } else {
            this.find('.form-group.id_datetime_threshold').show();
        }
    }

    select_collection($row) {
        this.settings_model.values.collection = $row.data('name')
        this.settings_model.values.collection_id = $row.data('pk')
        this.settings_model.values.is_sync = $row.data('synced')
        this.settings_model.save();
        this.select_row();
    }

    select_row() {
        this.find('.collection-row').removeClass('active');
        this.find(`.collection-row-${this.settings_model.values.collection_id}`).addClass('active');
    }

    update_connection_check_results($form) {
        if (this.connection_model.values['is_connected']) {
            $form.parent().addClass('is-connected');
        } else {
            $form.parent().removeClass('is-connected');
        }
    }

    submitModelForm($form, e) {
        let self = this;
        return super.defaultSubmitModelForm($form, e).then(function () {
            self.update_connection_check_results($form);
            self.update_settings_form();
        }).catch(() => {
            self.update_connection_check_results($form);
            self.update_settings_form();
        });
    }

    onChange($input) {
        $input.closest('form').submit();
    }

}

app.register(SetupChemotionController).addMixin('sdc-update-on-change');