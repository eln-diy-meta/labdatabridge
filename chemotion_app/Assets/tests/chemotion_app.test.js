/**
 * @jest-environment jsdom
 */

import {test_utils} from "sdc_client";
import '../src/chemotion_app/chemotion_app.organizer.js'
import '../../../Assets/__tests__/src/sdc_tools/sdc_tools.organizer.js'

describe('SetupChemotion', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('setup-chemotion', {'uuid': 1},
           '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
        expect(controller.contentUrl).toBe('/sdc_view/chemotion_app/setup_chemotion/1');
    });

});