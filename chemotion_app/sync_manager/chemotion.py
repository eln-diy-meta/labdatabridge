import base64
from datetime import datetime, date
from typing import Callable

import chemotion_api
from chemotion_api.elements import AbstractElement
from chemotion_api.elements.reaction import MaterialList

from chemotion_app.models import ChemotionConnection
from sync_tools.abstract_manager import AbstractManager
from sync_tools.options import ELN_TYPE


class EmptyPlaceholderElement():

    def __init__(self):
        self.id = None

    def save(self):
        pass

    def split(self):
        return self


class ChemotionSyncer(AbstractManager):

    def __init__(self, project_uuid: str, eln_a_or_b: ELN_TYPE):
        super().__init__(project_uuid, eln_a_or_b)
        self._setup(ChemotionConnection.objects.get(uuid=project_uuid, connection_position=eln_a_or_b))

    def syncable_models(self) -> list[tuple[str, str]]:
        return [(x, x) for x in self.instance.get_all_types()]

    def _setup(self, eln_connection: ChemotionConnection):
        self.instance = chemotion_api.Instance(eln_connection.url).login_token(eln_connection.token)

        root = self.instance.get_root_collection()
        try:
            if eln_connection.chemotion_settings.is_sync:
                self.col = root.sync_root.find(id=eln_connection.chemotion_settings.collection_id, )[0]
            else:
                self.col = root.find(id=eln_connection.chemotion_settings.collection_id, )[0]
        except:
            raise ValueError("Chemotion collection could not be found! Please make sure your Chemotion settings are "
                             "correct!")

        eln_connection.chemotion_settings.collection = self.col.__str__()
        eln_connection.chemotion_settings.save()

    def load_list_for_example_selection(self, sync_model_name: str, **kwargs) -> list[tuple[int, str]]:
        obj_list = self._load_obj_list(sync_model_name)
        return [(p.json_ld['@id'], p.name) for p in obj_list[:10]]

    def read_one(self, sync_model_name: str, id: int | str, **kwargs) -> dict:
        obj = self._load_single_obj(sync_model_name, id)
        return self._parse_obj_to_json(obj)

    def read(self, sync_model_name: str, last_update: datetime = None):
        # noinspection PyUnresolvedReferences
        for p in self._load_obj_list(sync_model_name).iter_elements():
            yield self._parse_obj_to_json(p, load=True)

    def write(self, sync_model_name: str, elem: dict):
        stored_elem = self._load_single_obj(sync_model_name, elem.get('id'))
        return self._write_loaded(stored_elem, elem)

    def _write_loaded(self, stored_elem, elem):
        for key, val in elem.items():
            if key not in ['id', 'last_update', 'molecule', 'data_figures']:
                self._write_segment(stored_elem.segments, elem, key)
        if 'molecule' in elem and hasattr(stored_elem, 'molecule'):
            if(elem['molecule']['cano_smiles'] != stored_elem.molecule['cano_smiles']):
                try:
                    m = self.instance.molecule().create_molecule_by_smiles(elem['molecule']['cano_smiles'])
                    stored_elem.molecule = m
                except:
                    pass
            #elif(elem['molecule']['inchistring'] != stored_elem.molecule['inchistring']):
            #    try:
            #        m = self.instance.molecule().create_molecule_by_cls(elem['molecule']['cano_smiles'])
            #        stored_elem.molecule = m
            #    except:
            #        pass

        stored_elem.save()
        pass

    def create_new(self, sync_model_name: str, sync_key: str | None, sync_val_key: any, setter: Callable[[dict], None],
                   **kwargs):
        if sync_model_name.lower().startswith('chmotion:type/sample/'):
            s = self._new_sample(name=sync_val_key, cano_smiles=sync_val_key)
        elif sync_model_name.lower().startswith('chmotion:type/reaction/'):
            s = self.col.new_reaction()
            s.properties['starting_materials'].append(self._get_dummy_sample())
            s.save()
        else:
            s = self.col.new_element_by_iri(sync_model_name)
            s.save()
        json_obj = self._parse_obj_to_json(s)
        setter(json_obj)
        return json_obj

    def _figure_to_data_link(self, response): # fc aka file_content
        base64_utf8_str = base64.b64encode(response.content).decode('utf-8')
        return f'data:image/svg+xml;base64,{base64_utf8_str}'

    def _parse_obj_to_json(self, obj: AbstractElement, load: bool = False, **kwargs):
        if load: obj.load()
        # if obj.element_type == 'reaction':
        for key, val in obj.segments.items():
            obj.segments.get(key)
        res = obj.segments
        res['id'] = obj.json_ld['@id']
        res['last_update'] = datetime.timestamp(obj.last_update)
        if hasattr(obj, 'load_image'):
            try:
                res['data_figures'] = self._figure_to_data_link(obj.load_image())
            except:
                pass
        if hasattr(obj, 'molecule'):
            res['molecule'] = {
                'cano_smiles': obj.molecule.get('cano_smiles'),
                'inchistring': obj.molecule.get('inchistring')
            }
        res = self._iterate(res)
        return res


    def _load_single_obj(self, sync_model_name: str, id: str):
        if sync_model_name in self.instance.get_all_types():
            return self.instance.get_json_ld_id(id)
        raise ValueError(f"{sync_model_name} is not supported!")

    def _load_obj_list(self, sync_model_name: str) -> list[AbstractElement]:
        return self.col.get_elements_of_iri(sync_model_name)

    def _iterate(self, d: any) -> any:
        if issubclass(d.__class__, chemotion_api.elements.abstract_element.AbstractElement):
            return self._parse_obj_to_json(d)
        elif isinstance(d, datetime):
            return d.isoformat()
        elif isinstance(d, date):
            return d.isoformat()
        elif not isinstance(d, dict):
            return d

        res = {}
        for k, v in d.items():
            if isinstance(v, list):
                res[k] = []
                for idx, item in enumerate(v):
                    res[k].append(self._iterate(item))
            elif isinstance(v, dict):
                res[k] = self._iterate(v)
            else:
                res[k] = self._iterate(v)
        return res

    def _new_sample(self, **props):
        if 'cano_smiles' in props:
            try:
                return self.col.new_sample_smiles(props['cano_smiles'])
            except:
                pass
        name = props.get('name', props.get('external_label'))
        if name is not None:
            try:
                return self.col.new_solvent(name)
            except:
                pass
        a = self.col.new_sample()
        a.toggle_decoupled()
        return a

    def _write_segment(self, stored_container: dict | list, new_container: dict | list, key: str | int):
        if isinstance(stored_container, MaterialList):
            new_elem: dict = new_container[key]
            while len(stored_container) <= key:
                stored_container.append(self._new_sample(**new_elem.get('Properties', {}), **new_elem.get('molecule', {})))
            stored_elem = stored_container[key]
        elif isinstance(new_container, list):
            del stored_container[len(new_container):]
            while len(stored_container) <= key:
                stored_container.append(None)
            stored_elem = stored_container[key]
            new_elem: dict = new_container[key]
        elif isinstance(new_container, dict):
            stored_elem = stored_container.get(key)
            new_elem: dict = new_container.get(key)
        else:
            stored_container[key] = new_container[key]
            return

        if issubclass(stored_elem.__class__, AbstractElement):
            self._write_loaded(stored_elem, new_elem)
            return

        if stored_elem == new_elem:
            return

        if isinstance(new_elem, list):
            if stored_elem is None:
                stored_elem = stored_container[key] = []
            iterator = enumerate(new_elem)
        elif isinstance(new_elem, dict):
            if stored_elem is None:
                stored_elem = stored_container[key] = {}
            iterator = new_elem.items()
        else:
            stored_container[key] = new_elem
            return

        res = {}
        for k, v in iterator:
            self._write_segment(stored_elem, new_elem, k)

    def _get_dummy_sample(self):
        d_col = self.col.get_or_create_collection('DUMMIES')
        samples = d_col.get_samples(1)
        if len(samples) > 0:
            return samples[0]
        s = d_col.new_sample()
        s.toggle_decoupled()
        s.save()
        return s
