from django.contrib import admin
from chemotion_app.models import ChemotionConnection

# Register your models here.
@admin.register(ChemotionConnection)
class ChemotionConnectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'url' , 'is_connected']