from chemotion_app.models import ChemotionSettings
import chemotion_api
from django.core.exceptions import ValidationError

from chemotion_app.models import ChemotionConnection
from django.forms.models import ModelForm
from django import forms


# Form Model ChemotionConnection

class ChemotionConnectionForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), help_text="Enter chemotion user Password", required=False)
    token = forms.CharField(widget=forms.HiddenInput(), required=False)
    is_connected = forms.CharField(widget=forms.HiddenInput())
    class Meta:
        model = ChemotionConnection
        fields = ['name', 'url', 'user', 'token', 'is_connected']



    def clean(self):
        super().clean()
        password = self.cleaned_data.get('password')
        token = self.cleaned_data.get('token', '')
        if len(self.errors) == 0:
            try:
                try:
                    instance = chemotion_api.Instance(self.cleaned_data['url']).test_connection()
                except Exception as e:
                    raise ValidationError("Connection to Chemotion failed")
                try:

                    if password is None or password == '':
                        instance.login_token(token).get_root_collection()
                    else:
                        self.cleaned_data['token'] = instance.login(self.cleaned_data['user'], self.cleaned_data['password']).token
                except:
                    if password is not None and password != '' and token is not None and password != '':
                        raise ValidationError("Log in to Chemotion failed: leave Password empty to use token")
                    raise ValidationError("Log in to Chemotion failed")
                self.cleaned_data['is_connected'] = True
                return
            except Exception as e:
                if self.instance is not None:
                    self.instance.is_connected = False
                    self.instance.token = ''
                    self.instance.save()
                raise e



# Form Model ChemotionSettings

class ChemotionSettingsForm(ModelForm):
    class Meta:
        model = ChemotionSettings
        exclude = ("uuid", )
