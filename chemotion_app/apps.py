from django.apps import AppConfig


class ChemotionAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chemotion_app'

    def ready(self):
        from chemotion_app.sync_manager.chemotion import ChemotionSyncer
        from sync_tools.manager import register_manager
        register_manager('chemotion', ChemotionSyncer)