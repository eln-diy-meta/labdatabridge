from django.urls import path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('setup_chemotion/<str:uuid>', sdc_views.SetupChemotion.as_view(), name='scd_view_setup_chemotion'),
]
