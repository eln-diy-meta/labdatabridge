import chemotion_api
from django.http import Http404
from sdc_core.sdc_extentions.views import SDCView, SdcLoginRequiredMixin
from django.shortcuts import render
from chemotion_app.models import ChemotionSettings, ChemotionConnection


class SetupChemotion(SdcLoginRequiredMixin, SDCView):
    template_name = 'chemotion_app/sdc/setup_chemotion.html'

    def get_all_sync_collections(self, instance):
        col_list = []
        for col in instance.get_root_collection().sync_root.children:
            self._get_all_child_collections(col, col_list)
        return col_list

    def get_all_collections(self, instance):
        col_list = []
        self._get_all_child_collections(instance.get_root_collection(), col_list)

        return col_list

    def _get_all_child_collections(self, col, col_list):
        for col in col.children:
            col_list.append(col)
            self._get_all_child_collections(col, col_list)

    def get_content(self, request, uuid, *args, **kwargs):
        try:
            chem = ChemotionConnection.objects.get(uuid=uuid, is_connected=True)
            chem_settings, c = ChemotionSettings.objects.get_or_create(uuid=uuid)
            if c:
                chem.chemotion_settings = chem_settings
                chem.save()
        except Exception as e:
            raise Http404('ELN Connection not available')
        try:
            instance = chemotion_api.Instance(chem.url).login_token(chem.token).test_connection()
        except Exception as e:
            raise Http404('Chemotion is not available')

        return render(request, self.template_name, {
            'settings': chem_settings,
            'collections': self.get_all_collections(
                instance) + self.get_all_sync_collections(instance)})
