from django.urls import path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('import_mappings/<int:pk>', sdc_views.ImportMappings.as_view(), name='scd_view_import_mappings'),
    path('operator_editor/<int:pk>', sdc_views.OperatorEditor.as_view(), name='scd_view_operator_editor'),
    path('json_key_selector_mixin', sdc_views.JsonKeySelectorMixin.as_view(), name='scd_view_json_key_selector_mixin'),
    path('property_change_list', sdc_views.PropertyChangeList.as_view(), name='scd_view_property_change_list'),
    path('synced_process_list/<int:pk>', sdc_views.SyncedProcessList.as_view(), name='scd_view_synced_process_list'),
    path('sync_process_watcher', sdc_views.SyncProcessWatcher.as_view(), name='scd_view_sync_process_watcher'),
    path('sync_mapping_editor/<int:pk>', sdc_views.SyncMappingEditor.as_view(), name='scd_view_sync_mapping_editor'),
    path('sync_obj_json_div/<int:pk>', sdc_views.SyncObjJsonDiv.as_view(), name='scd_view_sync_obj_json_div'),
    path('sync_mapping_manager/<int:pk>', sdc_views.SyncMappingManager.as_view(), name='scd_view_sync_mapping_manager'),
    path('sync_obj_editor/<int:pk>', sdc_views.SyncObjEditor.as_view(), name='scd_view_sync_obj_editor'),
    path('sync_obj_manager', sdc_views.SyncObjManager.as_view(), name='scd_view_sync_obj_manager'),
    path('list_sync_instance', sdc_views.ListSyncInstance.as_view(), name='scd_view_list_sync_instance'),
    path('home_menu/<int:pk>', sdc_views.HomeMenu.as_view(), name='scd_view_home_menu'),
    path('instance_dashboard/<int:pk>', sdc_views.InstanceDashboard.as_view(), name='scd_view_instance_dashboard'),
    path('create_sync_instance', sdc_views.CreateSyncInstance.as_view(), name='scd_view_create_sync_instance'),
    path('basic_info', sdc_views.BasicInfo.as_view(), name='scd_view_basic_info'),
    path('export/<int:pk>', sdc_views.ExportManager.as_view(), name='export_mappings'),
    path('export_operator/<str:src>/<int:pk>', sdc_views.OperatorExportManager.as_view(), name='export_operator'),
]
