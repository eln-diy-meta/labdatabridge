import json
import os.path
import shutil
import tempfile
from datetime import datetime

from celery import shared_task
from django.core.files.storage import FileSystemStorage
from django.forms import model_to_dict
from django.http import Http404, HttpResponse
from sdc_core.sdc_extentions.response import send_success, send_error
from sdc_core.sdc_extentions.views import SDCView, SdcLoginRequiredMixin
from django.shortcuts import render
from django.views import View
from rocrate.rocrate import ROCrate
from rocrate.model.person import Person
from main_app.models import ElnConnection, SyncObjModel, SyncProcess, SyncLog, MappingProperty

from main_app.sync_tools.manager import TestSyncManager, ProductiveSyncManager
from main_app.sync_tools.model_graph import ModelGraph
from sync_tools.options import ELN_TYPE


class BasicInfo(SDCView):
    template_name = 'main_app/sdc/basic_info.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)


class CreateSyncInstance(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/create_sync_instance.html'

    def get_current_create_model(self, request, *args, **kwargs):
       return request.session.get('current_create_model')

    def get_connection_model(self, request, eln_type, *args, **kwargs):
        eln_type = ELN_TYPE(eln_type)
        connection_model_pk = self.get_current_create_model(request)
        connection_model = ElnConnection.objects.get(pk=connection_model_pk, is_committed=False)
        element, created = connection_model.get_connections(eln_type)
        return {'pk': element.pk, 'model': connection_model.get_connection_app_names().get(eln_type)}

    def get_content(self, request, *args, **kwargs):
        connection_model_pk = request.session.get('current_create_model')
        if connection_model_pk is not None:
            try:
                ElnConnection.objects.get(pk=connection_model_pk, is_committed=False)
            except:
                connection_model_pk = None

        if connection_model_pk is None:
            instance = ElnConnection.objects.create(user=request.user)
            request.session['current_create_model'] = instance.pk
            request.session.save()

        return render(request, self.template_name)


class InstanceDashboard(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/instance_dashboard.html'

    def get_content(self, request, pk, *args, **kwargs):
        try:
            connection = ElnConnection.objects.get(user=request.user, pk=pk, is_committed=True)
            return render(request, self.template_name, {'connection': connection})
        except:
            raise Http404('ELN not available')



@shared_task
def _run_sync_now(process_pk):
    process = SyncProcess.objects.get(pk=process_pk)
    try:
        manger = ProductiveSyncManager(process)
        manger.run()
    except Exception as e:
        SyncLog.objects.create(process=process,
                               text='Synchronisation Failed! Maybe the ELNs are not reachable!',
                               type_log='e')

class HomeMenu(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/home_menu.html'

    def run_sync_now(self, request, pk, uuid, *args, **kwargs):
        try:
            connection = ElnConnection.objects.get(pk=pk, is_committed=True, user=request.user, is_active=False)
        except ElnConnection.DoesNotExist:
            return send_error(msg='ELN connection not available! Sorry!', status=404)
        process = SyncProcess.objects.create(connection=connection, uuid=uuid)
        _run_sync_now.delay(process.pk)
        return {}

    def reset_pairing(self, request, pk, *args, **kwargs):
        user = request.user
        processes = SyncProcess.objects.filter(connection_id=pk, connection__user=user)
        processes.delete()

    def get_content(self, request, pk, *args, **kwargs):
        try:
            connection = ElnConnection.objects.get(user=request.user, pk=pk, is_committed=True)
        except:
            raise Http404('ELN not available')
        connection_a = connection.get_connections(ELN_TYPE.A)
        connection_b = connection.get_connections(ELN_TYPE.B)
        return render(request, self.template_name, {'connection': connection})


class ListSyncInstance(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/list_sync_instance.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)


class SyncObjManager(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/sync_obj_manager.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name, {'pk': request.GET.get('pk')})


class SyncObjEditor(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/sync_obj_editor.html'

    def get_content(self, request, pk, *args, **kwargs):
        sync_model = SyncObjModel.objects.get(pk=pk, connection__user=request.user)
        return render(request, self.template_name, {'sync_model': sync_model})


class SyncMappingManager(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/sync_mapping_manager.html'

    def recompile_all(self, request, *args, **kwargs):
        user = request.user
        sync_model = SyncObjModel.objects.get(pk=kwargs.get('pk'), connection__user=user)
        for mp in sync_model.mappingproperty_set.all():
            MappingProperty.compile_operator(mp.a_to_b_xml, mp.a_to_b_operator)
            MappingProperty.compile_operator(mp.b_to_a_xml, mp.b_to_a_operator)

    def get_content(self, request, pk, *args, **kwargs):
        sync_model = SyncObjModel.objects.get(pk=pk, connection__user=request.user)
        return render(request, self.template_name, {'sync_model': sync_model})


class SyncObjJsonDiv(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/sync_obj_json_div.html'

    def test_sync_b_to_a(self, request, *args, **kwargs):
        user = request.user
        sync_model = SyncObjModel.objects.get(pk=kwargs.get('sync_pk'), connection__user=user)
        syncer = TestSyncManager(sync_model.connection)

        obj_a = syncer.sync_manager[ELN_TYPE.A].read_one(sync_model.a_model_name, kwargs.get('id_key_a'))
        obj_b = syncer.sync_manager[ELN_TYPE.B].read_one(sync_model.b_model_name, kwargs.get('id_key_b'))
        return syncer.sync_obj_b_to_a(obj_b, obj_a, sync_model, False)

    def test_sync_a_to_b(self, request, *args, **kwargs):
        user = request.user
        sync_model = SyncObjModel.objects.get(pk=kwargs.get('sync_pk'), connection__user=user)
        syncer = TestSyncManager(sync_model.connection)

        obj_a = syncer.sync_manager[ELN_TYPE.A].read_one(sync_model.a_model_name, kwargs.get('id_key_a'))
        obj_b = syncer.sync_manager[ELN_TYPE.B].read_one(sync_model.b_model_name, kwargs.get('id_key_b'))
        return syncer.sync_obj_a_to_b(obj_a, obj_b, sync_model, False)

    def load_a_sample(self, request, *args, **kwargs):
        user = request.user
        sync_model = SyncObjModel.objects.get(pk=kwargs.get('sync_pk'), connection__user=user)
        res = sync_model.connection.get_sync_manager(ELN_TYPE.A).read_one(sync_model.a_model_name,
                                                                           kwargs.get('id_key'))
        return res

    def load_b_sample(self, request, *args, **kwargs):
        user = request.user
        sync_model = SyncObjModel.objects.get(pk=kwargs.get('sync_pk'), connection__user=user)
        res = sync_model.connection.get_sync_manager(ELN_TYPE.B).read_one(sync_model.b_model_name,
                                                                           kwargs.get('id_key'))
        return res

    def get_content(self, request, pk, *args, **kwargs):
        user = request.user
        sync_model = SyncObjModel.objects.get(pk=pk, connection__user=user)
        connection = sync_model.connection
        a_list = connection.get_sync_manager(ELN_TYPE.A).load_list_for_example_selection(sync_model.a_model_name)
        b_list = connection.get_sync_manager(ELN_TYPE.B).load_list_for_example_selection(sync_model.b_model_name)
        return render(request, self.template_name, {'sync_model': sync_model,
                                                    'a_list': a_list,
                                                    'b_list': b_list
                                                    })


class SyncMappingEditor(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/sync_mapping_editor.html'

    def get_content(self, request, pk, *args, **kwargs):
        return render(request, self.template_name, {'pk': pk})


class SyncProcessWatcher(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/sync_process_watcher.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)


class SyncedProcessList(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/synced_process_list.html'

    def get_content(self, request, pk, *args, **kwargs):
        return render(request, self.template_name)


class PropertyChangeList(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/property_change_list.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)


class JsonKeySelectorMixin(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/json_key_selector_mixin.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)


class OperatorEditor(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/operator_editor.html'

    def get_content(self, request, pk, *args, **kwargs):
        mapping = MappingProperty.objects.get(pk=pk)
        connection = mapping.sync_model.connection
        eln_list = [connection.get_eln_type(ELN_TYPE.A), connection.get_eln_type(ELN_TYPE.B)]

        def get_eln_list():
            yield eln_list + ['A']
            eln_list.reverse()
            yield eln_list + ['B']

        return render(request, self.template_name,
                      {'eln_list': get_eln_list, 'a': eln_list[0], 'b': eln_list[1], 'mapping': mapping})


class ImportMappings(SdcLoginRequiredMixin, SDCView):
    template_name = 'main_app/sdc/import_mappings.html'

    def loadMappings(self, request, *args, **kwargs):
        session_data = request.session
        uid = session_data.get('_auth_user_id')
        sync_model = SyncObjModel.objects.get(pk=kwargs.get('pk'), connection__user_id=uid)
        mg = ModelGraph()
        mg.load()
        mg.create_new_mappings(sync_model)

    def get_content(self, request, pk, *args, **kwargs):
        sync_model = SyncObjModel.objects.get(pk=pk, connection__user=request.user)
        mg = ModelGraph()
        mg.load()
        props, paths = mg.find_all_property_paths(sync_model)
        return render(request, self.template_name, {'path_lists': paths})


class ExportManager(View):

    def post(self, request, pk):
        rocrate = request.FILES.get('import')
        dt_str = datetime.now().strftime("imports/%Y_%m_%d_%H_%M_%S_%f")
        fs = FileSystemStorage()
        filename = fs.save(f'{dt_str}_{rocrate.name}', rocrate)
        crate = ROCrate(fs.path(filename))
        connection = ElnConnection.objects.get(pk=pk, user=request.user)
        new_model_pk = 0
        for e in crate.get_entities():
            if e.get('context') == 'elndatabridge://model/mappingproperty/':
                if e.type == 'File' and e.source.suffix == '.json':
                    with open(e.source, 'r', encoding='utf8') as f:
                        data = json.loads(f.read())
                        new_model = SyncObjModel()
                        for key in ['a_model_name', 'b_model_name', 'a_sync_key', 'b_sync_key']:
                            setattr(new_model, key, data['model'][key])

                        if new_model.a_model_name not in [x for xs in connection.get_a_sync_manager().syncable_models() for x in xs]:
                            return send_error(msg=f"{new_model.a_model_name} not available {connection.name_a}")

                        if new_model.b_model_name not in [x for xs in connection.get_b_sync_manager().syncable_models() for x in xs]:
                            return send_error(msg=f"{new_model.b_model_name} not available {connection.name_b}")

                        new_model.connection = connection
                        new_model.name = f"Import: {data['model']['name']}"
                        new_model.save()
                        new_model_pk = new_model.pk
                        model_sub = {}
                        new_models = []
                        for mappings in data['mappings']:
                            model_mapping = MappingProperty()
                            for key in ['identifier', 'data_type', 'a_to_b_xml', 'b_to_a_xml', 'a_key', 'b_key']:
                                setattr(model_mapping, key, mappings[key])
                            model_mapping.sync_model = new_model
                            model_mapping.save()
                            new_models.append(model_mapping)
                            model_sub[mappings['@id']] = model_mapping.pk

                        for idx, mp in enumerate(new_models):
                            if data['mappings'][idx]['parent_element'] is not None:
                                mp.parent_element_id = model_sub[data['mappings'][idx]['parent_element']]
                                mp.save()
                            MappingProperty.compile_operator(mp.a_to_b_xml, str(mp.a_to_b_operator))
                            MappingProperty.compile_operator(mp.b_to_a_xml, str(mp.b_to_a_operator))

        return send_success(pk=new_model_pk)

    def get(self, request, pk):
        serialized_obj = model_to_dict(SyncObjModel.objects.get(pk=pk, connection__user=request.user),
                                       exclude=('pk', 'id', 'connection'))
        values = {
            'model': serialized_obj
        }
        host = request.build_absolute_uri('/')[:-1]

        values['model'] |= {'@type': 'elndatabridge://model/syncobjmodel/', '@id': f'{host}/api/syncobjmodel/{pk}'}
        values['mappings'] = []
        model_mapping = MappingProperty.objects.filter(sync_model__id=pk)
        for val in model_mapping:
            serialized_obj = model_to_dict(val, exclude=('pk', 'sync_model', 'a_to_b_operator', 'b_to_a_operator'))
            serialized_obj |= {'@type': f'elndatabridge://model/mappingproperty/',
                               '@id': f'{host}/api/mappingproperty/{serialized_obj["id"]}'}
            del serialized_obj["id"]
            if serialized_obj['parent_element'] is not None:
                serialized_obj['parent_element'] = f'{host}/api/mappingproperty/{serialized_obj["parent_element"]}'

            values['mappings'].append(serialized_obj)

        dirpath = tempfile.mkdtemp()
        crate = ROCrate()
        crate.name = f'export_syncobjmodel_{pk}'

        json_path = os.path.join(dirpath, 'syncobjmodel.json')
        with open(json_path, 'w+') as f:
            f.write(json.dumps(values))

        package = crate.add_file(json_path, dest_path='data/syncobjmodel.json', properties={
            "name": f'syncobjmodel_{pk}',
            'encodingFormat': 'application/json',
            'context': 'elndatabridge://model/mappingproperty/'
        })

        publisher = crate.add(
            Person(crate, f'{request.user.username}', {'name': f'{request.user.first_name} {request.user.last_name}'}))
        package["author"] = publisher
        crate.write_zip(os.path.join(dirpath, crate.name + '.zip'))
        with open(os.path.join(dirpath, crate.name + '.zip'), 'rb') as zip_file:
            response = HttpResponse(zip_file.read(), content_type='application/force-download')
        shutil.rmtree(dirpath)
        response['Content-Disposition'] = f'attachment; filename="{crate.name}.zip"'
        return response



class OperatorExportManager(View):

    def get(self, request, src, pk):

        model = MappingProperty.objects.get(pk=pk, sync_model__connection__user=request.user)
        if src == 'a':
            src_path = model.operator_a_to_b_file_path
            prefix = '_a_to_b'
        elif src == 'b':
            src_path = model.operator_b_to_a_file_path
            prefix = '_b_to_a'
        else:
            raise Http404('File could not be found')

        with open(src_path, 'rb') as f:
            response = HttpResponse(f.read(), content_type='text/x-python')

        response['Content-Disposition'] = f'attachment; filename="{model.identifier}_{prefix}.py"'
        return response