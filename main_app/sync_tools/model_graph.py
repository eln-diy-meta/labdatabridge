import copy
import re
import uuid
import xml.etree.ElementTree as ET
from django.template.loader import render_to_string

from main_app.models import SyncObjModel, MappingProperty
from sync_tools.options import ELN_TYPE


class ModelGraph:

    def __init__(self):
        self.nodes = []
        self.arcs = []

    def load(self):
        res = []
        for model in SyncObjModel.objects.filter(locked=True):
            res.append((
                *self.model_to_node(model),
                model.pk
            ))

        for model in res:
            self.add_arc(*model)

    def add_node(self, node):
        if node in self.nodes:
            return self.nodes.index(node)
        self.nodes.append(node)
        return len(self.nodes) - 1

    def add_arc(self, node_a: str, node_b: str, label: any):
        idx_a = self.add_node(node_a)
        idx_b = self.add_node(node_b)
        arc_a = (idx_a, idx_b, label)
        arc_b = (idx_b, idx_a, label)
        self.arcs.append(arc_b)
        self.arcs.append(arc_a)

    def _rec_find_path(self, idx_a: int, idx_b: int, visited: list[int]):
        visited.append(idx_a)
        all_arcs = [x for x in self.arcs if x[0] == idx_a and x[1] not in visited]

        done = []
        for arc in all_arcs:
            if arc[1] == idx_b:
                done.append([arc])
            else:
                for paths in self._rec_find_path(arc[1], idx_b, copy.deepcopy(visited)):
                    done.append([arc] + paths)
        return done

    def find_path(self, model: SyncObjModel) -> tuple[list[str]:list[list[int]]]:
        node_a, node_b = self.model_to_node(model)
        if node_a not in self.nodes or node_b not in node_b:
            return [], []
        idx_a = self.add_node(node_a)
        idx_b = self.add_node(node_b)
        result_text = []
        result_id = []
        for path in self._rec_find_path(idx_a, idx_b, [idx_a]):
            result_text.append('')
            result_id.append([])
            for step in path:
                result_text[-1] += f'{self.nodes[step[0]]}<->'
                result_id[-1].append(step[2])
            result_text[-1] += node_b
        return result_text, result_id

    def find_all_property_paths(self, model: SyncObjModel) -> tuple[
                                                              dict[str:list[tuple[ELN_TYPE, MappingProperty]]]:list[
                                                                  tuple[str:str:str]]]:
        path_text, path_id = self.find_path(model)
        return self.property_path(path_text, path_id, model)

    def create_new_mappings(self, sync_model: SyncObjModel):
        props, paths = self.find_all_property_paths(sync_model)
        new_mappings = []
        props_array = props.values()
        for v in props_array:
            new_mappings.append(self._make_new_mapping_property(sync_model, v))
        for i, v in enumerate(props_array):
            if v[0][1].parent_element is not None and new_mappings[i].parent_element is None:
                idx = next(i for i, x in enumerate(props_array) if x[0][1].pk == v[0][1].parent_element_id)
                new_mappings[i].parent_element = new_mappings[idx]
                new_mappings[i].save()

    @classmethod
    def property_path(cls, path_text: list[str],
                      path_id: list[list[int]],
                      model: SyncObjModel) -> tuple[dict[str:list[tuple[ELN_TYPE, MappingProperty]]]:list[
        tuple[str:str:str]]]:
        start_node, end_node = cls.model_to_node(model)

        idxes = [i[0] for i in sorted(enumerate(path_id), key=lambda x: len(x[1]))]
        path_id = [path_id[i] for i in idxes]
        path_text = [path_text[i] for i in idxes]
        full_path = []
        prop_mapping_paths = {}
        for path_idx, path in enumerate(path_id):
            current_node = start_node
            property_path = []
            prop_length = 0
            for step_pk in path:
                x = SyncObjModel.objects.get(pk=step_pk)
                node_a, node_b = cls.model_to_node(x)
                eln_type = ELN_TYPE.B
                if node_a == current_node:
                    eln_type = ELN_TYPE.A
                if current_node == start_node:
                    for prop in x.mappingproperty_set.all():
                        property_path.append([(eln_type, prop)])
                else:
                    for cp in property_path:
                        res = cp[-1][1].get_key(cp[-1][0].complement)

                        filter_set = {f'{eln_type}_key': res, 'data_type': cp[-1][1].data_type}
                        prop = x.mappingproperty_set.filter(**filter_set)
                        if len(prop) > 0:
                            cp.append((eln_type, prop.first()))
                    pass
                prop_length += 1
                property_path = [x for x in property_path if len(x) == prop_length]
                current_node = node_b
            for cp in property_path:
                a_key = cp[0][1].get_key(cp[0][0])
                b_key = cp[-1][1].get_key(cp[-1][0].complement)
                res = f'{a_key}-{b_key}'
                if res not in prop_mapping_paths:
                    full_path.append((path_text[path_idx], a_key, b_key))
                    prop_mapping_paths[res] = cp
        return prop_mapping_paths, full_path

    @classmethod
    def _make_new_mapping_property(cls, model: SyncObjModel,
                                   prop_path: list[tuple[ELN_TYPE, MappingProperty]]) -> tuple[MappingProperty: bool]:
        xml_a_to_b = cls._create_path_xml(prop_path)
        xml_b_to_a = cls._create_path_xml(prop_path, True)
        data_type = prop_path[-1][1].data_type
        a_key = prop_path[0][1].get_key(prop_path[0][0])
        b_key = prop_path[-1][1].get_key(prop_path[-1][0].complement)
        try:
            return MappingProperty.objects.get(a_key=a_key, b_key=b_key, data_type=data_type, sync_model = model)
        except:
            pass

        mp = MappingProperty.objects.create(identifier=prop_path[-1][1].identifier, a_to_b_xml=xml_a_to_b,
                                            b_to_a_xml=xml_b_to_a, a_key=a_key, b_key=b_key, data_type=data_type)
        res_a_to_b = MappingProperty.compile_operator(mp.a_to_b_xml, str(mp.a_to_b_operator))
        res_b_to_a = MappingProperty.compile_operator(mp.b_to_a_xml, str(mp.b_to_a_operator))
        if res_a_to_b != 0 or res_b_to_a != 0:
            mp.delete()
            raise ValueError('XML could not be processed!')
        mp.sync_model = model
        mp.save()
        return mp

    @classmethod
    def model_to_node(cls, model: SyncObjModel) -> tuple[str: str]:
        node_a = f'{model.connection.eln_a.name}:{model.a_model_name}'
        node_b = f'{model.connection.eln_b.name}:{model.b_model_name}'
        return node_a, node_b

    @classmethod
    def _create_path_xml(cls, prop_path, b_to_a=False):

        def remove_namespace(tree):
            for elem in tree.iter():
                if '}' in elem.tag:
                    elem.tag = elem.tag.split('}', 1)[1]
                for key in elem.attrib.keys():
                    if '}' in key:
                        new_key = key.split('}', 1)[1]
                        elem.attrib[new_key] = elem.attrib[key]
                        del elem.attrib[key]

        tree_list = None
        name_list = []

        x = 0
        y = 0
        for step_type, step_mapping in prop_path:
            if b_to_a:
                step_type = step_type.complement
                des = f"{step_mapping.sync_model}:{step_mapping.b_to_a_operator}"
            else:
                des = f"{step_mapping.sync_model}:{step_mapping.a_to_b_operator}"
            tree1 = ET.fromstring(step_mapping.get_xml_from(step_type))
            remove_namespace(tree1)
            des = f"{step_mapping.sync_model}:{step_mapping.a_to_b_operator}"
            name = "op_" + re.sub(r"[^a-zA-Z0-9]", '_', des)

            wrapper_xml = render_to_string('main_app/xml/function_wrapper.xml', {
                'block_uuid': uuid.uuid4().__str__(),
                'x': x,
                'y': y,
                'name': name,
                'description': des
            })
            x = None
            y = None
            wrapper_tree = ET.fromstring(wrapper_xml)
            node = wrapper_tree.find('statement')
            for child in tree1:
                if 'x' in child.attrib: del child.attrib['x']
                if 'y' in child.attrib: del child.attrib['y']

                node.append(child)
            if tree_list is not None:
                tree_list.find(f".//next[@id='next_{name_list[-1]}']").append(wrapper_tree)
            else:
                tree_list = wrapper_tree
            name_list.append(name)

        function_caller = render_to_string('main_app/xml/function_caller.xml', {
            'names': name_list,
            'id': uuid.uuid4().__str__(),
        })

        tree_list.find(f".//next[@id='next_{name_list[-1]}']").append(ET.fromstring(function_caller))

        return f"<xml>\n{ET.tostring(tree_list, encoding='unicode', short_empty_elements=False)}\n</xml>"
