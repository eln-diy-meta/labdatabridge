// Require Blockly core.
const Blockly = require('blockly');
// Require the default blocks.
const libraryBlocks = require('blockly/blocks');
// Require a generator.
const {pythonGenerator} = require('blockly/python');
const fs = require('fs');
const path = require('path');
require('./blocks.js')


const xmlText = process.argv[2];
const fileUuid = process.argv[3];
const dirPath = process.argv[4];

try {
    var xml = Blockly.utils.xml.textToDom(xmlText);
    // Create a headless workspace.
    let workspace = new Blockly.Workspace();
    Blockly.Xml.domToWorkspace(xml, workspace);
    let code = pythonGenerator.workspaceToCode(workspace);
    if (!code) {
        code = 'return current_val';
    }
    if (!fs.existsSync()) {
        fs.mkdirSync(dirPath, {recursive: true});
    }
    const filePath = path.join(dirPath, `${fileUuid}.py`);
    if (!fs.existsSync(dirPath)) {
        fs.unlinkSync(dirPath);
    }
    const content = `def handler(input_val, current_val=None, iteration_predecessor=[]):\n  try:\n${code.split('\n').map((x) => '    ' + x).join('\n')}\n  except:\n    return current_val`;
    const prefix = 'from datetime import datetime\n\n'
    fs.writeFile(filePath, prefix + content, err => {
        if (err) {

            process.exit(1);
        } else {
            process.exit(0);
        }
    });


} catch (e) {
    console.log(e);
    process.exit(2);
}