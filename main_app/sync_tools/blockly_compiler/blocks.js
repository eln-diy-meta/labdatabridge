const Blockly = require('blockly');
// Require a generator.
const {pythonGenerator, Order} = require('blockly/python');


Blockly.defineBlocksWithJsonArray(require('../../Assets/src/main_app/utils/bockly_def.json').blocks);

pythonGenerator.forBlock['get_from_json'] = function (block, generator) {
    var value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    var value_idx = generator.valueToCode(block, 'idx', Order.ATOMIC);

    return [`${value_json_obj}[${value_idx}]`, Order.NONE];
};

pythonGenerator.forBlock['set_in_json'] = function (block, generator) {
    var value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    var value_idx = generator.valueToCode(block, 'idx', Order.ATOMIC);
    var value_value = generator.valueToCode(block, 'value', Order.ATOMIC);

    return `${value_json_obj}[${value_idx}] = ${value_value}\n`;
};

pythonGenerator.forBlock['for_each_json'] = function (block, generator) {
    let variable_key = generator.nameDB_.getName(block.getFieldValue('Key'), Blockly.Variables.CATEGORY_NAME);
    let variable_val = generator.nameDB_.getName(block.getFieldValue('val'), Blockly.Variables.CATEGORY_NAME);
    let value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    let statements_code = generator.statementToCode(block, 'code');
    // TODO: Assemble javascript into code variable.
    return `for ${variable_key}, ${variable_val} in ${value_json_obj}.items():\n${statements_code}\n`;
};

pythonGenerator.forBlock['split_str'] = function (block, generator) {
    var value_in_str = generator.valueToCode(block, 'in_str', Order.ATOMIC);
    var value_at = generator.valueToCode(block, 'at', Order.ATOMIC);

    return [`str(${value_in_str}).split(str(${value_at}))`, Order.NONE];
};

pythonGenerator.forBlock['json_contains'] = function (block, generator) {
    const value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    const dropdown_key_or_val = block.getFieldValue('key_or_val');
    const value_target = generator.valueToCode(block, 'target', Order.ATOMIC);
    let code = '';
    if(dropdown_key_or_val === 'key') {
        code = `${value_target} in ${value_json_obj}`;
    } else if(dropdown_key_or_val === 'value') {
        code = `${value_target} in ${value_json_obj}.values()`;
    }

    // TODO: Change Order.NONE to the correct strength.
    return [code, Order.NONE];
};

pythonGenerator.forBlock['output'] = function (block, generator) {
    const value_name = generator.valueToCode(block, 'NAME', Order.ATOMIC);
    return `return ${value_name}\n`;
};

pythonGenerator.forBlock['input'] = function (block, generator) {
    return ['input_val', Order.NONE];
};

pythonGenerator.forBlock['convert_to'] = function(block, generator) {
    const value_input_variable = generator.valueToCode(block, 'input_variable', Order.ATOMIC);
    const dropdown_to_type = block.getFieldValue('to_type');
    let code;
    if(dropdown_to_type === 'int'){
        code = `int(${value_input_variable})`;
    } else if (dropdown_to_type === "str") {
        code = `str(${value_input_variable})`;
    } else if (dropdown_to_type === 'float') {
        code = `float(${value_input_variable})`;
    } else if (dropdown_to_type === 'bool') {
        code = `bool(${value_input_variable}) if not isinstance(${value_input_variable}, str) else str(${value_input_variable}).lower() in ['yes', 'true', 'right']`;
    }
    return [code, Order.NONE];
};

pythonGenerator.forBlock['empty_json'] = function(block, generator) {
    return ['{}', Order.NONE];
};

pythonGenerator.forBlock['current_value'] = function(block, generator) {
    return ['current_val', Order.NONE];
}

pythonGenerator.forBlock['iteration_predecessor'] = function(block, generator) {
    return ['iteration_predecessor', Order.NONE];
}

pythonGenerator.forBlock['str_to_date'] = function(block, generator) {
    const value_date = generator.valueToCode(block, 'date', Order.ATOMIC);
    const text_format = block.getFieldValue('format');
    const code = `datetime.strptime(${value_date}, '${text_format}')`
    return [code, Order.NONE];
};

pythonGenerator.forBlock['datetime_to_str'] = function(block, generator) {
    const value_date = generator.valueToCode(block, 'date', Order.ATOMIC);
    const text_format = block.getFieldValue('format');
    const code = `datetime.strftime(${value_date}, '${text_format}')`
    return [code, Order.NONE];
};

pythonGenerator.forBlock['function_wrapper'] = function(block, generator) {
    const text_func_name = block.getFieldValue('func_name');
    let statements_body = generator.statementToCode(block, 'body');
    if(statements_body.trim() === '') {
        return `def ${text_func_name}(input_val, current_val):pass\n`;
    }
    return `def ${text_func_name}(input_val, current_val):\n${statements_body}\n`;
};

pythonGenerator.forBlock['function_call'] = function(block, generator) {
    const text_func_name = block.getFieldValue('func_name');
    const value_current_val = generator.valueToCode(block, 'current_val', Order.ATOMIC);
    const value_input_val = generator.valueToCode(block, 'input_val', Order.ATOMIC);
    const code = `${text_func_name}(${value_input_val}, ${value_current_val})`;
    return [code, Order.NONE];
};