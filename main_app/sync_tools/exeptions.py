class NotAllowedToCreateException(Exception):
    def __init__(self):
        super().__init__("Not allowed to create Object!")