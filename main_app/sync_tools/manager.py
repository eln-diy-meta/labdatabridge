import copy
from typing import Iterable, Callable
from datetime import datetime

from django.conf import settings

# from eln_user.models import SyncProcess, SyncSettings, SyncedElement, SyncLog, SyncModel, SyncedField
from main_app.models import ElnConnection, SyncProcess, SyncObjModel, MappingProperty, SyncedElement, SyncedProperty, \
    SyncLog

from django.utils.timezone import make_aware
from django.db.models import Q
from sdc_core.sdc_extentions.import_manager import import_function

from main_app.sync_tools.exeptions import NotAllowedToCreateException
from sync_tools.abstract_manager import AbstractManager
from sync_tools.models import GeneralSyncOptions
from sync_tools.options import ELN_TYPE
import traceback


def _get_target_keys_from_src_key(src_key, target_key, key_list):
    src_key = src_key.strip('"').split('"."')
    src_idx_list = [i for i, v in enumerate(src_key) if v == '%d']
    if len(src_idx_list) == 0:
        return [target_key]
    target_key = target_key.strip('"').split('"."')
    target_idx_list = [i for i, v in enumerate(target_key) if v == '%d']
    result_list = []
    for key in key_list:
        new_key = copy.copy(target_key)
        key_as_list = key.strip('"').split('"."')
        for idx in range(len(src_idx_list)):
            new_key[target_idx_list[idx]] = key_as_list[src_idx_list[idx]]
        result_list.append('"{}"'.format('"."'.join(new_key)))
    return result_list


def _get_one_single_target_for_key(obj: dict, key: str | list, from_setter: bool = False) -> any:
    key, val = _get_target_for_key(obj, key, from_setter)

    if len(val) == 0:
        return None
    return val[0]


def _get_target_for_key(obj: dict, key: str | list, from_setter: bool = False) -> tuple[list[str], list[any]]:
    """
    This method returns a tuple with all concrete key path lists and a list with
    all represented values at the same index as the responding key. This system is needed
    since %d can serve as wildcard for integer list indexes within the source object.

    :param obj: The source object
    :param key: The origen key as string the single elements within the path should be seperated by ".". %d can serve as wildcard for list indices.
    :return: tuple with 0 -> all keypath and 1-> all responding values in obj
    """
    if key is None: return [], []
    if isinstance(key, str): key = key.strip('"').split('"."')
    obj, keys = [obj], [[]]
    for x in key:
        if len(obj) == 0:
            return [], []
        if isinstance(obj[0], list):
            if x == '%d':
                obj_len = len(obj)
                for i in range(obj_len):
                    for idx, val in enumerate(obj[i]):
                        obj.append(val)
                        keys.append(copy.copy(keys[i]) + [str(idx)])
                obj = obj[obj_len:]
                if len(obj) == 0:
                    return [], []
                keys = keys[obj_len:]
            else:
                for i, v in reversed(list(enumerate(obj))):
                    if from_setter:
                        while len(v) <= int(x):
                            v.append({})
                    if len(v) > int(x):
                        obj[i] = v[int(x)]
                        keys[i].append(x)
                    else:
                        del obj[i]
                        del keys[i]
        elif isinstance(obj[0], dict):
            for i, v in reversed(list(enumerate(obj))):
                if from_setter and x not in v:
                    v[x] = {}
                if x in v:
                    obj[i] = v[x]
                    keys[i].append(x)
                else:
                    del obj[i]
                    del keys[i]
    return ['"{}"'.format('"."'.join(x)) for x in keys], obj


def _set_target_for_key(obj, key, value):
    if key is None: return
    key_list = key.strip('"').split('"."')
    obj = _get_one_single_target_for_key(obj, key_list[:-1], True)
    if isinstance(obj, list):
        idx = int(key_list[-1])
        while len(obj) <= idx:
            obj.append({})
        obj[idx] = value
    elif isinstance(obj, dict):
        obj[key_list[-1]] = value


class CoreSyncManager:
    def __init__(self, eln_connection: ElnConnection):
        self._operators = {ELN_TYPE.A: {}, ELN_TYPE.B: {}}
        self.sync_manager: dict[ELN_TYPE, AbstractManager] = {
            ELN_TYPE.A: eln_connection.get_a_sync_manager(),
            ELN_TYPE.B: eln_connection.get_b_sync_manager(),
        }

        a_model_class = import_function(eln_connection.eln_a.model_import_path)
        a_model_class.objects.filter(uuid=eln_connection.uuid, connection_position=ELN_TYPE.B).delete()
        b_model_class = import_function(eln_connection.eln_b.model_import_path)
        b_model_class.objects.filter(uuid=eln_connection.uuid, connection_position=ELN_TYPE.A).delete()

        self.sync_options: dict[ELN_TYPE, GeneralSyncOptions] = {
            ELN_TYPE.A: a_model_class.objects.get(
                uuid=eln_connection.uuid, connection_position=ELN_TYPE.A).sync_settings,
            ELN_TYPE.B: b_model_class.objects.get(
                uuid=eln_connection.uuid, connection_position=ELN_TYPE.B).sync_settings
        }

        self._start_time = None
        self.eln_connection = eln_connection
        self._process = None
        self._last_process = None

    @property
    def process(self):
        if self._process is None:
            self._process = SyncProcess.objects.create(connection=self.eln_connection)
        return self._process

    @property
    def last_process(self):
        if self._last_process is None:
            try:
                self._last_process = SyncProcess.objects.filter(~Q(sync_done_time=None),
                                                                connection=self.eln_connection).latest('sync_done_time')
            except Exception as e:
                self._last_process = None
        return self._last_process

    def _find_sync_element(self, sync_model: SyncObjModel, sync_element: SyncedElement):

        result = {ELN_TYPE.A: None, ELN_TYPE.B: None}
        created = {ELN_TYPE.A: False, ELN_TYPE.B: False}

        def find_one_element(sync_key, model_name, eln_type: ELN_TYPE):
            if sync_key is not None:
                for elem in self.sync_manager[eln_type].read(model_name):
                    if _get_one_single_target_for_key(elem, sync_key) == sync_element.elem_sync_name:
                        return elem, False
            if not self.sync_options[eln_type].allow_create:
                SyncLog.objects.create(process=self.process,
                                       element=sync_element,
                                       eln=self.eln_connection.get_eln_type(eln_type),
                                       text=f"Not allowed to crated new Element",
                                       type_log='e')
                raise NotAllowedToCreateException()

            SyncLog.objects.create(process=self.process,
                                   element=sync_element,
                                   eln=self.eln_connection.get_eln_type(eln_type),
                                   text=f"Crated new Element",
                                   type_log='i')
            return self.sync_manager[eln_type].create_new(model_name, sync_key, sync_element.elem_sync_name,
                                                          lambda obj: _set_target_for_key(obj, sync_key,
                                                                                          sync_element.elem_sync_name)), True

        if sync_element.elem_a_id is None:
            sync_key = sync_model.a_sync_key
            result[ELN_TYPE.A], c = find_one_element(sync_key, sync_model.a_model_name,
                                                     ELN_TYPE.A)
            sync_element.elem_a_id = result[ELN_TYPE.A]['id']
            created[ELN_TYPE.A] = c
            sync_element.save()
        else:
            result[ELN_TYPE.A] = self.sync_manager[ELN_TYPE.A].read_one(sync_model.a_model_name, sync_element.elem_a_id)

        if sync_element.elem_b_id is None:
            sync_key = sync_model.b_sync_key
            result[ELN_TYPE.B], c = find_one_element(sync_key, sync_model.b_model_name,
                                                     ELN_TYPE.B)
            sync_element.elem_b_id = result[ELN_TYPE.B]['id']
            created[ELN_TYPE.B] = c
            sync_element.save()
        else:
            result[ELN_TYPE.B] = self.sync_manager[ELN_TYPE.B].read_one(sync_model.b_model_name, sync_element.elem_b_id)

        return (result, created)

    def _write_property_obj(self, sync_elem: SyncedElement,
                            prop: MappingProperty,
                            src_eln: ELN_TYPE,
                            dest_key: str,
                            src_key: str,
                            value: any,
                            operator: Callable[[any, any, list], any], last_update: datetime, array_idx: int | None = None,
                            commit: bool = False,
                            prev_iteration_obj: list = []) -> any:
        new_value = None

        if commit and sync_elem is not None:
            syncedproperty_filter = {'mapping': prop, f'key_{src_eln}': src_key}
            last_values = sync_elem.syncedproperty_set.filter(~Q(creating_process=self.process),
                                                              **syncedproperty_filter)
            last_value = None
            if new_value is None:
                if len(last_values) > 0:
                    last_value = last_values.latest('created_at').get_value()[src_eln.complement]
                new_value = operator(value, last_value, prev_iteration_obj)

            if new_value != last_value and new_value is not None:
                res, c = SyncedProperty.objects.get_or_create(mapping=prop, element=sync_elem,
                                                              creating_process=self.process,
                                                              **{f"key_{src_eln}": src_key,
                                                                 f"key_{src_eln.complement}": dest_key})
                if c or res.created_at < last_update:
                    if not c:
                        SyncLog.objects.create(process=self.process,
                                               element=sync_elem,
                                               eln=self.eln_connection.get_eln_type(src_eln),
                                               text=f"Conflict solved in favor of {self.eln_connection.get_eln_type(src_eln)}",
                                               type_log='i')

                    res.created_at = last_update
                    res.set_value(src_eln, value, new_value)
                else:
                    SyncLog.objects.create(process=self.process,
                                           element=sync_elem,
                                           eln=self.eln_connection.get_eln_type(src_eln),
                                           text=f"Conflict solved in favor of {self.eln_connection.get_eln_type(src_eln.complement)}",
                                           type_log='i')
            return new_value
        return operator(value, None, prev_iteration_obj)

    def _get_operator(self, src_eln: ELN_TYPE, prop: MappingProperty):
        operator = self._operators[src_eln].get(prop)
        if operator is None:
            operator = self._operators[src_eln][prop] = prop.operator_from_x(src_eln)
        return operator

    def sync_objs(self, sync_model: SyncObjModel, src_eln: ELN_TYPE, obj_src, obj_dst=None, commit=True,
                  sync_elem: SyncedElement = None,
                  last_update: datetime = datetime.now()):
        props = sync_model.mappingproperty_set.all()
        for prop in props:
            if prop.data_type != 'array':
                if src_eln == ELN_TYPE.A:
                    src_key = prop.a_key
                    dst_key = prop.b_key
                else:
                    src_key = prop.b_key
                    dst_key = prop.a_key
                operator = self._get_operator(src_eln, prop)
                prev_iteration_obj = []
                keys, val_src = _get_target_for_key(obj_src, src_key)
                if len(val_src) > 0:
                    all_dst_key = _get_target_keys_from_src_key(src_key, dst_key, keys)
                    for idx in range(len(val_src)):
                        new_val = self._write_property_obj(sync_elem=sync_elem, prop=prop, src_eln=src_eln,
                                                           value=val_src[idx],
                                                           src_key=keys[idx],
                                                           dest_key=all_dst_key[idx],
                                                           operator=operator,
                                                           last_update=last_update,
                                                           commit=commit,
                                                           prev_iteration_obj=prev_iteration_obj)

                        prev_iteration_obj.append(val_src[idx])
                        if new_val is not None and obj_dst is not None:
                            _set_target_for_key(obj_dst, all_dst_key[idx], new_val)

        return obj_dst

    def _read_eln(self, src_eln: ELN_TYPE, elem, sync_model,
                  parent_element: SyncedElement | None = None) -> SyncedElement | None:
        raise NotImplementedError()

    def _write_eln(self, sync_model, sync_elements=None):
        pass


class TestSyncManager(CoreSyncManager):
    def __init__(self, eln_connection: ElnConnection):
        super().__init__(eln_connection)

    def sync_obj_a_to_b(self, obj_a: dict, obj_b: dict, sync_model: SyncObjModel, commit=True,
                        sync_elem: SyncedElement = None):
        return self.sync_objs(sync_model, ELN_TYPE.A, obj_a, obj_b, commit, sync_elem)

    def sync_obj_b_to_a(self, obj_b: dict, obj_a: dict, sync_model: SyncObjModel, commit=True,
                        sync_elem: SyncedElement = None):
        return self.sync_objs(sync_model, ELN_TYPE.B, obj_b, obj_a, commit, sync_elem)

    def _read_eln(self, src_eln: ELN_TYPE, elem, sync_model,
                  parent_element: SyncedElement | None = None) -> SyncedElement | None:
        se = SyncedElement()
        se.elem_a_id = elem.get('id', 0)
        se.elem_b_id = elem.get('id', 0)
        return se


class ProductiveSyncManager(CoreSyncManager):
    def __init__(self, process: SyncProcess):
        super().__init__(process.connection)
        self._process = process

    def _get_sync_elem(self, src_eln: ELN_TYPE, id, sync_model: SyncObjModel, elem_sync_name=None,
                       parent_element: SyncedElement | None = None):
        SyncedElement_args = {'sync_model_id': sync_model.pk, 'parent_element': parent_element}

        id_key = f'elem_{src_eln}_id'
        SyncedElement_args[id_key] = id
        try:
            sync_elem = SyncedElement.objects.get(**SyncedElement_args)
            return sync_elem, False
        except Exception as e:
            if elem_sync_name is not None:
                if parent_element is not None:
                    SyncedElement_by_name_args = {'sync_model_id': sync_model.pk, 'parent_element': parent_element}
                else:
                    SyncedElement_by_name_args = {'sync_model_id': sync_model.pk, 'elem_sync_name': elem_sync_name}
                try:
                    sync_elem = SyncedElement.objects.get(**SyncedElement_by_name_args)
                    setattr(sync_elem, id_key, id)
                    sync_elem.save()
                    return sync_elem, True
                except:
                    pass
        return SyncedElement.objects.create(elem_sync_name=elem_sync_name, **SyncedElement_args), True

    def _read_eln(self, src_eln: ELN_TYPE, elem, sync_model,
                  parent_element: SyncedElement | None = None) -> SyncedElement | None:
        sync_key = getattr(sync_model, f'{src_eln}_sync_key')
        try:
            last_update = elem['last_update']
            dt_last_update = make_aware(datetime.fromtimestamp(last_update))
            if dt_last_update < self.sync_options[src_eln].get_datetime_threshold():
                return None

            sync_value = _get_one_single_target_for_key(elem, sync_key)
            if sync_key is not None and sync_value is None:
                raise Exception('If a sync key is set then the value sould not be None')
            sync_elem, needs_update = self._get_sync_elem(src_eln, elem.get('id'), sync_model,
                                                          sync_value, parent_element)

            if not needs_update:
                if self.last_process is not None:
                    needs_update = self.last_process.sync_done_time < dt_last_update
                else:
                    needs_update = True

            if needs_update:
                sync_elem.process = self.process
                sync_elem.save()
                self.sync_objs(sync_model=sync_model, src_eln=src_eln, obj_src=elem, commit=True,
                               sync_elem=sync_elem, last_update=dt_last_update)
                SyncLog.objects.create(process=self.process,
                                       element=sync_elem,
                                       eln=self.eln_connection.get_eln_type(src_eln),
                                       text=f"Successfully read {getattr(sync_model, f'{src_eln}_model_name')}: {sync_value}!",
                                       type_log='i')
            else:
                SyncLog.objects.create(process=self.process,
                                       element=sync_elem,
                                       eln=self.eln_connection.get_eln_type(src_eln),
                                       text=f"{getattr(sync_model, f'{src_eln}_model_name')}: {sync_value} up to date!",
                                       type_log='i')

        except Exception as e:
            extracted_list = traceback.extract_tb(e.__traceback__)
            if settings.DEBUG:
                traceback.print_tb(e.__traceback__)
                e_text = '\n'.join([item for item in traceback.StackSummary.from_list(extracted_list).format()])
            else:
                e_text = e.__str__()
            SyncLog.objects.create(process=self.process,
                                   eln=self.eln_connection.get_eln_type(src_eln),
                                   text=f"Error reading {getattr(sync_model, f'{src_eln}_model_name')}: {elem['id']} from {src_eln}:  {e}\n{e_text}",
                                   type_log='e')
            return None

        return sync_elem

    def _write_eln(self, sync_model: SyncObjModel, sync_elements: Iterable[SyncedElement] = None):
        if sync_elements is None:
            sync_elements: Iterable[SyncedElement] = self.process.elements.filter(sync_model=sync_model)
        for sync_element in sync_elements:
            try:
                (elements, needs_save) = self._find_sync_element(sync_model, sync_element)
                last_update_a = elements[ELN_TYPE.A]['last_update']
                dt_last_update_a = make_aware(datetime.fromtimestamp(last_update_a))
                last_update_b = elements[ELN_TYPE.B]['last_update']
                dt_last_update_b = make_aware(datetime.fromtimestamp(last_update_b))

                for sync_field in sync_element.syncedproperty_set.filter(creating_process=self.process).order_by('key_a', 'created_at'):
                    a_key = sync_field.key_a
                    b_key = sync_field.key_b
                    if (self.sync_options[ELN_TYPE.A].rw_type == 'w' and
                            (sync_field.written_by == 'b' or
                            self._start_time < dt_last_update_a)):
                        needs_save[ELN_TYPE.A] = True
                        _set_target_for_key(elements[ELN_TYPE.A], a_key, sync_field.value_a)

                    if (self.sync_options[ELN_TYPE.B].rw_type == 'w' and
                            (sync_field.written_by == 'a' or
                             self._start_time < dt_last_update_b)):
                        needs_save[ELN_TYPE.B] = True
                        _set_target_for_key(elements[ELN_TYPE.B], b_key, sync_field.value_b)

                if self.sync_options[ELN_TYPE.A].rw_type == 'w' and needs_save[ELN_TYPE.A]:
                    self.sync_manager[ELN_TYPE.A].write(sync_model.a_model_name, elements[ELN_TYPE.A])
                    SyncLog.objects.create(process=self.process,
                                           element=sync_element,
                                           eln=self.eln_connection.get_eln_type(ELN_TYPE.A),
                                           text=f"Successfully synced {sync_element.elem_sync_name}:",
                                           type_log='i')
                if self.sync_options[ELN_TYPE.B].rw_type == 'w' and needs_save[ELN_TYPE.B]:
                    self.sync_manager[ELN_TYPE.B].write(sync_model.b_model_name, elements[ELN_TYPE.B])

                    SyncLog.objects.create(process=self.process,
                                           element=sync_element,
                                           eln=self.eln_connection.get_eln_type(ELN_TYPE.B),
                                           text=f"Successfully synced {sync_element.elem_sync_name}:",
                                           type_log='i')
            except Exception as e:
                extracted_list = traceback.extract_tb(e.__traceback__)
                if settings.DEBUG:
                    traceback.print_tb(e.__traceback__)
                    e_text = '\n'.join([item for item in traceback.StackSummary.from_list(extracted_list).format()])
                else:
                    e_text = ''
                SyncLog.objects.create(process=self.process,
                                       element=sync_element,
                                       eln=None,
                                       text=f"Error syncing {sync_element.elem_sync_name}.\n-> {e.__str__()}",
                                       type_log='e')

    def run(self):
        self._start_time = make_aware(datetime.now())
        self.process.sync_time = self._start_time
        SyncLog.objects.create(process=self.process,
                               eln=None,
                               text=f"Started syncing.",
                               type_log='i')

        for sync_model in self.eln_connection.syncobjmodel_set.all().order_by('order'):
            for elem in self.sync_manager[ELN_TYPE.A].read(sync_model.a_model_name):
                self._read_eln(ELN_TYPE.A, elem, sync_model)
            for elem in self.sync_manager[ELN_TYPE.B].read(sync_model.b_model_name):
                self._read_eln(ELN_TYPE.B, elem, sync_model)

        for sync_model in self.eln_connection.syncobjmodel_set.order_by('order'):
            self._write_eln(sync_model)


        self.sync_manager[ELN_TYPE.B].clean_up()
        self.sync_manager[ELN_TYPE.A].clean_up()

        self.process.sync_done_time = make_aware(datetime.now())
        self.process.save()

        SyncLog.objects.create(process=self.process,
                               eln=None,
                               text=f"Finished syncing.",
                               type_log='i')
        self.eln_connection.last_sync = self.process.sync_done_time
        self.eln_connection.save()
        return self.process
