import os
import re

from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand

from django.core import management

from ElnAdapter import settings




class Command(BaseCommand):
    help = "Add a new platform."

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._replace_map = {}
        self._todos = {}
    def add_arguments(self, parser):
        parser.add_argument('-p', '--platform', type=str, help='The platform name you would like to add [as CamelCase]')

    def _copy_and_prepare(self, src, des_dir):
        fin = open(src, "rt", encoding='utf-8')


        filename = os.path.basename(src)
        des = os.path.join(des_dir, filename)
        for (key, rep) in self._replace_map.items():
            des = des.replace(key, rep)

        os.makedirs(os.path.dirname(des), exist_ok=True)
        fout = open(des, "wt", encoding='utf-8')

        for idx, line in enumerate(fin):
            for (key, rep) in self._replace_map.items():
                line = line.replace(key, rep)
                if "ToDo" in line:
                    file_link = f'{os.path.abspath(des)}:{idx + 1}'
                    self._todos[file_link] = re.sub(r'^[#\s]+', '', line)

            fout.write(line)

        fin.close()
        fout.close()


    def _copy(self, app_name):
        src = './xxx_app'
        dest = f'./{app_name}'
        if os.path.isdir(src):
            for root, dirs, files in os.walk(src):
                if '__pycache__' in dirs:
                    dirs.remove('__pycache__')
                rel_path = os.path.relpath(root, src)
                for file in files:
                    self._copy_and_prepare(os.path.join(root, file),
                                     os.path.join(dest, rel_path))

    def _print_todo(self):
        for (file, content) in self._todos.items():
            self.stdout.write(file)
            self.stdout.write(f"-> {content}")

    def handle(self, *args, **ops):
        platform = ops.get('platform')
        if platform is None:
            platform = input("The platform name you would like to add [as CamelCase]:")

        sc_platform = re.sub(r'(?<!^)(?=[A-Z])', '_', platform).lower()
        cc_platform = "".join(x.capitalize() for x in sc_platform.lower().split("_"))
        mc_platform = "-".join(sc_platform.lower().split("_"))
        app_name = f'{sc_platform}_app'

        management.call_command('startapp', app_name)
        self._todos[f"{settings.__file__}:44"] = f"ToDo: FIRST add {app_name} to the installed apps"

        settings.INSTALLED_APPS += (app_name,)
        apps.apps_ready = apps.models_ready = apps.loading = apps.ready = False
        apps.clear_cache()
        apps.populate((app_name,))

        management.call_command('sdc_cc', '-a', app_name, '-c', 'settings_' + sc_platform)
        self._replace_map = {'xxx': sc_platform, 'Xxx': cc_platform, 'x-x-x': mc_platform}
        self._copy(app_name)
        self._print_todo()



