from django.core.management.base import BaseCommand

from main_app.models import MappingProperty


class Command(BaseCommand):

    def handle(self, *args, **options):
        for mp in MappingProperty.objects.filter(data_type='atomic'):
            try:
                print(f'Compiling: {mp.identifier} ({mp.a_to_b_operator}, {mp.b_to_a_operator})')
                MappingProperty.compile_operator(mp.a_to_b_xml, mp.a_to_b_operator)
                MappingProperty.compile_operator(mp.b_to_a_xml, mp.b_to_a_operator)
            except Exception as e:
                print(str(e))
