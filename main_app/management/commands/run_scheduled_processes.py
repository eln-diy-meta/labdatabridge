from django.core.management import BaseCommand

from main_app.models import SyncProcess, SyncLog, ElnConnection, ProcessKey, SyncedProperty
from main_app.sync_tools.manager import ProductiveSyncManager


def scheduled_job():
    pk, c = ProcessKey.objects.get_or_create(pk=1)
    if not pk.check_lock():
        return
    print("Starting Crontab")

    for con in ElnConnection.objects.filter(is_committed=True, is_active=True):
        process = SyncProcess.objects.create(connection=con)
        try:
            manger = ProductiveSyncManager(process)
            manger.run()
            if len(SyncedProperty.objects.filter(creating_process=process)) == 0:
                process.delete()
        except Exception as e:
            SyncLog.objects.create(process=process,
                                   text='Synchronisation Failed! Maybe the ELNs are not reachable!',
                                   type_log='e')

    pk.set_done()

class Command(BaseCommand):

    def handle(self, *args, **options):
        scheduled_job()