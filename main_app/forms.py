from sdc_core.sdc_extentions.import_manager import import_function

from main_app.models import SyncedProperty
from main_app.models import SyncedElement
from main_app.models import SyncLog
from main_app.models import SyncProcess
from main_app.models import MappingProperty
from main_app.models import SyncObjModel
from django import forms
from main_app.models import ElnConnection
from main_app.models import ElnType
from django.forms.models import ModelForm
from django.db.models import Q

from sync_tools.options import ELN_TYPE


# Form Model ElnType

class ElnTypeForm(ModelForm):
    class Meta:
        model = ElnType
        fields = "__all__"


# Form Model ElnConnection

class ElnConnectionForm(ModelForm):
    is_committed = forms.CharField(widget=forms.HiddenInput())
    is_active = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = ElnConnection
        fields = ["eln_a", "eln_b", "is_committed", "is_active"]

    def clean(self):
        super().clean()
        if not self.cleaned_data['is_committed'] and self.cleaned_data['is_active']:
            self.add_error("is_active", f"The instance hasn't been committed. Therefore it cannot be activated!")
        if self.cleaned_data['is_committed']:
            for eln_pos in [ELN_TYPE.A, ELN_TYPE.B]:
                eln_cleand_data = self.cleaned_data[f'eln_{eln_pos}']
                if eln_cleand_data is not None:
                    ets = ElnType.objects.filter(~Q(pk=eln_cleand_data.pk))
                    for et in ets:
                        Connnection = import_function(et.model_import_path)
                        Connnection.objects.filter(uuid=self.instance.uuid, connection_position=eln_pos).delete()

    def clean_is_active(self):
        if isinstance(self.cleaned_data['is_active'], str):
            return self.cleaned_data['is_active'].lower() == 'true'
        return self.cleaned_data['is_active']

    def clean_is_committed(self):
        if isinstance(self.cleaned_data['is_committed'], str):
            return self.cleaned_data['is_committed'].lower() == 'true'
        return self.cleaned_data['is_committed']


# Form Model SyncObjModel

class SyncObjModelForm(ModelForm):
    class Meta:
        model = SyncObjModel
        exclude = ("connection", "last_update",)
        widgets = {'locked': forms.HiddenInput()}

    a_model_name = forms.ChoiceField(choices=[])
    b_model_name = forms.ChoiceField(choices=[])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Update choices based on instance values
        instance: SyncObjModel = kwargs.get('instance')
        if instance:
            # Customize this logic based on your requirements
            a_dynamic_choices = instance.connection.get_sync_manager(ELN_TYPE.A).syncable_models()
            self.fields['a_model_name'].choices = a_dynamic_choices
            b_dynamic_choices = instance.connection.get_sync_manager(ELN_TYPE.B).syncable_models()
            self.fields['b_model_name'].choices = b_dynamic_choices

    def clean_a_model_name(self):
        if self.instance.elements.count() > 0:
            return self.instance.a_model_name
        else:
            return self.cleaned_data['a_model_name']

    def clean_b_model_name(self):
        if self.instance.elements.count() > 0:
            return self.instance.b_model_name
        else:
            return self.cleaned_data['b_model_name']

    def clean_locked(self):
        if self.instance.locked:
            return self.instance.locked
        else:
            return self.cleaned_data['locked']


class SyncObjModelUnlockForm(ModelForm):
    class Meta:
        model = SyncObjModel
        fields = ("locked",)


class SyncObjModelCreateForm(ModelForm):
    class Meta:
        model = SyncObjModel
        fields = ("connection",)

    def save(self, commit=True):
        # Call the parent class's save() method to save the instance
        instance: SyncObjModel = super().save(commit=False)

        # Your custom logic after saving the instance
        # For example, you can perform additional database operations, send notifications, etc.

        if commit:
            a_dynamic_choices = self.instance.connection.get_sync_manager(ELN_TYPE.A).syncable_models()
            instance.a_model_name = a_dynamic_choices[0][0]
            b_dynamic_choices = self.instance.connection.get_sync_manager(ELN_TYPE.B).syncable_models()
            instance.b_model_name = b_dynamic_choices[0][0]
            instance.save()


# Form Model MappingProperty

class MappingPropertyForm(ModelForm):
    a_to_b_xml = forms.CharField(widget=forms.HiddenInput())
    b_to_a_xml = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)  # populates the post
        i = self.instance

    class Meta:
        model = MappingProperty
        exclude = ("sync_model", 'parent_element', 'a_to_b_operator', 'b_to_a_operator')

    def clean(self):
        super().clean()
        if self.instance.sync_model.locked:
            self.add_error(None, "Model is locked")

        if self.cleaned_data['data_type'] == 'atomic':
            res_a_to_b = MappingProperty.compile_operator(self.cleaned_data['a_to_b_xml'],
                                                          self.instance.a_to_b_operator)
            if res_a_to_b != 0:
                self.add_error('data_type', f'The ELN A to ELN B operator is not compilable!')
            res_b_to_a = MappingProperty.compile_operator(self.cleaned_data['b_to_a_xml'],
                                                          self.instance.b_to_a_operator)
            if res_b_to_a != 0:
                self.add_error('data_type', f'The ELN B to ELN A operator is not compilable!')

        if self.instance.parent_element is not None:
            if self.cleaned_data.get('a_key') is None or not self.cleaned_data['a_key'].startswith(
                    self.instance.parent_element.a_key):
                self.add_error('a_key', f'Key must start with {self.instance.parent_element.a_key}')
            if self.cleaned_data.get('b_key') is None or not self.cleaned_data['b_key'].startswith(
                    self.instance.parent_element.b_key):
                self.add_error('b_key', f'Key must start with {self.instance.parent_element.b_key}')


# Form Model MappingProperty

class CreateMappingPropertyForm(ModelForm):
    class Meta:
        model = MappingProperty
        fields = ("sync_model", 'parent_element')

    def clean(self):
        super().clean()
        if self.cleaned_data['sync_model'].locked:
            self.add_error(None, "Model is locked")


# Form Model SyncProcess

class SyncProcessForm(ModelForm):
    class Meta:
        model = SyncProcess
        fields = ("connection",)


# Form Model SyncLog

class SyncLogForm(ModelForm):
    class Meta:
        model = SyncLog
        fields = "__all__"


# Form Model SyncedElement

class SyncedElementForm(ModelForm):
    class Meta:
        model = SyncedElement
        fields = "__all__"


# Form Model SyncedProperty

class SyncedPropertyForm(ModelForm):
    class Meta:
        model = SyncedProperty
        fields = "__all__"
