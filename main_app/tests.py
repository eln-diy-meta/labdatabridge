import os
import shutil
import time
from datetime import timedelta

from chemotion_api import Instance as CI
from herbie_api import Instance as HI

from django.conf import settings
from django.test import TestCase, override_settings
from django.utils import timezone
from chemotion_app.models import ChemotionConnection, ChemotionSettings
from herbie_app.models import HerbieConnection

from main_app.models import SyncedElement, SyncedProperty, SyncObjModel, ElnConnection, ElnType, MappingProperty, \
    SyncProcess, SyncLog
from main_app.sync_tools.manager import ProductiveSyncManager
from main_app.sync_tools.model_graph import ModelGraph
from sync_tools.options import ELN_TYPE


# Create your tests here.

@override_settings(BRIDGE_OPERATORS_DIR="test_operators")
class SyncTest(TestCase):
    fixtures = ['chemotion_herbie_eln_type.json', ]

    def setUp(self):
        c1 = ElnConnection.objects.create(is_committed=True, eln_a=ElnType.objects.get(pk=1),
                                          eln_b=ElnType.objects.get(pk=2))
        self.c1 = c1
        cs = ChemotionSettings.objects.create(uuid=c1.uuid, collection='/Main', collection_id=4, is_sync=False)

        self.cc = ChemotionConnection.objects.create(uuid=c1.uuid,
                                                     name='LC',
                                                     is_connected=True,
                                                     url='http://localhost:3354',
                                                     user='MSU',
                                                     token='eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdF9uYW1lIjoiTWFydGluIiwidXNlcl9pZCI6MiwibGFzdF9uYW1lIjoiU3Rhcm1hbiIsImV4cCI6MTczMzIyMzM4Nn0.oarobFn26N4iBL_XceNojmRcnh1SqLNU2cDbDmxtXqY',
                                                     chemotion_settings=cs,
                                                     connection_model=c1,
                                                     connection_position=ELN_TYPE.A)

        self.hc = HerbieConnection.objects.create(uuid=c1.uuid,
                                                  name='HC',
                                                  is_connected=True,
                                                  verify_ssl=True,
                                                  url='http://localhost:4500',
                                                  token='Token ed789231902df05fa7b01caa3dbe25e8f7943227ba0d495e492b212f8d3bd5b1',
                                                  connection_model=c1,
                                                  connection_position=ELN_TYPE.B)

        self.update_date_time()

        ab = SyncObjModel.objects.create(connection=c1,
                                         name='PM',
                                         a_model_name="chmotion:type/reaction/1.8.2",
                                         b_model_name="post-modification",
                                         a_sync_key='"Properties"."name"',
                                         b_sync_key='"values"."reaction_name"',
                                         order=1,
                                         locked=True)

        def addMapping(a_key, b_key, data_type, identifier):
            MappingProperty.objects.create(sync_model=ab, a_key=a_key, b_key=b_key, data_type=data_type,
                                           identifier=identifier)

        addMapping('"Properties"."name"', '"values"."reaction_name"', 'atomic', 'Reaction Name')
        addMapping('"Properties"."starting_materials"."0"."Properties"."name"', '"values"."old_product"."public_id"',
                   'atomic', 'Starting Material')
        addMapping('"Properties"."solvents"', '"values"."modifying_solvents"', 'array', 'Solvents')
        addMapping('"Properties"."solvents"."%d"."Properties"."external_label"',
                   '"values"."modifying_solvents"."%d"."values"."solvent_label"', 'atomic', 'Solvent Label')

        for mp in MappingProperty.objects.all():
            MappingProperty.compile_operator(mp.a_to_b_xml, mp.a_to_b_operator)
            MappingProperty.compile_operator(mp.b_to_a_xml, mp.b_to_a_operator)

    def update_date_time(self):
        now = timezone.now() - timedelta(hours=5)
        self.cc.sync_settings.sync_all_existing = False
        self.cc.sync_settings.datetime_threshold = now
        self.hc.sync_settings.sync_all_existing = False
        self.hc.sync_settings.datetime_threshold = now
        self.cc.sync_settings.save()
        self.hc.sync_settings.save()

    def tearDown(self):
        # MappingProperty.objects.all().delete()
        operators_dir = os.path.join(os.path.dirname(__file__), settings.BRIDGE_OPERATORS_DIR)
        shutil.rmtree(operators_dir)

    def test_herbie_to_chemotion(self):
        self.update_date_time()
        SyncProcess.objects.all().delete()
        hi = HI('http://localhost:4500', 'Token ed789231902df05fa7b01caa3dbe25e8f7943227ba0d495e492b212f8d3bd5b1',
                verify_ssl=True)
        hi.setup_env()
        reaction = hi.get_post_modification(15)
        reaction.to_draft()
        reaction.values['modifying_solvents'][-1].values['solvent_label'] = 'Herbie Solvent 1'
        reaction.values['modifying_solvents'].add_new()
        reaction.values['modifying_solvents'][-1].values = {
            'solvent': 'Acetic acid',
            'solvent_label': 'Herbie Solvent 2',
            'volume_ml': 33,
            'volume_ratio_percent': 44
        }

        reaction.values['old_product'] = hi.get_materials()[3].values
        reaction.save_draft()
        reaction.publish()

        process = SyncProcess.objects.create(connection=self.c1)

        manger = ProductiveSyncManager(process)
        manger.run()
        all_vals = [p.value_a for p in SyncedProperty.objects.all().order_by('value_a')]
        for v in ['13 ss', 'Herbie Solvent 1', 'Herbie Solvent 2', 'React 4']:
            assert v in all_vals

    def test_chemotion_to_herbie(self):
        self.update_date_time()
        SyncProcess.objects.all().delete()
        ci = CI('http://localhost:3354').login('MSU', '1234qweR!')
        reaction = ci.get_reaction(3)
        material = reaction.properties['starting_materials'][0]
        material.load()
        material.properties['name'] = 'Chemotion ST_MA1'
        material.save()
        solvents = reaction.properties['solvents'][0]
        solvents.properties['external_label'] = 'Chemotion Solvent 1'
        solvents.save()
        # reaction.properties['solvents'].append(solvents)
        reaction.save()
        # reaction.load()
        # solvents = reaction.properties['solvents'][1]
        # solvents.properties['external_label'] = 'Chemotion Solvent 2'
        # solvents.save()

        process = SyncProcess.objects.create(connection=self.c1)

        manger = ProductiveSyncManager(process)
        manger.run()

        assert [p.value_a for p in SyncedProperty.objects.all().order_by('value_a')]  == ['Chemotion ST_MA1',
                                                                                         'Chemotion Solvent 1',
                                                                                         'React 4']
        assert len(SyncLog.objects.all()) == 4

        time.sleep(10)

        reaction = ci.get_reaction(3)
        reaction.properties['name'] = 'Chemotion reaction 1'
        reaction.save()
        process = SyncProcess.objects.create(connection=self.c1)

        manger = ProductiveSyncManager(process)
        manger.run()
        assert [p.value_a for p in SyncedProperty.objects.filter(creating_process=process).order_by('value_a')] == ['Chemotion reaction 1']

        time.sleep(1)
        self.update_date_time()
        SyncProcess.objects.all().delete()

        reaction = ci.get_reaction(3)
        reaction.properties['name'] = 'React 4'
        reaction.save()
        process = SyncProcess.objects.create(connection=self.c1)

        manger = ProductiveSyncManager(process)
        manger.run()
        assert 'React 4' in [p.value_a for p in SyncedProperty.objects.all().order_by('value_a')]


@override_settings(BRIDGE_OPERATORS_DIR="test_operators")
class GraphTest(TestCase):
    fixtures = ['chemotion_herbie_eln_type.json', ]

    def setUp(self):
        xml_add_b = '<xml><block type="output" id="YYYY_" x="63" y="180"><value name="NAME"><block type="text_join" id="ru*4Gr6c-z;}#8s%3*AS"><mutation items="2"></mutation><value name="ADD0"><block type="convert_to" id="y-=Yj*)boR;{ECYop_q1"><field name="to_type">str</field><value name="input_variable"><block type="input" id="27WCk5at([TC;S#79^[F"></block></value></block></value><value name="ADD1"><block type="text" id="bI:q`,dMPtLOm)8baj^_"><field name="TEXT">B</field></block></value></block></value></block></xml>'
        xml_add_a = '<xml><block type="output" id="YYYY_" x="63" y="180"><value name="NAME"><block type="text_join" id="ru*4Gr6c-z;}#8s%3*AS"><mutation items="2"></mutation><value name="ADD0"><block type="convert_to" id="y-=Yj*)boR;{ECYop_q1"><field name="to_type">str</field><value name="input_variable"><block type="input" id="27WCk5at([TC;S#79^[F"></block></value></block></value><value name="ADD1"><block type="text" id="bI:q`,dMPtLOm)8baj^_"><field name="TEXT">A</field></block></value></block></value></block></xml>'
        c1 = ElnConnection.objects.create(is_committed=True, eln_a=ElnType.objects.get(pk=1),
                                          eln_b=ElnType.objects.get(pk=2))
        c2 = ElnConnection.objects.create(is_committed=True, eln_a=ElnType.objects.get(pk=2),
                                          eln_b=ElnType.objects.get(pk=2))
        ab = SyncObjModel.objects.create(connection=c1, a_model_name="A", b_model_name="B", locked=True)
        ac = SyncObjModel.objects.create(connection=c1, a_model_name="A", b_model_name="C", locked=True)
        bc = SyncObjModel.objects.create(connection=c2, a_model_name="B", b_model_name="C", locked=True)
        eb = SyncObjModel.objects.create(connection=c1, a_model_name="E", b_model_name="B", locked=True)
        ed = SyncObjModel.objects.create(connection=c1, a_model_name="E", b_model_name="D", locked=True)
        dc = SyncObjModel.objects.create(connection=c2, a_model_name="D", b_model_name="C", locked=True)
        ec = SyncObjModel.objects.create(connection=c1, a_model_name="E", b_model_name="C", locked=True)
        self.src_model = SyncObjModel.objects.create(connection=c1, a_model_name="A", b_model_name="D", locked=False)

        MappingProperty.objects.create(identifier="Name", a_key='"A"."Name"', b_key='"B"."Name"', sync_model=ab,
                                       a_to_b_xml=xml_add_a, b_to_a_xml=xml_add_b)
        MappingProperty.objects.create(identifier="Name", a_key='"A"."Name"', b_key='"C"."Name"', sync_model=ac,
                                       a_to_b_xml=xml_add_b, b_to_a_xml=xml_add_a)
        MappingProperty.objects.create(identifier="Name", a_key='"B"."Name"', b_key='"C"."Name"', sync_model=bc)
        MappingProperty.objects.create(identifier="Name", a_key='"E"."Name"', b_key='"B"."Name"', sync_model=eb)
        MappingProperty.objects.create(identifier="Name", a_key='"E"."Name"', b_key='"D"."Name"', sync_model=ed)
        MappingProperty.objects.create(identifier="Name", a_key='"D"."Name"', b_key='"C"."Name"', sync_model=dc,
                                       b_to_a_xml=xml_add_a, a_to_b_xml=xml_add_b)
        MappingProperty.objects.create(identifier="Name", a_key='"E"."Name"', b_key='"C"."Name"', sync_model=ec)

        MappingProperty.objects.create(identifier="Age", a_key='"A"."Age"', b_key='"B"."Age"', sync_model=ab,
                                       a_to_b_xml=xml_add_a, b_to_a_xml=xml_add_a)
        MappingProperty.objects.create(identifier="Age", a_key='"B"."Age"', b_key='"C"."Age"', sync_model=bc,
                                       a_to_b_xml=xml_add_a, b_to_a_xml=xml_add_a)
        MappingProperty.objects.create(identifier="Age", a_key='"D"."Age"', b_key='"C"."Age"', sync_model=dc,
                                       a_to_b_xml=xml_add_a, b_to_a_xml=xml_add_a)

        for mp in MappingProperty.objects.all():
            MappingProperty.compile_operator(mp.a_to_b_xml, mp.a_to_b_operator)
            MappingProperty.compile_operator(mp.b_to_a_xml, mp.b_to_a_operator)

    def tearDown(self):
        # MappingProperty.objects.all().delete()
        operators_dir = os.path.join(os.path.dirname(__file__), settings.BRIDGE_OPERATORS_DIR)
        shutil.rmtree(operators_dir)

    def test_find_all_paths(self):

        graph = ModelGraph()
        graph.load()
        path_text, path_id = graph.find_path(self.src_model)

        porp_path, prop_path_names = graph.property_path(path_text, path_id, self.src_model)

        assert len(prop_path_names) == len(porp_path)
        assert len(prop_path_names) == 2
        assert prop_path_names[0][0] == 'Chemotion:A<->Herbie:C<->Herbie:D'
        assert prop_path_names[1][0] == 'Chemotion:A<->Herbie:B<->Herbie:C<->Herbie:D'
        for expected_path in ['Chemotion:A<->Herbie:B<->Herbie:C<->Herbie:D',
                              'Chemotion:A<->Herbie:B<->Herbie:C<->Chemotion:E<->Herbie:D',
                              'Chemotion:A<->Herbie:B<->Chemotion:E<->Herbie:D',
                              'Chemotion:A<->Herbie:B<->Chemotion:E<->Herbie:C<->Herbie:D',
                              'Chemotion:A<->Herbie:C<->Herbie:B<->Chemotion:E<->Herbie:D',
                              'Chemotion:A<->Herbie:C<->Herbie:D', 'Chemotion:A<->Herbie:C<->Chemotion:E<->Herbie:D']:
            assert expected_path in path_text

        graph.create_new_mappings(self.src_model)

        mapping = self.src_model.mappingproperty_set.get(a_key='"A"."Name"')
        assert mapping.operator_from_x(ELN_TYPE.A)('TEST') == 'TESTBA'
        assert mapping.operator_from_x(ELN_TYPE.B)('TEST') == 'TESTAB'
        assert mapping.b_key == '"D"."Name"'

        mapping = self.src_model.mappingproperty_set.get(a_key='"A"."Age"')
        assert mapping.operator_from_x(ELN_TYPE.A)('TEST') == 'TESTAAA'
        assert mapping.operator_from_x(ELN_TYPE.B)('TEST') == 'TESTAAA'
        assert mapping.b_key == '"D"."Age"'
