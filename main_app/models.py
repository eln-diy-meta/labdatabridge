import importlib
import json
import os
from typing import Type

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import QuerySet
from django.dispatch import receiver
from django.utils import timezone
from nodejs import node
from sdc_core.sdc_extentions.import_manager import import_function
from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models
from sync_tools.manager import get_manager, AbstractManager
import uuid

from sync_tools.options import ELN_TYPE
from sync_tools.parser_manager import SUPPORTED_TYPES

UserModel = get_user_model()


# Create your models here.

class ElnTypeSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = "Id"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class ElnType(models.Model, SdcModel):
    name = models.CharField(max_length=255, null=True)
    model_import_path = models.CharField(max_length=255, null=True,
                                         help_text="For Example 'chemotion.app.models.ChemotionSettings'")

    image = models.ImageField(null=True, blank=True)
    settings_controller = models.CharField(max_length=255, default="")

    def __str__(self):
        return self.name

    edit_form = "main_app.forms.ElnTypeForm"
    create_form = "main_app.forms.ElnTypeForm"
    html_list_template = "main_app/models/ElnType/ElnType_list.html"
    html_detail_template = "main_app/models/ElnType/ElnType_details.html"

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = ElnTypeSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


class ElnConnectionSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"), ("eln_a", "A"), ("eln_b", "B"))
    PLACEHOLDER = "ELN"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id", 'eln_a__name', 'eln_b__name')


class ElnConnection(models.Model, SdcModel):
    uuid = models.CharField(max_length=255, default=uuid.uuid4)
    is_committed = models.BooleanField(default=False, blank=True)
    user = models.ForeignKey(UserModel, null=True, on_delete=models.CASCADE)
    eln_a = models.ForeignKey(ElnType, blank=True, null=True, on_delete=models.CASCADE, related_name="connections_as_a")
    eln_b = models.ForeignKey(ElnType, blank=True, null=True, on_delete=models.CASCADE, related_name="connections_as_b")

    name_a = models.CharField(max_length=255, default="")
    name_b = models.CharField(max_length=255, default="")

    last_sync = models.DateTimeField(null=True)
    is_active = models.BooleanField(default=False)

    edit_form = "main_app.forms.ElnConnectionForm"
    create_form = "main_app.forms.ElnConnectionForm"
    html_list_template = "main_app/models/ElnConnection/ElnConnection_list.html"
    html_detail_template = "main_app/models/ElnConnection/ElnConnection_details.html"
    html_form_template = "main_app/models/ElnConnection/ElnConnection_form.html"

    def get_eln_type(self, eln_type: ELN_TYPE) -> ElnType:
        if (eln_type == ELN_TYPE.A):
            return self.eln_a
        elif (eln_type == ELN_TYPE.B):
            return self.eln_b
        else:
            raise ValueError(f"The argument eln_type has to be in [a, b] not {eln_type}")

    def get_connections(self, eln_type: ELN_TYPE):
        Connection = import_function(self.get_eln_type(eln_type).model_import_path)
        obj, c = Connection.objects.get_or_create(connection_position=eln_type, connection_model=self,
                                                  uuid=self.uuid)
        if (eln_type == ELN_TYPE.A and self.name_a != obj.name):
            self.name_a = obj.name
            self.save()
        elif (eln_type == ELN_TYPE.B and self.name_b != obj.name):
            self.name_b = obj.name
            self.save()

        return obj, c

    def get_a_connections(self):
        return self.get_connections(ELN_TYPE.A)[0]

    def get_b_connections(self):
        return self.get_connections(ELN_TYPE.B)[0]

    def get_a_sync_manager(self) -> AbstractManager:
        return self.get_sync_manager(ELN_TYPE.A)

    def get_b_sync_manager(self) -> AbstractManager:
        return self.get_sync_manager(ELN_TYPE.B)

    def get_sync_manager_class(self, eln_type: ELN_TYPE) -> Type[AbstractManager]:
        return get_manager(self.get_eln_type(eln_type).name)

    def get_sync_manager(self, eln_type: ELN_TYPE) -> AbstractManager:
        ManagerClass = self.get_sync_manager_class(eln_type)
        return ManagerClass(self.uuid, eln_type)

    def get_connection_app_names(self):
        res = {ELN_TYPE.A: '', ELN_TYPE.B: ''}
        if self.eln_a is not None:
            res[ELN_TYPE.A] = self.eln_a.model_import_path.split('.')[-1]

        if self.eln_b is not None:
            res[ELN_TYPE.B] = self.eln_b.model_import_path.split('.')[-1]

        return res

    def get_last_sync(self):
        if self.last_sync is None:
            return '-'
        return self.last_sync.isoformat()

    def get_is_active(self):
        return 'ON' if self.is_active else 'OFF'

    def get_number_of_syncs(self):
        return len(self.processes.all())

    def __str__(self):
        return f"{self.user.__str__()}: {self.name_a} ({self.eln_a}) <=> {self.name_b} ({self.eln_b})"

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = ElnConnectionSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return user is not None

    @classmethod
    def get_queryset(cls, user, action, obj):
        if action == 'list_view':
            return cls.objects.filter(user=user, is_committed=True)
        return cls.objects.filter(user=user)


class SyncObjModelSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = "Id"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("name", "a_model_name", "b_sync_key")

    def __init__(self, instances: QuerySet, data=None, *args, **kwargs):
        if (instances and len(instances) > 0):
            con: ElnConnection = instances[0].connection
            self.CHOICES = (
                ('name', f'Identifier'),
                ('a_model_name', f'{con.eln_a} model name'),
                ('b_model_name', f'{con.eln_b} model name'),
            )
        super().__init__(data, *args, **kwargs)


class SyncObjModel(models.Model, SdcModel):
    edit_form = "main_app.forms.SyncObjModelForm"
    create_form = "main_app.forms.SyncObjModelCreateForm"
    html_list_template = "main_app/models/SyncObjModel/SyncObjModel_list.html"
    html_detail_template = "main_app/models/SyncObjModel/SyncObjModel_details.html"
    forms = {
        'unlock': "main_app.forms.SyncObjModelUnlockForm"
    }

    name = models.CharField(max_length=255, default=uuid.uuid4)

    connection = models.ForeignKey(ElnConnection, on_delete=models.CASCADE)

    locked = models.BooleanField(default=False)

    a_model_name = models.CharField(max_length=255, null=True)
    b_model_name = models.CharField(max_length=255, null=True)

    a_sync_key = models.CharField(max_length=255, default=None, null=True, blank=True,
                                  help_text="Please select the JSON key for pairing elements between ELNs A and B by clicking on the corresponding value in the JSON schema below. If you leave either field empty, the system will attempt to create a new complimentary element for each found element.")
    b_sync_key = models.CharField(max_length=255, default=None, null=True, blank=True,
                                  help_text="Please select the JSON key for pairing elements between ELNs A and B by clicking on the corresponding value in the JSON schema below. If you leave either field empty, the system will attempt to create a new complimentary element for each found element.")

    order = models.IntegerField(default=1,
                                help_text="This value defines the order in which sync modes are processed. A lower number indicates earlier processing.")


    def model_name(self, eln: ELN_TYPE):
        if eln == ELN_TYPE.A:
            return self.a_model_name
        if eln == ELN_TYPE.B:
            return self.b_model_name
        raise ValueError(f"The argument eln: {eln} has to be in [{ELN_TYPE.A},{ELN_TYPE.B}]")

    def sync_key(self, eln: ELN_TYPE):
        if eln == ELN_TYPE.A:
            return self.a_sync_key
        if eln == ELN_TYPE.B:
            return self.b_sync_key
        raise ValueError(f"The argument eln: {eln} has to be in [{ELN_TYPE.A},{ELN_TYPE.B}]")

    def __str__(self):
        return self.name

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = SyncObjModelSearchForm(instances=context["instances"], data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return user is not None

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


###############################################
#                 SyncProcess                 #
###############################################

class SyncProcessSearchForm(AbstractSearchForm):
    CHOICES = (("-sync_time", "> Start"), ("sync_time", "< Start"), ("-sync_done_time", "> Done"), ("-sync_done_time", "< Done"))
    PLACEHOLDER = "Time"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("sync_time", "sync_done_time")


class SyncProcess(models.Model, SdcModel):
    edit_form = "main_app.forms.SyncProcessForm"
    create_form = "main_app.forms.SyncProcessForm"
    html_list_template = "main_app/models/SyncProcess/SyncProcess_list.html"
    html_detail_template = "main_app/models/SyncProcess/SyncProcess_details.html"

    connection = models.ForeignKey(ElnConnection, on_delete=models.CASCADE, related_name='processes')
    sync_time = models.DateTimeField(null=True)
    sync_done_time = models.DateTimeField(null=True)
    uuid = models.CharField(max_length=255, default="")

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = SyncProcessSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=12)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        if action == 'list_view':
            return cls.objects.filter(sync_time__isnull=False)
        return cls.objects.all()


#############################################
#                   Mapping                 #
#############################################


def get_default_workspace():
    return render_to_string('main_app/xml/workspace.xml', {uuid: uuid.uuid4().__str__()})


class MappingPropertySearchForm(AbstractSearchForm):
    CHOICES = ()
    PLACEHOLDER = ""

    SEARCH_FIELDS = ("identifier", "a_operator", "b_operator")

    def __init__(self, instances: QuerySet, data=None, *args, **kwargs):
        if (instances and len(instances) > 0):
            con: ElnConnection = instances[0].sync_model.connection
            self.CHOICES = (
                ('identifier', f'Identifier'),
                ('a_operator', f'{con.eln_a} Operator'),
                ('b_operator', f'{con.eln_b} Operator'),
                ('a_key', f'{con.eln_a} Key'),
                ('b_key', f'{con.eln_b} Key'),
            )
            self.DEFAULT_CHOICES = self.CHOICES[0][0]
        super().__init__(data, *args, **kwargs)


class MappingProperty(models.Model, SdcModel):
    edit_form = "main_app.forms.MappingPropertyForm"
    create_form = "main_app.forms.CreateMappingPropertyForm"
    html_list_template = "main_app/models/MappingProperty/MappingProperty_list.html"
    html_detail_template = "main_app/models/MappingProperty/MappingProperty_details.html"

    sync_model = models.ForeignKey(SyncObjModel, on_delete=models.CASCADE, null=True)
    identifier = models.CharField(max_length=255, default='')

    data_type = models.CharField(max_length=255, choices=SUPPORTED_TYPES, default=SUPPORTED_TYPES[0][0])

    a_to_b_operator = models.CharField(max_length=255, default=uuid.uuid4)
    b_to_a_operator = models.CharField(max_length=255, default=uuid.uuid4)

    a_to_b_xml = models.TextField(default=get_default_workspace)
    b_to_a_xml = models.TextField(default=get_default_workspace)

    a_key = models.CharField(max_length=255, verbose_name="Mapping key for model from ELN A", default='')
    b_key = models.CharField(max_length=255, verbose_name="Mapping key for model from ELN B", default='')
    parent_element = models.ForeignKey("self", on_delete=models.CASCADE, related_name='children', null=True,
                                       default=None, blank=True)

    def __str__(self):
        return f'"{self.identifier}"'

    def get_key(self, src_eln: ELN_TYPE):
        if src_eln == ELN_TYPE.A:
            return self.a_key
        return self.b_key

    def get_operator_from(self, src_eln: ELN_TYPE):
        if src_eln == ELN_TYPE.A:
            return self.a_to_b_operator
        return self.b_to_a_operator

    def get_xml_from(self, src_eln: ELN_TYPE):
        if src_eln == ELN_TYPE.A:
            return self.a_to_b_xml
        return self.b_to_a_xml

    def operator_from_x(self, src_eln: ELN_TYPE):
        op_id = self.a_to_b_operator if src_eln == ELN_TYPE.A else self.b_to_a_operator
        string_path = f'main_app.{settings.BRIDGE_OPERATORS_DIR}.{op_id}.handler'
        p, m = string_path.rsplit('.', 1)
        mod = importlib.import_module(p)
        mod = importlib.reload(mod)
        met = getattr(mod, m)
        return met

    def list_name(self):
        if self.identifier == '':
            return '<NO NAME>'
        return self.identifier

    @property
    def operator_a_to_b_file_path(self):
        return os.path.join(os.path.dirname(__file__), f"{settings.BRIDGE_OPERATORS_DIR}/{self.a_to_b_operator}.py")

    @property
    def operator_b_to_a_file_path(self):
        return os.path.join(os.path.dirname(__file__), f"{settings.BRIDGE_OPERATORS_DIR}/{self.b_to_a_operator}.py")

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = MappingPropertySearchForm(instances=context["instances"], data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=20)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.filter(sync_model__connection__user=user)

    @classmethod
    def compile_operator(cls, xml_string: str, uuid_file_name: str):
        operators_dir = os.path.join(os.path.dirname(__file__), settings.BRIDGE_OPERATORS_DIR)
        script_root = os.path.join(settings.BLOCKLY_SCRIPT_ROOT, 'index.js')
        return node.call([script_root, xml_string, uuid_file_name, operators_dir])


@receiver(models.signals.post_delete, sender=MappingProperty)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
   Deletes file from filesystem
   when corresponding `MediaFile` object is deleted.
   """
    for file_uuid in [instance.b_to_a_operator, instance.a_to_b_operator]:
        operator = os.path.join(os.path.dirname(__file__), f"{settings.BRIDGE_OPERATORS_DIR}/{file_uuid}.py")
        if os.path.isfile(operator):
            os.remove(operator)


#############################################
#                   Element                 #
#############################################

class SyncedElementSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = "Id"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class SyncedElement(models.Model, SdcModel):
    edit_form = "main_app.forms.SyncedElementForm"
    create_form = "main_app.forms.SyncedElementForm"
    html_list_template = "main_app/models/SyncedElement/SyncedElement_list.html"
    html_detail_template = "main_app/models/SyncedElement/SyncedElement_details.html"

    process = models.ForeignKey(SyncProcess, on_delete=models.CASCADE, related_name='elements', null=True)
    sync_model = models.ForeignKey(SyncObjModel, on_delete=models.CASCADE, related_name='elements', null=True)
    parent_element = models.ForeignKey("self", on_delete=models.CASCADE, related_name='children', null=True)

    elem_sync_name = models.CharField(max_length=255)
    elem_a_id = models.CharField(max_length=255, null=True)
    elem_b_id = models.CharField(max_length=255, null=True)

    @property
    def elem_ids(self):
        return {ELN_TYPE.A.to_id(): self.elem_a_id, ELN_TYPE.B.to_id(): self.elem_b_id}

    def prop_len(self, p_pk):
        return len(self.syncedproperty_set.filter(creating_process_id=p_pk))

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = SyncedElementSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=2)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.filter(sync_model__connection__user=user)


#############################################
#                   SyncLog                 #
#############################################

class SyncLogSearchForm(AbstractSearchForm):
    CHOICES = (("created_at", "Time"), ("type_log", "Error/Info"), ('eln__name', 'ELN'))
    PLACEHOLDER = "Content"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("text", "eln__name")


LOG_TYPES = (
    ('e', 'Error'),
    ('i', 'Info'),
)


class SyncLog(models.Model, SdcModel):
    edit_form = "main_app.forms.SyncLogForm"
    create_form = "main_app.forms.SyncLogForm"
    html_list_template = "main_app/models/SyncLog/SyncLog_list.html"
    html_detail_template = "main_app/models/SyncLog/SyncLog_details.html"

    process = models.ForeignKey(SyncProcess, on_delete=models.CASCADE, related_name='logs')
    created_at = models.DateTimeField(auto_now_add=True)
    text = models.CharField(max_length=255, default='')
    type_log = models.CharField(max_length=2, choices=LOG_TYPES, default='i')
    eln = models.ForeignKey(ElnType, null=True, on_delete=models.SET_NULL)
    element = models.ForeignKey(SyncedElement, null=True, on_delete=models.SET_NULL)

    def sender(self):
        if self.eln is None:
            return ''
        else:
            return f"{self.eln}:"

    def prop_len(self):
        if self.element is None:
            return 0
        return self.element.prop_len(self.process.pk)

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = SyncLogSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=20)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


class SyncedPropertySearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = "Id"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class SyncedProperty(models.Model, SdcModel):
    edit_form = "main_app.forms.SyncedPropertyForm"
    create_form = "main_app.forms.SyncedPropertyForm"
    html_list_template = "main_app/models/SyncedProperty/SyncedProperty_list.html"
    html_detail_template = "main_app/models/SyncedProperty/SyncedProperty_details.html"

    mapping = models.ForeignKey(MappingProperty, on_delete=models.CASCADE, null=True)
    creating_process = models.ForeignKey(SyncProcess, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    written_by = models.CharField(max_length=1, choices=(('a', 'a'),('b', 'b')), null=True)
    element = models.ForeignKey(SyncedElement, on_delete=models.CASCADE, null=True)
    value_a = models.JSONField(default=dict)
    value_b = models.JSONField(default=dict)
    key_a = models.CharField(max_length=255, default=None, null=True)
    key_b = models.CharField(max_length=255, default=None, null=True)

    def set_value(self, eln_type: ELN_TYPE, src_val, dest_val):
        dst = 'b' if eln_type == ELN_TYPE.A else 'a'
        src = 'a' if eln_type == ELN_TYPE.A else 'b'
        self.written_by = src
        setattr(self, f"value_{src}", src_val)
        setattr(self, f"value_{dst}", dest_val)
        self.save()

    def get_value(self):
        return {
            ELN_TYPE.A: self.value_a,
            ELN_TYPE.B: self.value_b
        }

    def array_idx_disp(self):
        if self.key_a is None:
            return '-'
        return self.key_a

    def value_disp(self):
        return f'<div>{self.creating_process.connection.get_eln_type(ELN_TYPE.A)}:<pre>{json.dumps(self.value_a, indent=2)}</pre>{self.creating_process.connection.get_eln_type(ELN_TYPE.B)}:<pre>{json.dumps(self.value_b, indent=2)}</pre></div>'

    def prev_value_disp(self):
        try:
            cx = SyncedProperty.objects.filter(mapping=self.mapping, element=self.element, key_a=self.key_a,
                                               created_at__lt=self.created_at).latest('created_at')
            return cx.value_disp()
        except:
            return '-'

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = SyncedPropertySearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.filter(mapping__sync_model__connection__user=user)


class ProcessKey(models.Model):
    lock_key = models.IntegerField(default=0)

    def check_lock(self) -> bool:
        ret_val = self.lock_key == 0
        self.lock_key = (self.lock_key + 1) % 50
        self.save()
        return ret_val

    def set_done(self):
        self.lock_key = 0
        self.save()

