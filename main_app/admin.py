from django.contrib import admin
from main_app.models import ElnConnection, ElnType, MappingProperty, SyncObjModel
from sync_tools.options import ELN_TYPE


# Register your models here.
class ElnTypeAdmin(admin.ModelAdmin):
    list_display = ["name", "model_import_path", "image", "settings_controller"]
    fields = ["name", "model_import_path", "image", "settings_controller"]

admin.site.register(ElnType, ElnTypeAdmin)

class ElnConnectionAdmin(admin.ModelAdmin):
    list_display = ["uuid", "name_a", "name_b", "is_committed"]
    fields = [("name_a", "eln_a"), ("name_b", "eln_b"), "uuid", "user", "is_committed"]
    def save_model(self, request, obj, form, change):
        # Your custom logic before saving the model
        # For example, you can modify obj or perform additional actions.
        if obj.eln_a is not None and change:
            name_a = form.cleaned_data.get('name_a')
            con_obj, c = obj.get_connections(ELN_TYPE.A)
            con_obj.name=name_a
            con_obj.save()
        if obj.eln_b is not None and change:
            name_b = form.cleaned_data.get('name_b')
            con_obj, c = obj.get_connections(ELN_TYPE.B)
            con_obj.name=name_b
            con_obj.save()

        # Call the original save_model method to save the model
        super().save_model(request, obj, form, change)

        # Your custom logic after saving the model
        # For example, you can perform additional actions.

admin.site.register(ElnConnection, ElnConnectionAdmin)


# Register your models here.
class MappingPropertyAdmin(admin.ModelAdmin):
    list_display = ["identifier", "data_type", "sync_model"]

admin.site.register(MappingProperty, MappingPropertyAdmin)

class SyncObjModelAdmin(admin.ModelAdmin):
    list_display = ["name", "connection", "locked", "a_model_name", "b_model_name"]

admin.site.register(SyncObjModel, SyncObjModelAdmin)