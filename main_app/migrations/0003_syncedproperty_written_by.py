# Generated by Django 5.0.6 on 2024-06-23 19:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0002_alter_mappingproperty_a_key_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='syncedproperty',
            name='written_by',
            field=models.CharField(choices=[('a', 'a'), ('b', 'b')], max_length=1, null=True),
        ),
    ]
