import {AbstractSDC, app} from 'sdc_client';


class SyncedProcessListController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/synced_process_list/%(pk)s"; //<synced-process-list data-pk=""></synced-process-list>
        this.$process_list = '';
        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk) {
        this.sync_pk = pk;
        this.model = this.newModel('SyncProcess', {'connection__id': pk});
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

    process_list() {
        return this.$process_list
    }

}

app.register(SyncedProcessListController).addMixin('sdc-list-view');