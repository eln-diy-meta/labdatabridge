import {AbstractSDC, app, trigger} from 'sdc_client';


class CreateSyncInstanceController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/create_sync_instance"; //<create-sync-instance></create-sync-instance>
        this.contentReload = true;

        this.current_eln_types = {eln_a: null, eln_b: null};
        this.current_eln_connections = {eln_a: null, eln_b: null};
        this.eln_connection = null;

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit() {
    }

    onLoad($html) {
        let self = this;
        return new Promise((resolve) => {
            this.serverCall('get_current_create_model').then((eln_user_pk) => {
                self.eln_connection = this.newModel('ElnConnection', {pk: eln_user_pk});
                self.eln_connection.load().then(() => {
                    $html.find('.main-form').append(self.eln_connection.editForm());
                    resolve(super.onLoad($html));
                }).catch(() => {
                    resolve(super.onLoad($html));
                });
            });
        });
    }

    update_connection_related_view() {
        const a_connected = this.current_eln_connections.eln_a?.values.is_connected;
        const b_connected = this.current_eln_connections.eln_b?.values.is_connected;
        if(a_connected) {
            this.find('.form-con-box-a').addClass('connected');
        } else {
            this.find('.form-con-box-a').removeClass('connected');
        }
        if(b_connected) {
            this.find('.form-con-box-b').addClass('connected');
        }else {
            this.find('.form-con-box-b').removeClass('connected');
        }

        this.find('.save-next-btn').prop('disabled', !(a_connected && b_connected));
    }

    update_connection_check_results() {
        const self = this;
        if(this.eln_connection.values.eln_a !== null && this.current_eln_types.eln_a !== this.eln_connection.values.eln_a.valueOf()) {
            this.serverCall('get_connection_model', {eln_type: 'a'}).then((res) => {
                self.current_eln_types.eln_a = self.eln_connection.values.eln_a.valueOf();
                self.current_eln_connections.eln_a = self.newModel(res.model, {pk: res.pk});
                self.find('.form-con-a').safeEmpty().append(self.current_eln_connections.eln_a.editForm(-1, ()=> {
                   self.update_connection_related_view();
                }));

            }).catch(()=>{});
        } else {
            self.update_connection_related_view();
        }
        if(this.eln_connection.values.eln_b !== null && this.current_eln_types.eln_b !== this.eln_connection.values.eln_b.valueOf()) {
            this.serverCall('get_connection_model', {eln_type: 'b'}).then((res) => {
                self.current_eln_types.eln_b = self.eln_connection.values.eln_b.valueOf();
                self.current_eln_connections.eln_b = self.newModel(res.model, {pk: res.pk});
                self.find('.form-con-b').safeEmpty().append(self.current_eln_connections.eln_b.editForm(-1, ()=> {
                    self.update_connection_related_view();
                }));
            }).catch(()=>{});
        } else {
            self.update_connection_related_view();
        }
    }

    save_next_btn() {
        const a_connected = this.current_eln_connections.eln_a.values.is_connected;
        const b_connected = this.current_eln_connections.eln_b.values.is_connected;
        if(a_connected && b_connected) {
            this.eln_connection.values.is_committed = true;
            this.eln_connection.save().then(()=> {
                trigger('onNavigateToController', `/~instance-dashboard~&pk=${this.eln_connection.values.pk}`);
            });
        }

    }

    willShow() {
        this.update_connection_check_results();
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

    onChange($input) {
        $input.closest('.ajax-form').submit();
    }

    submitModelForm($form, e) {
        let self = this;
        return super.defaultSubmitModelForm($form, e).then(function () {
            self.update_connection_check_results();
        }).catch(() => {
            self.update_connection_check_results();
        });
    }

}

app.register(CreateSyncInstanceController).addMixin('sdc-update-on-change');