import {AbstractSDC, app, on} from 'sdc_client';
import {getResizerEvents, onResizeMovable} from '../../utils/libs/moveable.js';

class InstanceDashboardController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/instance_dashboard/%(pk)s"; //<instance-dashboard data-pk=""></instance-dashboard>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
        this.events.unshift(getResizerEvents());
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit() {
    }

    onLoad($html) {
        on('login', this);
        on('logout', this);
        return super.onLoad($html);
    }

    login() {
        this.find('.btn-layer').addClass('logged-in');
    }

    logout() {
        this.find('.btn-layer').removeClass('logged-in');
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        this.onResize();
        return super.onRefresh();
    }

    onResize() {
        onResizeMovable.call(this);
    }

}

app.register(InstanceDashboardController);