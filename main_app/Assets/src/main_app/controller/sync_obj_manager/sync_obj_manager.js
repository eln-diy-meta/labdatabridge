import {AbstractSDC, app, trigger} from 'sdc_client';

class SyncObjManagerController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/sync_obj_manager"; //<sync-obj-manager></sync-obj-manager>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk) {
        this.sync_pk = pk;
        this.model = this.newModel('SyncObjModel', {'connection__id': pk});

    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

    createNewProb() {
        this.model.create({'connection': this.sync_pk}).then((data) => {
            let instance = data.data.instance
            trigger('goTo', `.~sync-obj-editor~&model=SyncObjModel&pk=${instance.pk}`)
        });
    }

    controller_name() {
        return 'Sync Model Manager'
    }

    unlock_form(data) {
        let self = this;
        let timeout_handler = null;

        function lock_update() {
            let model = self.newModel('SyncObjModel', {'pk': data.pk, 'connection__id': self.sync_pk});
            model.load().then(() => {
                model.values.locked = true;
                model.save().then(() => {
                    model.close();
                });
            });
        }

        return <div>
            <button type="button" data-bs-toggle="tooltip" data-bs-placement="left"
                    title="Lock mapping. This cannot be undone!" className="btn btn-success" onClick={function () {
                $(this).hide().next().show();
                timeout_handler = setTimeout(lock_update, 1000);
            }}>
                <i className="bi bi-unlock"></i>
            </button>
            <button onClick={function () {
                if (timeout_handler) clearTimeout(timeout_handler);
                $(this).hide().prev().show();
            }} style="display: NONE;" className="btn btn-info">Undo locking!
            </button>
        </div>
    }

    submitImport($btn, evt) {
        $btn.closest('form').submit();
    }

    uploadImport($form, etv) {
        etv.preventDefault();
        if (this._upload_is_disabled) {
            return
        }

        this._upload_is_disabled = true;
        const $btn = $form.find('label');

        const btn_loading = () => {
            if (this._upload_is_disabled) {
                setTimeout(btn_loading, 100);
                let text = $btn.text();
                $btn.text(text.slice(-1) + text.slice(0, -1));
            }
        }

        this.submitForm($form[0]).then((data) => {
            trigger('goTo', `.~sync-obj-editor~&model=SyncObjModel&pk=${data.pk}`)
        }).catch((a,b)=> {
            console.log(a,b);
        }).finally(() => {
            $form.get(0).reset();
            $btn.text(text);
            this._upload_is_disabled = false;
        });

        const text = $btn.text();
        $btn.text('IMPORTING PLEASE WAIT....');
        btn_loading();
    }

}

app.register(SyncObjManagerController).addMixin('sdc-list-view');