import {AbstractSDC, app} from 'sdc_client';


class SyncObjEditorController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/sync_obj_editor/%(pk)s"; //<sync-obj-editor data-pk=""></sync-obj-editor>
        this.model_name = 'SyncObjModel';
        this.eln_model_name = {'a': null, 'b': null};

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        this.events.unshift();
        this.input_id_json = ['#id_', '_sync_key'];
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    on_error() {
        this.on_update();
    }

    on_update(a, b) {
        if (this.eln_model_name.a !== this.find('#id_a_model_name').val() ||
            this.eln_model_name.b !== this.find('#id_b_model_name').val()) {
            app.getController(this.find('sync-obj-json-div')).reload();
        }
        this.eln_model_name.a = this.find('#id_a_model_name').val();
        this.eln_model_name.b = this.find('#id_b_model_name').val();
        this.update_key();
    }

    onInit() {
    }

    onLoad($html) {
        // <sync-obj-json-div data-pk="{{sync_model.pk}}"><h3 class="text-center">...LOADING...</h3></sync-obj-json-div>
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        this.update_key();
        this.eln_model_name.a = this.model.values?.a_model_name;
        this.eln_model_name.b = this.model.values?.b_model_name;
        return super.onRefresh();
    }

}

app.register(SyncObjEditorController).addMixin('sdc-model-form', 'json-key-selector-mixin');