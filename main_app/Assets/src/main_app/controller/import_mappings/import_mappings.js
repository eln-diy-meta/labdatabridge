import {AbstractSDC, app} from 'sdc_client';


class ImportMappingsController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/import_mappings/%(pk)s"; //<import-mappings data-pk=""></import-mappings>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk) {
        this.pk = pk;
    }

    onLoad($html) {
        $html.find('.mapping-key').each(function() {
           const $this = $(this);
           const text = $this.text().replace(/^"+/, '').replace(/"+$/, '');
           $this.empty();
           for(let text_snippet of text.trim('"').split('"."')) {
               $this.append(`<span>${text_snippet}</span>`)
           }
        });
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

    load_mappings($btn) {
        function btn_loading() {
            if($btn.prop("disabled")) {
                setTimeout(btn_loading, 200);
                let text = $btn.text();
                $btn.text(text.slice(-1) + text.slice(0,-1));
            }
        }
        const text = $btn.prop("disabled",true).text();
        $btn.text('LOADING....');
        btn_loading();
        this.serverCall('loadMappings', {pk: this.pk}).then(()=> {
            $btn.prop("disabled",false).text(text);
        }).catch(()=> {
            $btn.prop("disabled",false).text(text);
        });
    }

}

app.register(ImportMappingsController);