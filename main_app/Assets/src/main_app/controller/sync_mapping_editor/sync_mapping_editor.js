import {AbstractSDC, app} from 'sdc_client';


class SyncMappingEditorController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/sync_mapping_editor/%(pk)s"; //<sync-mapping-editor data-pk=""></sync-mapping-editor>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit() {
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        this.model.load().then(()=> {
            this._parentController.jsonKeySelectorMixin(this.model.values.parent_element);
        });

        return super.willShow();
    }

    onRefresh() {
        this.on_update();
        return super.onRefresh();
    }

    on_error() {
        this.on_update();
    }

    on_update() {
        this._parentController.update_key();
        const data = this.find('#id_data_type').val();
        if(data === 'atomic') {
            this.find('.open-operator-editor').show();
        } else {
            this.find('.open-operator-editor').hide();
        }
    }

    get srcValA() {
        return this._parentController.getAValueForKey();
    }

    get srcValB() {
        return this._parentController.getBValueForKey();
    }

    onSaveA(xml, commit=true) {
        let $elem = this.find('#id_a_to_b_xml').val(xml)
        if(commit) {
            $elem.closest('form').submit();
        }
    }

    onSaveB(xml, commit=true) {
        let $elem = this.find('#id_b_to_a_xml').val(xml);
        if(commit){
            $elem.closest('form').submit();
        }
    }

}

app.register(SyncMappingEditorController).addMixin('sdc-model-form');