import {AbstractSDC, app} from 'sdc_client';
import {uuidv4} from "sdc_client/src/simpleDomControl/sdc_utils.js";


class SyncObjJsonDivController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/sync_obj_json_div/%(pk)s"; //<sync-obj-json-div data-pk=""></sync-obj-json-div>
        this.load_async = true;
        this.aPrefixKey = this.bPrefixKey = ['root', 'root'];

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk) {
        this.sync_pk = pk;
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        this.load_a_sample();
        this.load_b_sample();
        return super.onRefresh();
    }


    load_a_sample() {
        const value_a = this.find('#a_id_key').val();
        if (!value_a) return;
        const $text_container = this.find('.json-a-container').safeEmpty();
        $text_container.text('...Loading...');
        this.serverCall('load_a_sample', {
            sync_pk: this.sync_pk,
            id_key: value_a
        }).then((x) => {
            this.json_a = x;
            $text_container.empty().append(this._prepare_sample_json(x)).find('.json_elem').attr('sdc_click', 'select_a_elem');
            this.setRoot(this.aPrefixKey[1], this.bPrefixKey[1]);
        });
    }

    load_b_sample() {
        const value_b = this.find('#b_id_key').val();
        if (!value_b) return;
        const $text_container = this.find('.json-b-container').safeEmpty();
        $text_container.text('...Loading...');
        this.serverCall('load_b_sample', {
            sync_pk: this.sync_pk,
            id_key: value_b
        }).then((x) => {
            this.json_b = x;
            $text_container.empty().append(this._prepare_sample_json(x)).find('.json_elem').attr('sdc_click', 'select_b_elem');
            this.setRoot(this.aPrefixKey[1], this.bPrefixKey[1]);
        });
    }

    select_b_elem(elm) {
        if (!this._parentController.json_select) {
            console.error(`${this._parentController.controller_name()} needs to implement json_select`);
            return;
        }

        if (!elm.hasClass('disabled')) {
            let newKey = elm.data('key');
            if (newKey.startsWith(this.bPrefixKey[0])) {
                const prefix = this.bPrefixKey[1].replace(/"$/, "").split('"."');
                const keyArray = newKey.split('"."');
                keyArray[prefix.length] = "%d";
                keyArray.splice(0, prefix.length, ...prefix);
                newKey = keyArray.join('"."')
            }
            this._parentController.json_select('b', newKey);
        }
    }

    getAValueFromKey(key) {
        return this._getValueFromKey(key, this.json_a);
    }

    getBValueFromKey(key) {
        return this._getValueFromKey(key, this.json_b);
    }

    _getValueFromKey(key, jsonObj) {
        const keyList = key.replace(/^"|"$/gm, '').split('"."');
        for (let keyIdx = 0; keyIdx < keyList.length; ++keyIdx) {
            if (jsonObj instanceof Array) {
                if (keyList[keyIdx] === '%d') {
                    keyList[keyIdx] = 0;
                } else {
                    keyList[keyIdx] = parseInt(keyList[keyIdx]);
                }
            }
            if (!jsonObj.hasOwnProperty(keyList[keyIdx])) {
                return [null, false]
            }
            jsonObj = jsonObj[keyList[keyIdx]];
        }

        return [jsonObj, true];
    }

    select_a_elem(elm) {
        if (!this._parentController.json_select) {
            console.error(`${this._parentController.controller_name()} needs to implement json_select`);
            return;
        }

        if (!elm.hasClass('disabled')) {
            let newKey = elm.data('key');

            if (newKey.startsWith(this.aPrefixKey[0])) {
                const prefix = this.aPrefixKey[1].replace(/"$/, "").split('"."');
                const keyArray = newKey.split('"."');
                keyArray[prefix.length] = "%d";
                keyArray.splice(0, prefix.length, ...prefix);
                newKey = keyArray.join('"."')
            }
            this._parentController.json_select('a', newKey);
        }
    }

    _prepareAPrefixHash(key) {
        this.aPrefixKey = [key.split('%d').join('0'), key];
        return this._hashCode(this.aPrefixKey[0]);
    }

    _prepareBPrefixHash(key) {
        this.bPrefixKey = [key.split('%d').join('0'), key];
        return this._hashCode(this.bPrefixKey[0]);
    }

    _hashCode(str) {
        return str.split('').reduce((prevHash, currVal) =>
            (((prevHash << 5) - prevHash) + currVal.charCodeAt(0)) | 0, 0);
    }

    setRoot(aKey, bKey) {

        aKey = this._prepareAPrefixHash(aKey);
        bKey = this._prepareBPrefixHash(bKey);

        this.find(`.json-a-container .json_elem`).addClass('disabled');
        this.find(`.json-a-container .for-${aKey} .disabled`).removeClass('disabled');
        this.find(`.json-a-container .${aKey}`).removeClass('disabled');

        this.find(`.json-b-container .json_elem`).addClass('disabled');
        this.find(`.json-b-container .for-${bKey} .disabled`).removeClass('disabled');
        this.find(`.json-b-container .${bKey}`).removeClass('disabled');
    }


    _prepare_sample_json(x, json_path = [], parent_class_key = this._hashCode('root')) {
        let text = `<span class="for-${parent_class_key}">`;
        const space = "  ";
        if (x instanceof Array) {
            const uuid_key = uuidv4();
            text += `[${this._collapse_btn(uuid_key)}<span class="json-container ${uuid_key}">`
            for (let i = 0; i < x.length; ++i) {
                let new_json_path = [...json_path];
                new_json_path.push(i);
                const json_key = `"${new_json_path.join('"."')}"`;
                const class_key = this._hashCode(json_key);
                text += `<br>${space.repeat(new_json_path.length)}<span data-dom-id='${class_key}' class='${class_key} json_elem' data-key='${json_key}'>${i}</span>: `
                text += `${this._prepare_sample_json(x[i], new_json_path, class_key)}`;
            }
            text += `<br>${space.repeat(json_path.length)}</span>]`
        } else if (x instanceof Object) {
            const uuid_key = uuidv4();
            text += `{${this._collapse_btn(uuid_key)}<span class="json-container ${uuid_key}">`
            for (const key in x) {
                let new_json_path = [...json_path];
                new_json_path.push(key)
                let val = x[key];
                const json_key = `"${new_json_path.join('"."')}"`;
                const class_key = this._hashCode(json_key);
                text += `<br>${space.repeat(new_json_path.length)}<span data-dom-id='${class_key}' class='${class_key} json_elem' data-key='${json_key}'>${key}</span>: `;
                text += `${this._prepare_sample_json(val, new_json_path, class_key)}`;
            }
            text += `<br>${space.repeat(Math.max(json_path.length, 0))}</span>}`
        } else {
            x = `${x}`.split('\n')[0].substring(0, 100);
            const json_key = `"${json_path.join('"."')}"`;
            const class_key = this._hashCode(json_key) + '_value';
            return `<span data-dom-id='${class_key}' class='${class_key} json_elem' data-key='${json_key}'>${x}</span>`;
        }

        return `${text}</span>`;

    }

    _collapse_btn(id) {
        return `<button sdc_click="collapse_json_section" class="collapse-json-btn open" data-section="${id}"><i class="bi bi-arrows-collapse"></i><i class="bi bi-arrows-expand"></i></button>`
    }

    collapse_json_section(btn) {
        btn.toggleClass('collapsed')
        this.find(`.${btn.data('section')}`).toggle();
    }

    testSyncAToB() {
        const value_a = this.find('#a_id_key').val();
        const value_b = this.find('#b_id_key').val();
        const self = this;
        const $modal = self.find('#diff-modal');
        new Modal($modal).show();
        $modal.find('.json-sync-results').text('...Loading...');
        this.serverCall('test_sync_a_to_b', {
            sync_pk: this.sync_pk,
            id_key_a: value_a,
            id_key_b: value_b
        }).then((res) => {
            this._show_diff_modal(res, this.json_b, $modal);
        });
    }

    testSyncBToA() {
        const value_a = this.find('#a_id_key').val();
        const value_b = this.find('#b_id_key').val();
        const self = this;
        const $modal = self.find('#diff-modal');
        new Modal($modal).show();
        $modal.find('.json-sync-results').text('...Loading...');
        this.serverCall('test_sync_b_to_a', {
            sync_pk: this.sync_pk,
            id_key_a: value_a,
            id_key_b: value_b
        }).then((res) => {
            this._show_diff_modal(res, this.json_a, $modal);
        });
    }

    _show_diff_modal(res, old_json, $modal) {

        let $new_text = $('<div></div>').html(this._prepare_sample_json(res));
        let $old = $('<div></div>').html(this._prepare_sample_json(old_json));

        $($new_text).find('.json_elem').each(function () {
            const $this = $(this);
            const $old_this = $old.find(`.${$this.data('dom-id')}`);
            if ($old_this.length < 1) {
                $this.addClass('new_json_element');
            } else if ($old_this.html() !== $this.html()) {
                $this.addClass('changed_json_element');
            }
        });

        $modal.find('.json-sync-results').html($($new_text).html());
        this._prepareWarnings($modal, res, old_json);
    }

    _prepareWarnings($modal, new_json, old_josn, total_key='#') {
        const newType = this._instanceType(new_json);
        const oldType = this._instanceType(old_josn);
        if (newType !== oldType) {
            $modal.find('.json-sync-results').prepend(`<div class="alert alert-warning">${total_key}<br>was tpye ${newType} now is ${oldType}</div>`);
            return;
        }
        if (newType === 'array' || newType === 'object')
            for (const [key, value] of Object.entries(new_json)) {
                try {
                    this._prepareWarnings($modal, value, old_josn[key], total_key + `."${key}"`);
                } catch {}
            }
    }

    _instanceType(n) {
        if (n instanceof Array) {
            return 'array';
        }
        if (Number(n) === n && n % 1 === 0) {
            return 'int';
        }
        if (Number(n) === n && n % 1 !== 0) {
            return 'float';
        }
        return typeof n;
    }

}

app.register(SyncObjJsonDivController);