import {AbstractSDC, app, trigger} from 'sdc_client';
import {uuidv4} from "../../utils.js";


class HomeMenuController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/home_menu/%(pk)s"; //<home-menu data-pk=""></home-menu>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk) {
        this.model = this.newModel('ElnConnection', {pk: pk})
        this.processModels = this.newModel('SyncProcess', {connection__pk: pk});
        this.processModels.isConnected();
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        this.update_details();
        this.processModels.on_create = this.model.on_update = () => {
            this.update_details();
        };
        return super.willShow();
    }

    update_details() {
        const $details = this.model.detailView(-1, () => {
            this.find('.eln-sync-details').empty().append($details);
            const $tooltip = this.find('[data-bs-toggle="tooltip"]');
            console.log($tooltip);
            $tooltip.tooltip();
        });
    }

    onRefresh() {
        return super.onRefresh();
    }

    removeInstance($btn, e) {
        this.model.delete().then(() => {
            app.safeEmpty(this.$container).append('<div><h2>Deleted!</h2><button href="/" class="navigation-links btn btn-dark">Back</button></div>');
            this.refresh();
        });
    }

    run_sync_now($btn) {
        const text = $btn.text();
        $btn.prop('disabled', true).text('..syncing..');
        let uuid = uuidv4();

        this.serverCall('run_sync_now', {'pk': this.model.values.pk, uuid}).then((res) => {
            $btn.prop('disabled', false).text(text);
            trigger('goTo', ['.', 'sync-process-watcher'], {uuid: uuid, runner: 'runProcess'});
        }).catch((res) => {
            $btn.prop('disabled', false).text(text);
        });
    }

    deactivate_sync() {
        this.model.values.is_active = false;
        this.model.save();
    }

    activate_sync() {
        this.model.values.is_active = true;
        this.model.save();
    }

    controller_name() {
        return 'Menu'
    }

    reset_pairing(btn) {
        let instancePk = btn.data('instancePk');
        this.serverCall('reset_pairing', {pk: instancePk})
    }

}

app.register(HomeMenuController);