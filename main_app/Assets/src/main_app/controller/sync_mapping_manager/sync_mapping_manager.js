import {AbstractSDC, app, trigger} from 'sdc_client';


class SyncMappingManagerController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/sync_mapping_manager/%(pk)s"; //<sync-mapping-manager data-pk=""></sync-mapping-manager>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
        this.input_id_json = ['.sdc_sub_detail_container.active #id_', '_key'];
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk) {
        this.sync_pk = pk;
        this.model = this.newModel('MappingProperty', {'sync_model__id': pk});
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

    createNewProb() {
        this.model.create({'sync_model': this.sync_pk}).then((res)=> {
            trigger('goTo', ['','*','*','*','*', 'sync-mapping-editor'], {model: 'MappingProperty', pk: res.data.instance.pk});
        });
    }

    jsonKeySelectorMixin(pk) {
        this.model.load().then(()=> {
            let parent_element = this.model.byPk(pk) || {};
            for(let c of this._childController.syncObjJsonDiv) {
                c.setRoot(parent_element.a_key || 'root', parent_element.b_key || 'root');
            }
        });
    }

    recompileAll($btn) {
        function btn_loading() {
            if($btn.prop("disabled")) {
                setTimeout(btn_loading, 200);
                let text = $btn.text();
                $btn.text(text.slice(-1) + text.slice(0,-1));
            }
        }
        const text = $btn.prop("disabled",true).text();
        $btn.text('COMPILING....');
        btn_loading();

        this.serverCall('recompile_all', {pk: this.sync_pk}).finally(()=> {
            $btn.prop("disabled",false).text(text);
        });
    }

    addSubInstance($btn) {
        this.model.create({'sync_model': this.sync_pk, 'parent_element': $btn.data('instance-pk')}).then((res)=> {
            let newElement = res.data.instance;
            trigger('goTo', ['','*','*','*','*', 'sync-mapping-editor'], {model: 'MappingProperty', pk: newElement.pk});
        });
    }

    controller_name() {
        return 'Sync. Mapping'
    }

}

app.register(SyncMappingManagerController).addMixin('sdc-list-view', 'json-key-selector-mixin');