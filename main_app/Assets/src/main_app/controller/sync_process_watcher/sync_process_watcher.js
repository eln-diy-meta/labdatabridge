import {AbstractSDC, app} from 'sdc_client';


class SyncProcessWatcherController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/sync_process_watcher"; //<sync-process-watcher></sync-process-watcher>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk, uuid) {
        if (!pk) {
            this.model = this.newModel('SyncLog', {process__uuid: uuid});
        } else {
            this.model = this.newModel('SyncLog', {process: pk});
        }
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        this._runRefresh();
        return super.willShow();
    }

    _runRefresh() {
        if (this.model.socket.readyState === WebSocket.OPEN || this.model.socket.readyState === WebSocket.CONNECTING) {
            if (this.model.socket.readyState === WebSocket.OPEN) {
                this.model.on_update();
            }
            setTimeout(() => {
                this._runRefresh();
            }, 500);
        }
    }

    onRefresh() {
        return super.onRefresh();
    }

}

app.register(SyncProcessWatcherController).addMixin('sdc-list-view');