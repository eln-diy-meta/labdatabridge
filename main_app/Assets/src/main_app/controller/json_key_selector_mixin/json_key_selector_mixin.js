import {AbstractSDC, app} from 'sdc_client';


class JsonKeySelectorMixinController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/json_key_selector_mixin"; //<json-key-selector-mixin></json-key-selector-mixin>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
        this.input_id_json = [];
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit() {
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }


    update_key() {
        for(let a_or_b of ['a', 'b']) {
            let $elem = this.find(this.input_id_json.join(a_or_b));
            if($elem.length === 0) {
                return;
            }

            let value = $elem.val();
            let val_list = [];
            if(value) {
                val_list = value.replace(/^[\s"]+|[\s"]+$/gm, '').split('"."');
            }
            $elem.hide();
            let $div_container = $elem.next();
            if (!$div_container.hasClass(`container_${a_or_b}`)) {
                $elem.hide();
                $div_container = $('<div></div>').addClass(`key_container container_${a_or_b}`);
                $elem.after($div_container);
            }
            $div_container.empty();
            for(let val of val_list) {
                $div_container.append(`<span class="key_element">${val}</span>`);
            }
            if(val_list.length > 0) {
                $div_container.append(`<button type="button" sdc_click="empty_key" class="empty_btn btn btn-outline-danger btn-sm">X</button>`);
            }
        }

    }

    empty_key($btn) {
        $btn.parent().prev().val(null).trigger('change');
    }

    getAValueForKey() {
        return this._getValueForKey('a');
    }

    getBValueForKey() {
        return this._getValueForKey('b');
    }

    _getValueForKey(a_or_b) {
        let $elem = this.find(this.input_id_json.join(a_or_b));
        if($elem.length === 0) {
            return;
        }

        let key_value = $elem.val();
        for(let cc of this._childController.syncObjJsonDiv) {
            try {
                if (a_or_b === 'a') {
                    return cc.getAValueFromKey(key_value);
                } else if (a_or_b === 'b') {
                    return cc.getBValueFromKey(key_value);
                }
            } catch {}
        }

        return [null, false];

    }

    json_select(a_or_b, key) {
        let $elem = this.find(this.input_id_json.join(a_or_b));
        this._set_key($elem, key);
    }

    _set_key($elem, key) {
        if ($elem.length === 0) return;
        let val = $elem.val();
        if(!val.match(/[+-/-]$/g)) {
            val = '';
        }

        val = `${val}${key}`;
        $elem.val(val).trigger('change');
    }

}

app.register(JsonKeySelectorMixinController);