import {AbstractSDC, app} from 'sdc_client';


class PropertyChangeListController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/property_change_list"; //<property-change-list></property-change-list>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(element_pk, process_pk) {
        console.log(element_pk, process_pk);
        this.model = this.newModel('SyncedProperty', {'creating_process_id': process_pk, 'element_id': element_pk});
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

}

app.register(PropertyChangeListController).addMixin('sdc-list-view');