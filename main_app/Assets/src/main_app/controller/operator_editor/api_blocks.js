import {common, Variables} from 'blockly';
import {javascriptGenerator, Order} from 'blockly/javascript.js';

import { new_blocks } from '../../utils/bockly_def.js'

export const blocks = common.createBlockDefinitionsFromJsonArray(new_blocks);

javascriptGenerator.forBlock['get_from_json'] = function (block, generator) {
    var value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    var value_idx = generator.valueToCode(block, 'idx', Order.ATOMIC);

    return [`${value_json_obj}[${value_idx}]`, Order.NONE];
};

javascriptGenerator.forBlock['set_in_json'] = function (block, generator) {
    var value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    var value_idx = generator.valueToCode(block, 'idx', Order.ATOMIC);
    var value_value = generator.valueToCode(block, 'value', Order.ATOMIC);

    return `${value_json_obj}[${value_idx}] = ${value_value};\n`;
};

javascriptGenerator.forBlock['for_each_json'] = function (block, generator) {
    let variable_key = generator.nameDB_.getName(block.getFieldValue('Key'), Variables.CATEGORY_NAME);
    let variable_val = generator.nameDB_.getName(block.getFieldValue('val'), Variables.CATEGORY_NAME);
    let value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    let statements_code = generator.statementToCode(block, 'code');
    // TODO: Assemble javascript into code variable.
    return `for (const [${variable_key}, ${variable_val}] of Object.entries(${value_json_obj})) {\n${statements_code}}\n`;
};

javascriptGenerator.forBlock['split_str'] = function (block, generator) {
    var value_in_str = generator.valueToCode(block, 'in_str', Order.ATOMIC);
    var value_at = generator.valueToCode(block, 'at', Order.ATOMIC);

    return [`String(${value_in_str}).split(String(${value_at}))`, Order.NONE];
};

javascriptGenerator.forBlock['json_contains'] = function (block, generator) {
    const value_json_obj = generator.valueToCode(block, 'json_obj', Order.ATOMIC);
    const dropdown_key_or_val = block.getFieldValue('key_or_val');
    const value_target = generator.valueToCode(block, 'target', Order.ATOMIC);
    let code = '';
    if (dropdown_key_or_val === 'key') {
        code = `${value_json_obj}.hasOwnProperty(${value_target})`;
    } else if (dropdown_key_or_val === 'value') {
        code = `Object.values(${value_json_obj}).includes(${value_target})`;
    }

    // TODO: Change Order.NONE to the correct strength.
    return [code, Order.NONE];
};

javascriptGenerator.forBlock['output'] = function (block, generator) {
    const value_name = generator.valueToCode(block, 'NAME', Order.ATOMIC);
    return `return ${value_name}\n`;
};

javascriptGenerator.forBlock['input'] = function (block, generator) {
    return ['input_val', Order.NONE];
};

javascriptGenerator.forBlock['convert_to'] = function (block, generator) {
    const value_input_variable = generator.valueToCode(block, 'input_variable', Order.ATOMIC);
    const dropdown_to_type = block.getFieldValue('to_type');
    let code;
    if (dropdown_to_type === 'int') {
        code = `parseInt(${value_input_variable})`;
    } else if (dropdown_to_type === "str") {
        code = `String(${value_input_variable})`;
    } else if (dropdown_to_type === 'float') {
        code = `parseFloat(${value_input_variable})`;
    } else if (dropdown_to_type === 'bool') {
        code = `typeof ${value_input_variable} === 'string' ? `
        code += `['yes', 'true', 'right'].includes(String(${value_input_variable}).toLowerCase())`;
        code += ` : Boolean(${value_input_variable})`;
    }

    return [code, Order.NONE];
};

javascriptGenerator.forBlock['empty_json'] = function (block, generator) {
    return ['{}', Order.NONE];
};

javascriptGenerator.forBlock['current_value'] = function(block, generator) {
    return ['current_val', Order.NONE];
}

javascriptGenerator.forBlock['iteration_predecessor'] = function(block, generator) {
    return ['iteration_predecessor', Order.NONE];
}

javascriptGenerator.forBlock['str_to_date'] = function(block, generator) {
    const value_date = generator.valueToCode(block, 'date', Order.ATOMIC);
    const text_format = block.getFieldValue('format');
    const code = `new Date(${value_date}) /* The format is ignored for code testing! */`
    return [code, Order.NONE];
};

javascriptGenerator.forBlock['datetime_to_str'] = function(block, generator) {
    const value_date = generator.valueToCode(block, 'date', Order.ATOMIC);
    const text_format = block.getFieldValue('format');
    const code = `new Date(${value_date}).toString() /* The format is ignored for code testing! */`
    return [code, Order.NONE];
};

javascriptGenerator.forBlock['function_wrapper'] = function(block, generator) {
    const text_func_name = block.getFieldValue('func_name');
    const statements_body = generator.statementToCode(block, 'body');
    return `async function ${text_func_name}(input_val, current_val){\n${statements_body}}\n\n`;
};

javascriptGenerator.forBlock['function_call'] = function(block, generator) {
    const text_func_name = block.getFieldValue('func_name');
    const value_current_val = generator.valueToCode(block, 'current_val', Order.ATOMIC);
    const value_input_val = generator.valueToCode(block, 'input_val', Order.ATOMIC);
    const code = `await ${text_func_name}(${value_input_val}, ${value_current_val})`;
    return [code, Order.NONE];
};


javascriptGenerator.addReservedWords('highlightBlock');