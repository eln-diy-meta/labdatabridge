import {AbstractSDC, app, trigger} from 'sdc_client';
import * as Blockly from 'blockly/core.js';
import {javascriptGenerator} from 'blockly/javascript.js';

import {blocks} from './api_blocks.js';

class OperatorEditorController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/main_app/operator_editor/%(pk)s"; //<operator-editor data-pk=""></operator-editor>
        this.srcVal = {};
        this._currentlyActive = null;
        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'mousdown': {'#blocklyDiv': (ev, $elem) => alert(0)}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(pk, srcValA, srcValB, onSaveA, onSaveB) {
        if (typeof onSaveA !== 'function' || typeof onSaveB !== 'function') {
            trigger('goTo', '..');
        }
        this.srcVal = {A: undefined, B: undefined};
        if (srcValA[1] === true) {
            this.srcVal['A'] = srcValA[0];
        }

        if (srcValB[1] === true) {
            this.srcVal['B'] = srcValB[0];
        }

        this.onSave = {
            A: onSaveA,
            B: onSaveB
        }
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        Blockly.common.defineBlocks(blocks);
        /* TODO: Change toolbox XML ID if necessary. Can export toolbox XML from Workspace Factory. */
        var toolbox = document.getElementById("toolbox");


        var options = {
            toolbox: toolbox,
            collapse: true,
            comments: true,
            disable: false,
            maxBlocks: Infinity,
            trashcan: true,
            horizontalLayout: false,
            toolboxPosition: 'start',
            css: true,
            media: 'https://blockly-demo.appspot.com/static/media/',
            rtl: false,
            scrollbars: true,
            sounds: true,
            oneBasedIndex: true
        };


        for (let src of ['A', 'B']) {
            if(!this.srcVal[src]) {
                this.find(`.editor-body-${src} .value-src`).text(String(this.srcVal[src]));
            } else {
                this.find(`.editor-body-${src} .value-src`).text(JSON.stringify(this.srcVal[src]));
            }
        }
        this.workspace = Blockly.inject('blocklyDiv', options);

        /* Load Workspace Blocks from XML to workspace. Remove all code below if no blocks to load */

        /* TODO: Change workspace blocks XML ID if necessary. Can export workspace blocks XML from Workspace Factory. */
        this.workspaceBlocks = {
            A: this.find('.workspace-container-A xml')[0],
            B: this.find('.workspace-container-B xml')[0]
        };

        /* Load blocks to workspace. */
        Blockly.Xml.domToWorkspace(this.workspaceBlocks.A, this.workspace);
        this.open_page(this.find('.nav-link.active'));

        return super.willShow();
    }

    generateCode() {
        window.LoopTrap = 500;
        const workspace = this.workspace;
        javascriptGenerator.STATEMENT_PREFIX = 'try { await highlightBlock(%1); } catch (e) {alert(e); return e; }\n';
        let codeExecute = javascriptGenerator.workspaceToCode(workspace);
        javascriptGenerator.STATEMENT_PREFIX = '';
        const codePrint = javascriptGenerator.workspaceToCode(workspace);
        this.find(`.editor-body-${this._currentlyActive} .pytext`).text(codePrint);
        if(!codeExecute) {
            codeExecute = 'return CURRENT_VALUE;'
        }
        const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));
        const $targetField = this.find(`.editor-body-${this._currentlyActive} .value-target`);

        try {
            (async function (INPUT, CURRENT_VALUE, ITERATION_PREDECESSOR) {
                try {
                    return eval(`async (input_val, current_val, iteration_predecessor) => {
                        async function highlightBlock(id) {
                            if (--window.LoopTrap === 0) throw "To meny loops.";
                            workspace.highlightBlock(id);
                            let text = ['Values of all variables at current state:'];
                            for (const v of workspace.getAllVariables()) {
                                try {
                                    text.push(v.name + ' = ' + JSON.stringify(eval(v.name)));
                                } catch {}
                            }
                            $targetField.text(text.join('\\n'));
                            await sleep(300);
                        }
                        
                        ${codeExecute}
                    }`)(INPUT, CURRENT_VALUE, ITERATION_PREDECESSOR);
                } catch (e) {
                    throw e;
                }

            })(this.srcVal[this._currentlyActive], this.srcVal[this._currentlyNotActive], []).then((output) => {
                if(!output) {
                    $targetField.text(String(output));
                } else {
                    $targetField.text(JSON.stringify(output));
                }
            }).catch((e)=> {
                throw e;
            });
        } catch (e) {
            alert(e);
        }

    }

    open_page($btn) {
        this.workspaceBlocks[this._currentlyActive] = Blockly.Xml.workspaceToDom(this.workspace);
        this._currentlyActive = $btn.data('src');
        this.workspace.clear();
        Blockly.Xml.domToWorkspace(this.workspaceBlocks[this._currentlyActive], this.workspace);
        this.find('.nav-link.active').removeClass('active');
        $btn.addClass('active');
        this.find('.editor-body').hide();
        this.find(`.editor-body-${this._currentlyActive}`).show();
        this.find(`.download_python`).hide();
        this.find(`.download_python-${this._currentlyActive}`).show();
    }

    save() {
        this.save_all();
    }

    save_all() {
        this.workspaceBlocks[this._currentlyActive] = Blockly.Xml.workspaceToDom(this.workspace);
        this._save(this.workspaceBlocks.A, 'A', false);
        this._save(this.workspaceBlocks.B, 'B');
    }

    _save(domXml, target, commit = true) {
        this.onSave[target](domXml.outerHTML, commit);
    }

    onRefresh() {
        return super.onRefresh();
    }

    get _currentlyNotActive() {
        if (this._currentlyActive === 'A') {
            return 'B';
        }
        return 'A';
    }

    importWorkspace($input) {
        const file = $input[0].files[0];
        if (file) {
            var reader = new FileReader();
            reader.readAsText(file, "UTF-8");
            reader.onload =  (evt) => {
                this.workspace.clear();
                try {
                    const new_xml = $(evt.target.result)[0];
                    Blockly.Xml.domToWorkspace(new_xml, this.workspace);
                    this.workspaceBlocks[this._currentlyActive] = new_xml;
                } catch {
                    trigger("pushErrorMsg", 'Upps!!', 'File content not correct!');
                    Blockly.Xml.domToWorkspace(this.workspaceBlocks[this._currentlyActive], this.workspace);
                }
            }
            reader.onerror = function (evt) {
                trigger("pushErrorMsg", 'Upps!!', 'error reading file');
            }
        }

    }

    exportWorkspace() {
        this.workspaceBlocks[this._currentlyActive] = Blockly.Xml.workspaceToDom(this.workspace);

        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.workspaceBlocks[this._currentlyActive].outerHTML))  ;
        element.setAttribute('download', 'blockly_export.xml');

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);

    }

}

app.register(OperatorEditorController);