/**
 * @jest-environment jsdom
 */

import {test_utils} from "sdc_client";
import '../src/main_app/main_app.organizer.js'
import '../../../Assets/__tests__/src/sdc_tools/sdc_tools.organizer.js'

describe('BasicInfo', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('basic-info', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
    });

});

describe('CreateSyncInstance', () => {
    let controller, Controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('create-sync-instance', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
    });

});

describe('InstanceDashboard', () => {
    let controller, Controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('instance-dashboard', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
    });

});

describe('HomeMenu', () => {
    let controller, Controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('home-menu', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
    });

});

describe('ListSyncInstance', () => {
    let controller, Controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('list-sync-instance', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
        const $div = controller.$container.find('div');
        expect($div.length).toBe(1);
    });

});

describe('SyncObjManager', () => {
    let controller, Controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('sync-obj-manager', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
    });

});

describe('SyncObjEditor', () => {
    let controller, Controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('sync-obj-editor', {}, '<div><h1>Controller loading...</h1></div>');
    });

    test('Load Content', async () => {
    });

});

describe('SyncMappingManager', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('sync-mapping-manager',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('sync-mapping-manager');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('SyncObjJsonDiv', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('sync-obj-json-div',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('sync-obj-json-div');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('SyncMappingEditor', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('sync-mapping-editor',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('sync-mapping-editor');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('SyncProcessWatcher', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('sync-process-watcher',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('sync-process-watcher');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('SyncedProcessList', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('synced-process-list',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('synced-process-list');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('PropertyChangeList', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('property-change-list',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('property-change-list');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('JsonKeySelectorMixin', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('json-key-selector-mixin',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('json-key-selector-mixin');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('OperatorEditor', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('operator-editor',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('operator-editor');
        expect($div.length).toBeGreaterThan(0);
    });

});

describe('ImportMappings', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('import-mappings',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('import-mappings');
        expect($div.length).toBeGreaterThan(0);
    });

});