from subprocess import DEVNULL

from django.apps import AppConfig
from django.conf import settings
from nodejs import npm


class MainAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main_app'

    def ready(self):
        script_root = settings.BLOCKLY_SCRIPT_ROOT
        npm.call(['install'], cwd=script_root, stdout=DEVNULL)

