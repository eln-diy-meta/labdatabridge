 #!/bin/bash
cd /srv/app

printenv > .env

redis-server --daemonize yes

kill -9 $(ps aux | grep celery | grep -v grep | awk '{print $2}' | tr '\n' ' ') > /dev/null 2>&1
python -m celery -A ElnAdapter worker -D

service nginx start
./manage.py migrate
cron
./manage.py loaddata chemotion_herbie_eln_type.json
./manage.py initsuperuser
./manage.py build_all_oprator
daphne -b 0.0.0.0 -p 8000 ElnAdapter.asgi:application
