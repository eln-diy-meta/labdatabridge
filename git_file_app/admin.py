from django.contrib import admin
from git_file_app.models import GitFileConnection

# Register your models here.
@admin.register(GitFileConnection)
class GitFileConnectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'url' , 'is_connected']