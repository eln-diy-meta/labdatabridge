import json
import os

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from rocrate.rocrate import ROCrate

from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models
from git import Repo

from file_app_utils.models import AbstractFileConnection
from sync_tools.models import AbstractConnection


# Create your models here.

class GitFileSettingsSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = ""
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class GitFileSettings(models.Model, SdcModel):
    edit_form = "git_file_app.forms.GitFileSettingsForm"
    create_form = "git_file_app.forms.GitFileSettingsForm"
    html_list_template = "git_file_app/models/GitFileSettings/GitFileSettings_list.html"
    html_detail_template = "git_file_app/models/GitFileSettings/GitFileSettings_details.html"

    uuid = models.CharField(max_length=255, default='0', editable=False)
    branch = models.CharField(max_length=255)

    def save(
            self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        if self.uuid == '0':
            self.uuid = self.connection.uuid
        super().save(force_insert, force_update, using, update_fields)

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = GitFileSettingsSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


class GitFileConnectionSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = ""
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class GitFileConnection(AbstractConnection, AbstractFileConnection, SdcModel):
    edit_form = "git_file_app.forms.GitFileConnectionForm"
    create_form = "git_file_app.forms.GitFileConnectionForm"
    html_list_template = "git_file_app/models/GitFileConnection/GitFileConnection_list.html"
    html_detail_template = "git_file_app/models/GitFileConnection/GitFileConnection_details.html"

    token = models.CharField(max_length=255, null=True)
    url = models.URLField(verbose_name="URL", null=True)

    git_settings = models.OneToOneField(GitFileSettings, null=True, on_delete=models.CASCADE,
                                        related_name='connection')

    def __str__(self):
        return self.url

    def open_repo(self):
        return Repo(self.file_path)

    def load_repo(self):
        repo = Repo(self.file_path)
        repo.git.checkout(self.git_settings.branch[len('origin/'):])
        o = repo.remotes.origin
        o.fetch()
        o.pull()
        self.load_files()

    @property
    def fetch_url(self):
        return self.build_fetch_url(self.url, self.token)

    @classmethod
    def build_fetch_url(cls, url, token):
        url = url.split("://", 1)
        return f'{url[0]}://{token}@{url[1]}'

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = GitFileConnectionSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()

