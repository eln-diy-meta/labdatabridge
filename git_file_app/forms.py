import os.path
import shutil
import tempfile

from django import forms
from django.core.exceptions import ValidationError

from git_file_app.models import GitFileSettings
from git_file_app.models import GitFileConnection
from django.forms.models import ModelForm
from git import Repo


# Form Model GitFileConnection

class GitFileConnectionForm(ModelForm):
    is_connected = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = GitFileConnection
        fields = ['name', 'url', 'token', 'is_connected']

    def clean(self):
        super().clean()
        token = self.cleaned_data.get('token', '')
        url = GitFileConnection.build_fetch_url(self.cleaned_data.get('url'), token)
        if len(self.errors) == 0:
            temp_dir = tempfile.mkdtemp()
            try:
                Repo.clone_from(url, temp_dir)
                shutil.move(str(temp_dir), str(self.instance.file_path))
            except Exception as e:
                if self.instance is not None:
                    self.instance.is_connected = False
                    self.instance.token = ''
                    self.instance.save()
                raise ValidationError("Connection to the git failed")
            finally:
                if os.path.exists(temp_dir):
                    shutil.rmtree(temp_dir)
            self.cleaned_data['is_connected'] = True


# Form Model GitFileSettings

class GitFileSettingsForm(ModelForm):
    class Meta:
        model = GitFileSettings
        fields = "__all__"
