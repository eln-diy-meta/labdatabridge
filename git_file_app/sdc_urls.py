from django.urls import path, re_path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('git_settings/<str:uuid>', sdc_views.GitSettings.as_view(), name='scd_view_git_settings'),
]
