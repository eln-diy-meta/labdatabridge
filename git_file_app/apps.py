from django.apps import AppConfig


class GitFileAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'git_file_app'

    def ready(self):
        from git_file_app.sync_manager.git_Interface import GitSyncer
        from sync_tools.manager import register_manager
        register_manager('GitFile', GitSyncer)
