from django.http import Http404
from sdc_core.sdc_extentions.views import SDCView
from django.shortcuts import render

from git_file_app.models import GitFileConnection, GitFileSettings


class GitSettings(SDCView):
    template_name = 'git_file_app/sdc/git_settings.html'

    def get_content(self, request, uuid, *args, **kwargs):
        try:
            git_obj = GitFileConnection.objects.get(uuid=uuid)
            git_settings, c = GitFileSettings.objects.get_or_create(uuid=uuid)
            if c:
                git_obj.git_settings = git_settings
                git_obj.save()

            repo = git_obj.open_repo()
            remote_refs = repo.remote().refs
            branches = []
            for refs in remote_refs:
                branches.append(refs.name)
            return render(request, self.template_name, {
                'settings': git_settings,
                'branches': branches
            })
        except:
            raise Http404('ELN not available')
