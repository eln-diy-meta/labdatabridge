from django.http import Http404
from sdc_core.sdc_extentions.views import SDCView
from django.shortcuts import render

from xxx_app.models import XxxConnection


class SettingsXxx(SDCView):
    template_name='xxx_app/sdc/settings_xxx.html'

    def get_content(self, request, uuid, *args, **kwargs):
        try:
            XxxConnection.objects.get(uuid=uuid, is_connected=True)
        except:
            raise Http404('ELN not available')
        return render(request, self.template_name)