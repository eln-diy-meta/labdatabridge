from django.urls import path, re_path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('settings_xxx/<str:uuid>', sdc_views.SettingsXxx.as_view(), name='scd_view_settings_xxx'),
]
