from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models




class XxxConnection(models.Model, SdcModel):
    class SearchForm(AbstractSearchForm):
        """A default search form used in the list view. You can delete it if you dont need it"""
        CHOICES = (("id", "Id"),)
        PLACEHOLDER = ""
        DEFAULT_CHOICES = CHOICES[0][0]
        SEARCH_FIELDS = ("id",)

    class _SdcMeta:
        """Meta data information needed to manage all SDC operations."""
        edit_form = "xxx_app.forms.XxxConnectionForm"
        create_form = "xxx_app.forms.XxxConnectionForm"
        html_list_template = "xxx_app/models/XxxConnection/XxxConnection_list.html"
        html_detail_template = "xxx_app/models/XxxConnection/XxxConnection_details.html"

    # The following properties are just the most common ones.
    #  ToDo Check the properties you need to to establish a connection to Xxx
    token = models.CharField(max_length=255, null=True, help_text="Please enter a valid API key to authenticate in Xxx!")
    url = models.URLField(verbose_name="URL", null=True, help_text="Please enter a valid host URL for a Xxx instance!")
    verify_ssl = models.BooleanField(default=True, blank=True, help_text="Uncheck this box if you are using a self-signed SSL certificate!")

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.SdcMeta.html_list_template:
            sf = cls.SearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf,  range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()
