from django import forms
from django.core.exceptions import ValidationError

from xxx_app.models import XxxConnection
from django.forms.models import ModelForm


# Form Model XxxConnection

class XxxConnectionForm(ModelForm):
    is_connected = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = XxxConnection
        fields = ['name', 'url', 'verify_ssl', 'token', 'is_connected']



    def clean(self):
        super().clean()
        token = self.cleaned_data.get('token', '')
        url = self.cleaned_data.get('url', '')
        verify_ssl = self.cleaned_data.get('verify_ssl', True)
        try:
            # Check whether the user's details are
            # correct and whether authentication in Xxx is possible.
            # ToDo Implement a connection test
            raise NotImplementedError("Check whether the user's details")
        except Exception as e:
            self.cleaned_data['is_connected'] = False
            raise ValidationError("Connection to Xxx failed")

        self.cleaned_data['is_connected'] = True
