from django.apps import AppConfig
from django.core.exceptions import ObjectDoesNotExist

from main_app.models import ElnType


class XxxAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'xxx_app'

    def ready(self):
        from xxx_app.sync_manager.xxx import XxxSyncer
        from sync_tools.manager import register_manager
        register_manager('Xxx', XxxSyncer)
        try:
            ElnType.objects.get(name="Xxx")
        except ObjectDoesNotExist:
            ElnType.objects.crate(name="Xxx", model_import_path="xxx.models.XxxeConnection", settings_controller="settings_xxx")
