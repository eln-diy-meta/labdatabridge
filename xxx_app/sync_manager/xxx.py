import datetime
from typing import Callable

from sync_tools.abstract_manager import AbstractManager
from sync_tools.options import ELN_TYPE


class XxxSyncer(AbstractManager):


    def __init__(self, project_uuid: str, eln_a_or_b: ELN_TYPE):
        super().__init__(project_uuid, eln_a_or_b)
        # ToDo: Establish Connection to Xxx


    def syncable_models(self) -> list[tuple[str, str]]:
        """
        Return a list of all synchronizable models or types.
        Each entry has to be a tuple in the form (<INTERNAL NAME>, <READABLE NAME>)

        :return: list of all synchronizable models
        """

        # ToDo: Return a list of all synchronizable models

        return []


    def load_list_for_example_selection(self, sync_model_name: str, **kwargs) -> list[tuple[int, str]]:
        """
        Return a list of objects from Xxx which can be used to setup the mapping.
        Each entry has to be a tuple in the form (<BB ID>, <READABLE NAME>)

        :param sync_model_name: internal name of the type/model

        :return:list of all synchronizable models
        """

        # ToDo: Add load a list of all objects of given type

        return super().load_list_for_example_selection(sync_model_name)

    def read_one(self, sync_model_name: str, id: int, **kwargs) -> dict:
        """
        Returns a JSON representation of a single object for a specific type and ID.
        The JSON must be in the same schema that is used as a parameter in the write method.

        :param sync_model_name: internal name of the type/model
        :param id: Xxx platform id

        :return: a JSON representation of a single
        """

        # ToDo: Load a single object and parse it ot a simple JSON format

        return super().read_one(sync_model_name, id)

    def read(self, sync_model_name: str, last_update: datetime.datetime = None) -> list[dict]:
        """
        Returns a list of JSON representations of all objects of a specific type that were updated after last_update.
        The JSON of each entry must be in the same schema that is used as a parameter in the write method.

        :param sync_model_name: internal name of the type/model
        :param last_update: last update threshold

        :return: a list of JSON representations of all objects
        """

        # ToDo: Load all objects and parse them to a simple JSON format

        return super().read(sync_model_name, last_update)

    def write(self, sync_model_name: str, changed_element: dict):
        """
        Uses the changed_element parameter to save
        all values to the responding object at Xxx

        :param sync_model_name: internal name of the type/model
        :param changed_element: changed/updated json representation
        """

        # ToDo: Save changes to Xxx

        super().write(sync_model_name, changed_element)

    def create_new(self, sync_model_name: str, sync_key: str | None, sync_val_key: any, setter: Callable[[dict], None], **kwargs):
        """
        Creates a new entry in Xxx of a certain type. As additional parameters, the JSON key and the corresponding value
        are given on the basis of which the mapping parameter is found.
        However, you can use the setter parameter to specify the value of the json representation

        :param sync_model_name: The name of the model type
        :param sync_key: The key path in the JSON to the sync value
        :param sync_val_key: The sync mapping value
        :param setter: The setter is a callable function. Call it with the JSON obj as argument to automatically set the 'sync_key' at 'sync_val_key'

        :return: Returns the JSON object of the newly generated obj.
        """

        # ToDo: Create a new Entry of a given type in Xxx

        return super().create_new(sync_model_name,sync_key, sync_val_key, setter)
