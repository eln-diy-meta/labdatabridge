from django.contrib import admin
from xxx_app.models import XxxConnection

# Register your models here.
@admin.register(XxxConnection)
class XxxConnectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'url', "token" , 'is_connected']