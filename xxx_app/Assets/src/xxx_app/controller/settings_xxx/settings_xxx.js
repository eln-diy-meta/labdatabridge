import {AbstractSDC, app} from 'sdc_client';


class SettingsXxxController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/xxx_app/settings_xxx/%(uuid)s"; //<settings-x-x-x data-uuid=""></settings-x-x-x>


        /**
         * Uncomment the following line to make sure the HTML template
         * of this controller is not cached and reloaded for every instance
         * of this controller.
         */
        // this.contentReload = true;

        /**
         * Uncomment the following line to make this controller asynchronous.
         * This means that the parent controller finishes loading without
         * waiting for this controller
         */
        // this.load_async = true;

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncomment the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(uuid, pos) {
        this.connection_model = this.newModel('XxxConnection', {uuid: uuid, connection_position: pos});
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        this.find('.connection-form').append(this.connection_model.editForm(-1, () => {
            this.generalSyncOptions = this.newModel(this.connection_model.values.sync_settings);
            this.find('.sync-setting-form').append(this.generalSyncOptions.editForm(-1, () => {
                this.update_settings_form();
                return super.willShow();
            }));
        }));
    }

    onRefresh() {
        return super.onRefresh();
    }


    update_settings_form() {
        if (this.find('#id_sync_all_existing')[0].checked) {
            this.find('.form-group.id_datetime_threshold').hide();
        } else {
            this.find('.form-group.id_datetime_threshold').show();
        }
    }

    onChange($input) {
        $input.closest('form').submit();
    }

    submitModelForm($form, e) {
        let self = this;
        return super.defaultSubmitModelForm($form, e).then(function () {
            self.update_settings_form();
        }).catch(() => {
            self.update_settings_form();
        });
    }
}

app.register(SettingsXxxController);