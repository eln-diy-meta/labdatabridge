/**
 * @jest-environment jsdom
 */

import {test_utils} from 'sdc_client';
import {} from "#root/src/xxx_app/xxx_app.organizer.js";
import '#root/src/sdc_tools/sdc_tools.organizer.js'
import '#root/src/sdc_user/sdc_user.organizer.js'

describe('SettingsXxx', () => {
    let controller;

    beforeEach(async () => {
        // Create new controller instance based on the standard process.
        controller = await test_utils.get_controller('settings-xxx',
                                                  {},
                                                  '<div><h1>Controller Loaded</h1></div>');
    });

    test('Load Content', async () => {
        const $div = $('body').find('settings-xxx');
        expect($div.length).toBeGreaterThan(0);
    });

});