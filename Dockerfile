FROM python:3.12 as client_build
RUN apt-get update

ENV NODE_VERSION=22.8.0
RUN apt install -y curl
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

RUN mkdir -p /srv/app
RUN mkdir -p /srv/app/private_libs
WORKDIR /srv/app

COPY . .

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

RUN npm install

RUN python manage.py sdc_update_links
RUN npm run build
RUN python manage.py collectstatic  --no-input

FROM python:3.12
LABEL authors="martin"
# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /srv/app
RUN mkdir -p /srv/app/private_libs
WORKDIR /srv/app

RUN apt-get update && apt-get install -y nginx nodejs npm cron nano redis-server

# install dependencies
COPY requirements.txt .
COPY exported_requirements.txt .
COPY private_libs private_libs
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install daphne

COPY . .
RUN mv /srv/app/etc_config/cron_entrypoint.sh /srv/app/cron_entrypoint.sh
RUN mv /srv/app/etc_config/entrypoint.sh /srv/app/entrypoint.sh
RUN chmod +x /srv/app/cron_entrypoint.sh
RUN chmod +x /srv/app/entrypoint.sh

COPY etc_config/default.conf /etc/nginx/sites-enabled/default
COPY --from=client_build /srv/app/www /usr/share/nginx/static
COPY media /usr/share/nginx/media

EXPOSE 80
RUN touch /var/log/cron.log

#RUN echo "SHELL=/bin/bash" > /etc/cron.d/my_custom_cron
#RUN echo "PATH=/sbin:/bin:/usr/sbin:/usr/bin" >> /etc/cron.d/my_custom_cron

RUN echo "*/1 * * * * bash -c '/srv/app/cron_entrypoint.sh' >> /var/log/cron.log 2>&1" > /etc/cron.d/my_custom_cron

RUN chmod 0644 /etc/cron.d/my_custom_cron
RUN /usr/bin/crontab /etc/cron.d/my_custom_cron

RUN mkdir /srv/app/node_modules
RUN mkdir /srv/app/static
RUN service cron start

ENTRYPOINT ["bash", "/srv/app/entrypoint.sh"]