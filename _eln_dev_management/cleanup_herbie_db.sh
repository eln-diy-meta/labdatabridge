HERBIEPATH="${HERBIEPATH:-../../../herbie-dev}"
DJANGO_PORT="${DJANGO_PORT:-4500}"


HERBIEPATH="$(cd "$(dirname "${HERBIEPATH}")"; pwd)/$(basename "${HERBIEPATH}")"
echo "Herbie path: $HERBIEPATH"
BASEDIR=$(dirname $0)
BASEDIR="$(cd "$(dirname "${BASEDIR}")"; pwd)"
echo "Script path: $BASEDIR"

cd $HERBIEPATH

if [ "$( PGPASSWORD=secret psql -h localhost -p 5432 -U admin herbie -XtAc "SELECT 1 FROM pg_database WHERE datname='herbie'" )" = '1' ]
then
    echo "Database available exists"
else
    docker-compose up -d
    sleep 2
    echo "Database not available pleas Check"
fi



PYTHON=$HERBIEPATH/venv/bin/python

$PYTHON $HERBIEPATH/manage.py flush --noinput
$PYTHON $HERBIEPATH/manage.py loaddata --exclude auth.permission --exclude contenttypes $BASEDIR/dumps/herbie/latest.json

if lsof -Pi :$DJANGO_PORT -sTCP:LISTEN -t >/dev/null ; then
    lsof -i tcp:$DJANGO_PORT | awk 'NR!=1 {print $2}' | xargs -r kill
    echo "Chemotion is running (or port ${DJANGO_PORT} is used by another app)"
fi