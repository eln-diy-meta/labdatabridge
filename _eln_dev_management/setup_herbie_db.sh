HERBIEPATH="${HERBIEPATH:-../../../herbie-dev}"
DJANGO_PORT="${DJANGO_PORT:-4500}"


HERBIEPATH="$(cd "$(dirname "${HERBIEPATH}")"; pwd)/$(basename "${HERBIEPATH}")"
echo "Herbie path: $HERBIEPATH"
BASEDIR=$(dirname $0)
BASEDIR="$(cd "$(dirname "${BASEDIR}")"; pwd)"
echo "Script path: $BASEDIR"

cd $HERBIEPATH

if [ "$( PGPASSWORD=secret psql -h localhost -p 5432 -U admin herbie -XtAc "SELECT 1 FROM pg_database WHERE datname='herbie'" )" = '1' ]
then
    echo "Database available exists"
else
    docker-compose up -d
    sleep 2
    echo "Database not available pleas Check"
fi



PYTHON=$HERBIEPATH/venv/bin/python
timestamp=$(date +%Y_%m_%d_%H_%M_%S_%N)
mkdir -p $BASEDIR/dumps/herbie
$PYTHON $HERBIEPATH/manage.py dumpdata --exclude auth.permission --exclude contenttypes  > $BASEDIR/dumps/herbie/$timestamp.json
$PYTHON $HERBIEPATH/manage.py flush --noinput
$PYTHON $HERBIEPATH/manage.py loaddata --exclude auth.permission --exclude contenttypes $BASEDIR/herbie_test_base_dump.json

rm $BASEDIR/dumps/herbie/latest.json
ln -s $BASEDIR/dumps/herbie/$timestamp.json $BASEDIR/dumps/herbie/latest.json

echo $BASEDIR/dumps/$timestamp.json

$PYTHON $HERBIEPATH/manage.py runserver 0.0.0.0:$DJANGO_PORT