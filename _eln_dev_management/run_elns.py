import configparser
import os
import shutil
import subprocess
from threading import Thread
import atexit

from chemotion_api.commands import InstanceManager
# Herbie:
# URL: http://localhost:4500
# Token ed789231902df05fa7b01caa3dbe25e8f7943227ba0d495e492b212f8d3bd5b1

# Chemotion:
# URL: http://localhost:3354
# User: MSU
# Password: 1234qweR!

config = configparser.ConfigParser()
config.read('./eln_dev_config.ini')
def main_chemotion():
    global config
    df = config['CHEMOTION']['CHEMOTION_DOCKER_COMPOSE_FILE']
    im = InstanceManager(df)
    try:
        im.new_instance(postgres_port=5532)
    except:
        pass
    im.up(eln=True, db=True)
    os.makedirs('./dumps/chemotion', exist_ok=True)
    f = im.backup(port=im.db_port)
    im.restore('./chemotion_test_base_dump.tar', port=im.db_port)
    res = os.path.abspath(shutil.move(f, './dumps/chemotion'))
    latest = os.path.join(os.path.dirname(res), 'latest.tar')
    try:
        os.unlink(latest)
    except FileNotFoundError:
        pass
    os.symlink(res, latest)



def stop_chemotion():
    global config
    df = config['CHEMOTION']['CHEMOTION_DOCKER_COMPOSE_FILE']
    im = InstanceManager(df)
    im.restore('./dumps/chemotion/latest.tar', port=im.db_port)
    im.stop()


def main_herbie():
    global config
    try:
        df = config['HERBIE']['HERBIE_PROJECT_DIR']
        my_env = os.environ.copy()
        my_env["HERBIEPATH"] = df
        my_env["DJANGO_PORT"] = '4500'
        my_env["DJANGO_IRI_PREFIX"] = f'http://localhost:{my_env["DJANGO_PORT"]}/'
        command = ['/bin/sh', './setup_herbie_db.sh']

        proc = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=my_env)
        a,b = proc.communicate()
        print(a.decode())
        if proc.returncode == 0:
            pass #raise Exception()
    except Exception as e:
        pass

def stop_herbie():
    global config
    try:
        df = config['HERBIE']['HERBIE_PROJECT_DIR']
        my_env = os.environ.copy()
        my_env["HERBIEPATH"] = df
        my_env["DJANGO_PORT"] = '4500'
        my_env["DJANGO_IRI_PREFIX"] = f'http://localhost:{my_env["DJANGO_PORT"]}/'
        command = ['/bin/sh', './cleanup_herbie_db.sh']

        proc = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=my_env)
        a,b = proc.communicate()
        print(a.decode())
        if proc.returncode == 0:
            pass #raise Exception()
    except Exception as e:
        pass

def main():
    t1 = Thread(target=main_herbie)
    t2 = Thread(target=main_chemotion)

    def exit_handler():
        stop_chemotion()
        stop_herbie()
        print('clean_up is done')

    atexit.register(exit_handler)
    # start the new thread
    t1.daemon = True
    t2.daemon = True
    t1.start()
    t2.start()
    # wait for the new thread to finish
    while True:
        pass


if __name__ == '__main__':
    main()