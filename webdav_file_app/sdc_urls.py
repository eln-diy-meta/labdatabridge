from django.urls import path, re_path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('webdav_settings/<str:uuid>', sdc_views.WebdavSettings.as_view(), name='scd_view_webdav_settings'),
    path('path_search_field', sdc_views.PathSearchField.as_view(), name='scd_view_path_search_field'),
]
