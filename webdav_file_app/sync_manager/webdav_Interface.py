from datetime import datetime
from typing import Callable

from webdav_file_app.models import WebdavFileConnection
from sync_tools.abstract_manager import AbstractManager
from sync_tools.options import ELN_TYPE


class EmptyPlaceholderElement():

    def __init__(self):
        self.id = None

    def save(self):
        pass

    def split(self):
        return self


class WebdavSyncer(AbstractManager):

    def __init__(self, project_uuid: str, eln_a_or_b: ELN_TYPE):
        super().__init__(project_uuid, eln_a_or_b)
        self.eln_connection = WebdavFileConnection.objects.get(uuid=project_uuid, connection_position=eln_a_or_b)
        self.eln_connection.load_files()
        self._file_path = self.eln_connection.file_path


    def syncable_models(self) -> list[tuple[str, str]]:
        ret_val = []
        for model in self.eln_connection.get_models():
            ret_val.append((model[0], model[0]))
        return ret_val

    def load_list_for_example_selection(self, sync_model_name: str, **kwargs) -> list:
        return [(x[0]['id'], x[1],) for x in self._load_obj_list(sync_model_name)]

    def read_one(self, sync_model_name: str, id: int, **kwargs) -> dict:
        return self._load_single_obj(sync_model_name, id)

    def read(self, sync_model_name: str, last_update: datetime = None):
        # noinspection PyUnresolvedReferences
        return [p[0] for p in self._load_obj_list(sync_model_name)]

    def write(self, sync_model_name: str, src_elem: dict):
        target_elem = self.eln_connection.read_file(src_elem['id'], clean=True)
        res_target_elem = self._iter_write(src_elem, target_elem)
        self.eln_connection.write_file(src_elem['id'], res_target_elem)

    def _iter_write(self, src, target):
        if isinstance(src, dict):
            if not isinstance(target, dict):
                target = dict()
            for k,v in src.items():
                target[k] = self._iter_write(v, target.get(k))
            return target
        elif isinstance(src, list):
            if not isinstance(target, list):
                target = list()
            while len(target) < len(src):
                target.append(None)
            for k,v in enumerate(src):
                target[k] = self._iter_write(v, target[k])
            return target[:len(src)]

        return src

    def create_new(self, sync_model_name: str, sync_key: str | None, sync_val_key: any, setter: Callable[[dict], None], **kwargs):
        raise NotImplementedError()

    def _load_single_obj(self, sync_model_name: str, id: int):
        return self.eln_connection.read_file(id)

    def _load_obj_list(self, sync_model_name: str) -> list[dict]:
        for obj_item in self.eln_connection.all_files_of_model(sync_model_name):
            yield (obj_item.read_file(), obj_item.path_extension )

    def clean_up(self, *args, **kwargs):
        self.eln_connection.push()