from django.apps import AppConfig




class WebdavFileAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'webdav_file_app'

    def ready(self):
        from webdav_file_app.sync_manager.webdav_Interface import WebdavSyncer
        from sync_tools.manager import register_manager
        register_manager('WebdavROCrate', WebdavSyncer)
