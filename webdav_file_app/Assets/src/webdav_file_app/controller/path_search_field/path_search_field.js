import {AbstractSDC, app} from 'sdc_client';


class PathSearchFieldController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/webdav_file_app/path_search_field"; //<path-search-field></path-search-field>
        this.load_async = true;
        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit() {
    }

    onLoad($html) {
        $html.filter('.main_container').empty().append(this.$container.find('.main-input-container'));
        return super.onLoad($html);
    }

    willShow() {
        return super.willShow();
    }

    onRefresh() {
        return super.onRefresh();
    }

    click_path($btn){
        this.find('#id_file_path').val($btn.text());
        this.find('#id_file_path').trigger('change');
    }

    submit_model_form_success(res) {
        this.find('.loading-p').hide();
        const new_form = res?.html;
        if (new_form) {
            this.find('.path_search_options').empty().html($(new_form).find('.path_search_options').html());
            this.filterOptions(this.find('#id_file_path'));
        }
    }

    onChange() {
        this.find('.loading-p').show();
    }

    filterOptions($input) {
        const val = $input.val();
        this.find('.path_search_options p').each(function() {
            const $this = $(this);
            if($this.text().startsWith(val)) {
                $this.show();
            } else {
                $this.hide();
            }
        });
    }

    submit_model_form_error(res) {
        this.submit_model_form_success(res);
    }

}

app.register(PathSearchFieldController);