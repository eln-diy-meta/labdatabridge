import os
import shutil
import tempfile
from urllib.parse import urlparse

from django.core.exceptions import ValidationError
from django.forms import Widget

from django.template import loader
from django.utils.safestring import mark_safe
from nextcloud_client import nextcloud_client

from webdav_file_app.models import WebdavFileSettings, WebdavClient
from webdav_file_app.models import WebdavFileConnection
from django.forms.models import ModelForm
from webdav3.client import Client

from django import forms


class PathSearchFieldWidget(Widget):
    template_name = 'webdav_file_app/path_search_field_widget.html'

    def __init__(self, client: WebdavClient, attrs=None):
        super().__init__(attrs)
        self._client = client

    def _get_options(self, v_path):
        if v_path is None or v_path == '':
            v_path = '/'
        try:
            res = [v_path]
            for x in self._client.list_dirs(v_path):
                    res.append(x)
            return res
        except:
            if v_path == '/':
                raise ConnectionError('Could not list dirs')
            new_v_path = os.path.dirname(v_path)
            if new_v_path == v_path:
                new_v_path = '/'

            return self._get_options(new_v_path)

    def render(self, name, value, attrs=None, renderer=None):
        context = self.get_context(name, value, attrs)
        context['options'] = self._get_options(value)
        template = loader.get_template(self.template_name).render(context)
        return mark_safe(template)




class WebdavFileConnectionForm(ModelForm):
    is_connected = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = WebdavFileConnection
        fields = ['name', 'server_type', 'url', 'user', 'password', 'is_connected']

    def clean(self):
        super().clean()
        password = self.cleaned_data.get('password', '')
        url = self.cleaned_data.get('url', '')
        user = self.cleaned_data.get('user', '')



        if len(self.errors) == 0:
            temp_dir = tempfile.mkdtemp()
            try:
                if self.cleaned_data['server_type'] == 'webdav':
                    options = {
                        'webdav_hostname': url,
                        'webdav_login': user,
                        'webdav_password': password,
                        'webdav_timeout': 30
                    }
                    client = Client(options)
                    client.list()
                elif self.cleaned_data['server_type'] == 'nextcloud':
                    url = urlparse(url)
                    url = f'{url.scheme}://{url.hostname}'
                    nc = nextcloud_client.Client(url)
                    nc.login(user, password)
            except Exception as e:
                if self.instance is not None:
                    self.instance.is_connected = False
                    self.instance.save()
                raise ValidationError("Connection to the git failed")
            finally:
                if os.path.exists(temp_dir):
                    shutil.rmtree(temp_dir)
            self.cleaned_data['is_connected'] = True


# Form Model WebdavFileSettings

class WebdavFileSettingsForm(ModelForm):

    class Meta:
        model = WebdavFileSettings
        fields = ("file_path",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance is not None:
            self.fields['file_path'].widget = PathSearchFieldWidget(client=WebdavClient(self.instance.connection))

    def clean(self):
        client = WebdavClient(self.instance.connection)
        try:
            next(client.list_dirs(self.cleaned_data['file_path']), None)
        except:
            self.add_error('file_path', f'{self.cleaned_data["file_path"]} does not exist!')
        shutil.rmtree(self.instance.connection.file_path, ignore_errors=True)

    def clean_file_path(self):
        message = self.cleaned_data['file_path']
        if message is None or len(message) == 0:
            return '/'
        return message