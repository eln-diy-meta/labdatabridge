from sdc_core.sdc_extentions.views import SDCView
from django.shortcuts import render



class PathSearchField(SDCView):
    template_name='webdav_file_app/sdc/path_search_field.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)

class WebdavSettings(SDCView):
    template_name='webdav_file_app/sdc/webdav_settings.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)