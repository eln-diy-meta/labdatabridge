import os.path
import shutil
from urllib.parse import urlparse

import nextcloud_client
from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models

from file_app_utils.models import AbstractFileConnection
from sync_tools.models import AbstractConnection
from webdav3.client import Client


# Create your models here.
class WebdavFileSettingsSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = ""
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class WebdavFileSettings(models.Model, SdcModel):
    edit_form = "webdav_file_app.forms.WebdavFileSettingsForm"
    create_form = "webdav_file_app.forms.WebdavFileSettingsForm"
    html_list_template = "webdav_file_app/models/WebdavFileSettings/WebdavFileSettings_list.html"
    html_detail_template = "webdav_file_app/models/WebdavFileSettings/WebdavFileSettings_details.html"

    file_path = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.connection.name} settings'

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = WebdavFileSettingsSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


def default_webdav_file_settings():
    return WebdavFileSettings.objects.create().pk


class WebdavFileConnectionSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = ""
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


SERVER_TYPES = (
    ('webdav', 'WebDAV'),
    ('nextcloud', 'Nextcloud'),
)


class WebdavFileConnection(AbstractConnection, AbstractFileConnection, SdcModel):
    edit_form = "webdav_file_app.forms.WebdavFileConnectionForm"
    create_form = "webdav_file_app.forms.WebdavFileConnectionForm"
    html_list_template = "webdav_file_app/models/WebdavFileConnection/WebdavFileConnection_list.html"
    html_detail_template = "webdav_file_app/models/WebdavFileConnection/WebdavFileConnection_details.html"

    password = models.CharField(max_length=255, null=True)
    user = models.CharField(max_length=255, null=True)
    url = models.URLField(verbose_name="URL", null=True)

    server_type = models.CharField(max_length=100, choices=SERVER_TYPES, default=SERVER_TYPES[1][0])

    settings = models.OneToOneField(WebdavFileSettings, default=default_webdav_file_settings,
                                    on_delete=models.CASCADE,
                                    related_name='connection')

    def __str__(self):
        return self.url

    def load_files(self):
        WebdavClient(self).download_dir(remote_dir=self.settings.file_path, local_dir=self.file_path)
        super().load_files()

    def push(self):
        WebdavClient(self).upload_dir(remote_dir=self.settings.file_path, local_dir=self.file_path)

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = WebdavFileConnectionSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()


class WebdavClient():
    def __init__(self, con: WebdavFileConnection):
        self._password = con.password
        self._user = con.user
        self._url = con.url
        self._server_type = con.server_type
        self._nc = None
        self._wc = None

    @property
    def client(self):
        if self._server_type == 'nextcloud':
            return self._nextcloud_client()
        elif self._server_type == 'webdav':
            return self._webdav_client()
        raise ValueError(f"Server type: {self._server_type} must be in [webdav, nextcloud]")

    def download_dir(self, remote_dir, local_dir):
        os.makedirs(local_dir, exist_ok=True)
        if self._server_type == 'nextcloud':
            return self._nextcloud_download_dir(remote_dir, local_dir)
        elif self._server_type == 'webdav':
            return self._webdav_download_dir(remote_dir, local_dir)
        raise ValueError(f"Server type: {self._server_type} must be in [webdav, nextcloud]")

    def upload_dir(self, remote_dir, local_dir):
        if self._server_type == 'nextcloud':
            return self._nextcloud_upload_dir(remote_dir, local_dir)
        elif self._server_type == 'webdav':
            return self._webdav_upload_dir(remote_dir, local_dir)
        raise ValueError(f"Server type: {self._server_type} must be in [webdav, nextcloud]")

    def list_dirs(self, remote_dir):
        if self._server_type == 'nextcloud':
            return self._nextcloud_list_dirs(remote_dir)
        elif self._server_type == 'webdav':
            return self._webdav_list_dirs(remote_dir)
        raise ValueError(f"Server type: {self._server_type} must be in [webdav, nextcloud]")

    def _nextcloud_list_dirs(self, remote_dir):
        all_files_folders = self.client.list(remote_dir)
        for node in all_files_folders:
            if node.is_dir():
                yield node.path

    def _webdav_list_dirs(self, remote_dir):
        path_list = self.client.list(remote_dir, get_info=True)
        root_dir = path_list[0]['path']
        for x in path_list[1:]:
            if x['isdir']:
                new_path = x['path'][len(root_dir):]
                new_path = os.path.join(remote_dir, new_path)[:-1]
                yield new_path

    def _nextcloud_download_dir(self, remote_dir, local_dir):
        nc = self.client
        all_files_folders = nc.list(remote_dir)
        local_files = os.listdir(local_dir)
        for node in all_files_folders:
            local_target_path = os.path.join(local_dir, node.name)
            last_mod = 0
            remote_last_modified = node.get_last_modified().timestamp()
            if node.name in local_files:
                local_files.remove(node.name)
                last_mod = os.stat(local_target_path).st_mtime
            if node.is_dir():
                self._nextcloud_download_dir(node.path, local_target_path)
            elif remote_last_modified > last_mod:
                nc.get_file(node.path, local_target_path)
                os.utime(local_target_path, (remote_last_modified, remote_last_modified))
        for element in local_files:
            element_path = os.path.join(local_dir, element)
            if os.path.isdir(element_path):
                shutil.rmtree(element_path)
            else:
                os.remove(element_path)

    def _webdav_download_dir(self, remote_dir, local_dir):
        client = self.client
        if not os.path.exists(remote_dir):
            client.download_sync(remote_path=remote_dir, local_path=local_dir)
        else:
            client.pull(remote_directory=remote_dir, local_directory=local_dir)

    def _webdav_client(self):
        if self._wc is None:
            self._wc = Client({
                'webdav_hostname': self._url,
                'webdav_login': self._user,
                'webdav_password': self._password,
                'webdav_timeout': 30,
            })
        return self._wc

    def _nextcloud_client(self):
        if self._nc is None:
            url = urlparse(self._url)
            url = f'{url.scheme}://{url.hostname}'
            self._nc = nextcloud_client.Client(url)
            self._nc.login(self._user, self._password)
        return self._nc

    def _nextcloud_upload_dir(self, remote_dir, local_dir):
        nc = self.client
        all_files_folders = nc.list(remote_dir)
        local_files = os.listdir(local_dir)
        for node_name in local_files:
            local_target_path = os.path.join(local_dir, node_name)

            node = next((x for x in all_files_folders if x.name == node_name), None)
            last_mod = 0
            if node is not None:
                last_mod = node.get_last_modified().timestamp()
            if os.path.isdir(local_target_path):
                if node is not None:
                    self._nextcloud_upload_dir(node.path, local_target_path)
            elif os.stat(local_target_path).st_mtime > last_mod:
                nc.put_file(node.path, local_target_path)

    def _webdav_upload_dir(self, remote_dir, local_dir):
        client = self.client
        client.push(remote_directory=remote_dir, local_directory=local_dir)

