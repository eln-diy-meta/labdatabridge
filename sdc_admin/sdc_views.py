from sdc_core.sdc_extentions.views import SDCView
from django.shortcuts import render



class SdcAdminDashboard(SDCView):
    template_name='sdc_admin/sdc/sdc_admin_dashboard.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)