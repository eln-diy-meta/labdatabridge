from django.apps import AppConfig


class SdcAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sdc_admin'
