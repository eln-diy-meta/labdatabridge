from django.urls import path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('sdc_admin_dashboard', sdc_views.SdcAdminDashboard.as_view(), name='scd_view_sdc_admin_dashboard'),
]
