import {} from "./webdav_file_app/webdav_file_app.organizer.js";
import {} from "./git_file_app/git_file_app.organizer.js";
import {} from "./kadi_app/kadi_app.organizer.js";
import {} from "./herbie_app/herbie_app.organizer.js";
import {} from "./chemotion_app/chemotion_app.organizer.js";
import {} from "./main_app/main_app.organizer.js";
import {} from "./sdc_admin/sdc_admin.organizer.js";
import {} from "./sdc_tools/sdc_tools.organizer.js";
import {} from "./sdc_user/sdc_user.organizer.js";
import {app} from 'sdc_client';

import('jquery').then(({default: $}) => {
    window['jQuery'] = window['$'] = $;
    Promise.all([import('bootstrap/dist/js/bootstrap.bundle.js'), import('cytoscape'), import('cytoscape-euler'), import('lodash')])
        .then(([bootstrap, cytoscape, cytoscapeEuler, lodash]) => {
        window['Modal'] = bootstrap.Modal;
        window['cytoscape'] = cytoscape.cytoscape;
        window['euler'] = cytoscapeEuler.euler;
        window['_'] = lodash.default;
        app.init_sdc().then(() => {
        });
    });
});