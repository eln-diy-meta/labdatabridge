from django.forms import ModelForm, DateTimeInput, DateTimeField

from sync_tools.models import GeneralSyncOptions



# Form Model GeneralSyncOptions




class GeneralSyncOptionsForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['datetime_threshold'].widget.input_type = 'datetime-local'

    class Meta:
        model = GeneralSyncOptions
        exclude = ['uuid']
