import abc
from datetime import datetime

from sync_tools.options import ELN_TYPE
from typing import Callable

class AbstractManager(abc.ABC):
    _eln_a_or_b: ELN_TYPE
    _project_uuid: str

    def __init__(self, project_uuid: str, eln_a_or_b: ELN_TYPE):
        self._eln_a_or_b: ELN_TYPE = eln_a_or_b
        self._project_uuid: str = project_uuid

    def syncable_models(slef) -> list[tuple[str, str]]:
        """
        Returns a tuple-list of Element names which can be synced with this system.
        Each element in the list should be a tuple with two strings, the internal name and a human-readable name.

        :return: A list for a selection process.
        """
        raise NotImplementedError("The syncable_models has to be implemented")

    def read_one(self, sync_model_name: str, id: int | str, **kwargs) -> dict:
        raise NotImplementedError(f"load_single_obj_json has to be overwritten!")

    def load_list_for_example_selection(self, sync_model_name: str, **kwargs) -> list[tuple[int, str]]:
        raise NotImplementedError(f"load_list_for_example_selection has to be overwritten!")

    def read(self, sync_model_name: str, last_update: datetime = None):
        raise NotImplementedError(f"read has to be overwritten!")

    def write(self, sync_model_name: str, elem: dict):
        raise NotImplementedError(f"write has to be overwritten!")

    def create_new(self, sync_model_name: str, sync_key: str | None, sync_val_key: any, setter: Callable[[dict], None],**kwargs):
        """
        Create a new entry in the ELN

        :param sync_model_name: The name of the model type
        :param sync_key: The key path in the JSON to the sync value
        :param sync_val_key: The sync mapping value
        :param setter: The setter is a callable function. Call it with the json obj as argument to automatically set the 'sync_key' at 'sync_val_key'
        :param kwargs:

        :return: Returns the JSON object of the newly generated obj.
        """
        raise NotImplementedError(f"create a new element. This methode has to be overwritten!")

    def clean_up(self, *args, **kwargs):
        """
        Called after the process is finished
        """
        pass