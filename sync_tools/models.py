from django.utils import timezone
from django.utils.timezone import make_aware
from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from datetime import datetime

from django.db import models
from django.conf import settings
from sdc_core.sdc_extentions import import_manager

ConnectionModel = import_manager.import_function(settings.CONNECTION_MODEL)


class GeneralSyncOptionsSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = ""
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)

RW_TYPE = (
    ('w', 'Read & Write'),
    ('r', 'Read only'),
)
class GeneralSyncOptions(models.Model, SdcModel):
    edit_form = "sync_tools.forms.GeneralSyncOptionsForm"
    create_form = "sync_tools.forms.GeneralSyncOptionsForm"
    html_list_template = "sync_tools/models/GeneralSyncOptions/GeneralSyncOptions_list.html"
    html_detail_template = "sync_tools/models/GeneralSyncOptions/GeneralSyncOptions_details.html"

    rw_type = models.CharField(verbose_name="Read/Write Type", max_length=1, choices=RW_TYPE, default=RW_TYPE[0][0], help_text="")
    sync_all_existing = models.BooleanField(default=False, blank=True, help_text="If this field is selected, the datetime threshold is ignored and all available elements are included in the synchronization")
    datetime_threshold = models.DateTimeField(default=timezone.now, blank=True, help_text="All entries in your ELN before the selected time are ignored in the synchronization")
    allow_create = models.BooleanField(default=True, blank=True, help_text="Select here whether you allow the synchronization tool to create new entries in your ELN")
    uuid = models.CharField(max_length=255, null=True)

    def __str__(self):
        return f'Settings ({self.uuid})'

    @classmethod
    def create_new(cls):
        return cls.objects.create().pk

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):

        if template_name == cls.html_list_template:
            sf = GeneralSyncOptionsSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()

    def get_datetime_threshold(self) -> datetime:
        if self.sync_all_existing:
            naive = datetime(1980, 1, 1, 1, 1)
            return make_aware(naive)
        return self.datetime_threshold


class AbstractConnection(models.Model):
    is_connected = models.BooleanField(default=False)
    connection_model = models.ForeignKey(ConnectionModel, on_delete=models.CASCADE)
    uuid = models.CharField(max_length=255, editable=False)
    connection_position = models.CharField(max_length=2, default='a')

    name = models.CharField(max_length=255, blank=True, default="")
    sync_settings = models.ForeignKey(GeneralSyncOptions, default=GeneralSyncOptions.create_new, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True
        unique_together = ('connection_position', 'connection_model', 'uuid')

    def save(self, *args, **kwargs):
            if self.sync_settings.uuid is None:
                self.sync_settings.uuid = self.uuid
                self.sync_settings.save()
            super().save(*args, **kwargs)
