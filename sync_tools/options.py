from enum import Enum


class ELN_TYPE(Enum):
    A = "a"
    B = "b"

    def __str__(self):
        return str(self.value)

    def to_id(self):
        return f'id_{self}'

    @property
    def complement(self):
        if self == ELN_TYPE.A:
            return ELN_TYPE.B
        return ELN_TYPE.A