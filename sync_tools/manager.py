from sync_tools.abstract_manager import AbstractManager
from typing import Type
MANAGER: dict[str, Type[AbstractManager]] = {}


def register_manager(eln_name: str, manger: Type[AbstractManager]):
    MANAGER[eln_name.lower()] = manger


def get_manager(eln_name: str) -> Type[AbstractManager]:
    return MANAGER[eln_name.lower()]