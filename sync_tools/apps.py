from django.apps import AppConfig


class SyncToolsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sync_tools'
