class AbstractObj(dict):
    def __init__(self, id_a: int | None = None, id_b: int | None = None, **kwargs):
        kwargs['id_a'] = id_a
        kwargs['id_b'] = id_b
        super().__init__(**kwargs)

    @property
    def id_a(self) -> int:
        return self['id_a']

    @id_a.setter
    def id_a(self, id_a: int):
        self['id_a'] = id_a

    @property
    def id_b(self) -> int:
        return self['id_b']

    @id_b.setter
    def id_b(self, id_b: int):
        self['id_b'] = id_b


class SimpleObj(AbstractObj):
    pass


class Solvent(AbstractObj):

    def __init__(self, volume_ml: float, name: str, purity: int, id_a: int | None = None, id_b: int | None = None):
        kwargs: dict[str, any] = {}
        kwargs['volume_ml'] = volume_ml
        kwargs['purity'] = purity
        kwargs['name'] = name
        super().__init__(id_a, id_b, **kwargs)

    @property
    def volume_ml(self) -> float:
        return self['volume_ml']

    @volume_ml.setter
    def volume_ml(self, volume_ml: float):
        self['volume_ml'] = volume_ml

    @property
    def purity(self) -> int:
        return self['purity']

    @purity.setter
    def purity(self, purity: int):
        self['purity'] = purity

    @property
    def name(self) -> str:
        return self['name']

    @name.setter
    def name(self, name: str):
        self['name'] = name


class TimeTemperature(AbstractObj):

    def __init__(self, temperature: float, hour: int, minute: int, second: int, id_a: int | None = None,
                 id_b: int | None = None):
        kwargs: dict[str, int | float] = {}
        kwargs['hour'] = hour
        kwargs['minute'] = minute
        kwargs['second'] = second
        kwargs['temperature'] = temperature
        super().__init__(id_a, id_b, **kwargs)

    @property
    def hour(self) -> int:
        return self['hour']

    @hour.setter
    def hour(self, hour: int):
        self['hour'] = hour

    @property
    def minute(self) -> int:
        return self['minute']

    @minute.setter
    def minute(self, minute: int):
        self['minute'] = minute

    @property
    def second(self) -> int:
        return self['second']

    @second.setter
    def second(self, second: int):
        self['second'] = second

    @property
    def temperature(self) -> str:
        return self['temperature']

    @temperature.setter
    def temperature(self, temperature: float):
        self['temperature'] = temperature

    def get_time_string(self) -> str:
        h = str(self.hour).zfill(2)
        m = str(int(self.minute)).zfill(2)
        s = str(int(self.second)).zfill(2)
        return f"{h}:{m}:{s}"

    def get_hours_int(self) -> int:
        return max(1, 1 + self.hour + round(self.minute / 60))
