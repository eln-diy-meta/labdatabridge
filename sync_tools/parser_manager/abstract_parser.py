from sync_tools.options import ELN_TYPE
from sync_tools.parser_manager.type_classes import AbstractObj, SimpleObj, Solvent, TimeTemperature
from datetime import datetime


class AbstractPaserManager:

    def __init__(self, eln_a_or_b: ELN_TYPE):
        self._eln_a_or_b: ELN_TYPE = eln_a_or_b

    @classmethod
    def _check_if_altered(cls, new_sync_value, last_sync_value):
        if last_sync_value is None: return True
        return not new_sync_value == last_sync_value

    def _prepare_ids_obj(self, last_sync_value: AbstractObj | None, id: int | None = None):
        ids = {} if last_sync_value is None else {ELN_TYPE.A.to_id(): last_sync_value.id_a if last_sync_value is not None else None,
                                                  ELN_TYPE.B.to_id(): last_sync_value.id_b if last_sync_value is not None else None}
        if id is not None:
            ids[self._eln_a_or_b.to_id()] = id
        return ids

    def in_date_Y_m_d(self, value: str, last_sync_value: datetime | None = None) -> datetime | None:
        value = datetime.strptime(value, '%Y-%d-%m')
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_date_Y_m_d(self, value: datetime) -> str:
        return value.strftime('%Y-%d-%m')

    def in_date_timestamp(self, value: float, last_sync_value: datetime | None = None) -> datetime | None:
        value = datetime.fromtimestamp(value)
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_date_timestamp(self, value: datetime) -> float:
        raise value.timestamp()

    def in_date_obj(self, value: datetime, last_sync_value: datetime | None = None) -> datetime | None:
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_date_obj(self, value: datetime) -> datetime:
        return value

    def in_date_isoformat(self, value: str, last_sync_value: datetime | None = None) -> datetime | None:
        value = datetime.fromisoformat(value)
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_date_isoformat(self, value: datetime) -> str:
        return value.isoformat()

    def in_float(self, value: str | int | float, last_sync_value: float | None = None) -> float | None:
        value = float(value)
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_float(self, value: str | int | float) -> float:
        return float(value)

    def in_int(self, value: str | int | float, last_sync_value: float | None = None) -> int | None:
        value = int(value)
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_int(self, value: str | int | float) -> int:
        return int(value)

    def in_str(self, value: str | int | float, last_sync_value: str | None = None) -> str | None:
        value = str(value)
        if not self._check_if_altered(value, last_sync_value): return None
        return value

    def out_str(self, value: str | int | float) -> str:
        return str(value)

    def in_str_capitalize(self, value: str, last_sync_value: str | None = None) -> str | None:
        return self.in_str(value, last_sync_value)

    def out_str_capitalize(self, value: str) -> str:
        value = str(value).capitalize()
        return value

    def in_str_lower(self, value: str, last_sync_value: str | None = None) -> str | None:
        return self.in_str(value, last_sync_value)

    def out_str_lower(self, value: str) -> str:
        return str(value).lower()

    def in_sub_model(self, value: int | dict[int], last_sync_value: SimpleObj | None = None) -> SimpleObj | None:
        raise NotImplementedError()

    def out_sub_model(self, value: SimpleObj) -> int:
        raise NotImplementedError()

    def in_solvent(self, value: dict, last_sync_value: Solvent | None = None) -> Solvent | None:
        if not self._check_if_altered(value, last_sync_value): return None
        raise NotImplementedError()

    def out_solvent(self, value: Solvent) -> dict:
        raise NotImplementedError()

    def in_time_temperature(self, value: dict,
                            last_sync_value: TimeTemperature | None = None) -> TimeTemperature | None:
        if not self._check_if_altered(value, last_sync_value): return None
        raise NotImplementedError()

    def out_time_temperature(self, value: TimeTemperature) -> dict:
        raise NotImplementedError()
