#if [ ! -d "./venv" ]; then
#  python3 -m venv ./venv
#fi

#source venv/bin/activate

pip install -r requirements.txt

pip install private_libs/herbieapi-0.1.9.tar.gz
pip install private_libs/kadi4matapi-0.1.3.tar.gz
RUN pip install daphne

npm install
npm run build

python manage.py collectstatic
python manage.py migrate
python manage.py loaddata chemotion_herbie_eln_type.json

sudo echo "*/1 * * * * bash -c '/srv/app/cron_entrypoint.sh' >> /var/log/cron.log 2>&1" > /etc/cron.d/my_custom_cron
sudo chmod 0644 /etc/cron.d/my_custom_cron
sudo /usr/bin/crontab /etc/cron.d/my_custom_cron