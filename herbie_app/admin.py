from django.contrib import admin
from herbie_app.models import HerbieConnection

# Register your models here.
@admin.register(HerbieConnection)
class HerbieConnectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'url' , 'is_connected']