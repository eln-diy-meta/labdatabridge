from django.urls import path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('setup_herbie/<str:uuid>', sdc_views.SetupHerbie.as_view(), name='scd_view_setup_herbie'),
]
