from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models
from main_app.forms import ElnConnection
from sync_tools.models import AbstractConnection


# Create your models here.

class HerbieConnectionSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = "Id"
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)


class HerbieConnection(AbstractConnection, SdcModel):

    token = models.CharField(max_length=255, null=True)
    url = models.URLField(verbose_name="URL", null=True)
    verify_ssl = models.BooleanField(default=True, blank=True)

    edit_form = "herbie_app.forms.HerbieConnectionForm"
    create_form = "herbie_app.forms.HerbieConnectionForm"
    html_list_template = "herbie_app/models/HerbieConnection/HerbieConnection_list.html"
    html_detail_template = "herbie_app/models/HerbieConnection/HerbieConnection_details.html"

    def __str__(self):
        return self.url

    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = HerbieConnectionSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf, range=2)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()
