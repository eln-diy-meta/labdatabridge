from django.apps import AppConfig


class HerbieAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'herbie_app'


    def ready(self):
        from herbie_app.sync_manager.herbie import HerbieSyncer
        from sync_tools.manager import register_manager
        register_manager('herbie', HerbieSyncer)