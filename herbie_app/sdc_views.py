from django.http import Http404
from sdc_core.sdc_extentions.views import SDCView, SdcLoginRequiredMixin
from django.shortcuts import render

from herbie_app.models import HerbieConnection


class SetupHerbie(SdcLoginRequiredMixin, SDCView):
    template_name='herbie_app/sdc/setup_herbie.html'

    def get_content(self, request, uuid, *args, **kwargs):
        try:
            HerbieConnection.objects.get(uuid=uuid, is_connected=True)
        except:
            raise Http404('ELN not available')
        return render(request, self.template_name)