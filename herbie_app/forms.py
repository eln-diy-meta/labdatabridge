import herbie_api
from django.core.exceptions import ValidationError

from herbie_app.models import HerbieConnection
from django.forms.models import ModelForm
from django import forms


# Form Model HerbieConnection

class HerbieConnectionForm(ModelForm):
    is_connected = forms.CharField(widget=forms.HiddenInput())
    class Meta:
        model = HerbieConnection
        fields = ['name', 'url', 'verify_ssl', 'token', 'is_connected']

    def clean(self):
        super().clean()
        token = self.cleaned_data.get('token', '')
        try:
            verify_ssl = self.cleaned_data['verify_ssl']
            herbie_api.Instance(self.cleaned_data['url'], token, verify_ssl).test_connection()
        except Exception as e:
            self.cleaned_data['is_connected'] = False
            raise ValidationError("Connection to Herbie failed")
        self.cleaned_data['is_connected'] = True