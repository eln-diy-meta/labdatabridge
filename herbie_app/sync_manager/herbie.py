import re
import uuid
from datetime import date, datetime
from typing import Callable

import herbie_api
from django.utils import timezone
from herbie_api import Instance
from herbie_api.materials import Material, EmptyMaterial
from herbie_api.protocol import SubElementList

from herbie_app.models import HerbieConnection

from sync_tools.abstract_manager import AbstractManager
from sync_tools.options import ELN_TYPE


class HerbieSyncer(AbstractManager):

    def __init__(self, project_uuid: str, eln_a_or_b: ELN_TYPE):
        super().__init__(project_uuid, eln_a_or_b)
        self._temp_time = timezone.now().timestamp()
        self._marerial_re = re.compile(r'api/materials/\d+/')
        self._setup(HerbieConnection.objects.get(uuid=project_uuid, connection_position=eln_a_or_b))

    def _setup(self, eln_connection: HerbieConnection):
        self.instance = Instance(eln_connection.url, eln_connection.token, eln_connection.verify_ssl)
        self.instance.setup_env()


    def syncable_models(self) -> list[tuple[str, str]]:
        return [('post-modification', 'Post-modification'), ('product', 'Product'), ('material', 'Material')]


    def load_list_for_example_selection(self, sync_model_name: str, **kwargs) -> list[tuple[int, str]]:
        obj_list, name_key = self._load_obj_list(sync_model_name)
        for p in obj_list:
            yield (p.id, p.values[name_key])

    def read_one(self, sync_model_name: str, id: int, **kwargs) -> dict:
        obj = self._load_obj(sync_model_name, id)
        return self._parse_obj_to_json(obj)

    def read(self, sync_model_name: str, last_update: datetime = None) -> list[dict]:
        for p in self._load_obj_list(sync_model_name)[0]:
            yield self._parse_obj_to_json(p)

    def write(self, sync_model_name: str, elem: dict):
        stored_elem = self._load_obj(sync_model_name, elem.get('id'))
        if issubclass(stored_elem.__class__, herbie_api.protocol.Protocol):
            stored_elem.to_draft()
            self._write_segment({'values': stored_elem.values}, elem, 'values')
            self._write_segment({'meta': stored_elem.meta}, elem, 'meta')
            stored_elem.save_draft()
            try:
                stored_elem.publish()
            except:
                pass

    def create_new(self, sync_model_name: str, sync_key: str | None, sync_val_key: any, setter: Callable[[dict], None], **kwargs):


        sync_model_name = sync_model_name.lower()
        if sync_model_name == 'material':
            if sync_val_key == '' or sync_val_key is None:
                sync_val_key = f'EMPTY ({uuid.uuid4().__str__()})'
            obj = self._get_or_create_material(sync_val_key)
            obj_json = self._parse_obj_to_json(obj)

        else:
            obj = self.instance.create_protocol(sync_model_name)
            self._add_meta(obj)
            obj.save_draft()
            obj_json = self._parse_obj_to_json(obj)
            setter(obj_json)
        return obj_json

    def _add_meta(self, obj):
        me = self.instance.get_me()
        obj.meta['clients_author'] += [me]
        obj.meta['clerks'] += [me]
        obj.meta['performance_times'] += [{'date': date.today(), 'activity': 1}]

    def _get_or_create_material(self, public_id):
        m = self.instance.get_materials({'public_id': public_id})
        if len(m) > 0:
            return m[0]
        obj = self.instance.create_protocol('product')
        self._add_meta(obj)
        obj.values['product_id_name'] = public_id
        obj.save_draft()
        obj.publish()
        return Material(obj.material)


    def _load_obj_list(self, sync_model_name: str) -> tuple[list[herbie_api.Protocol], str]:
        sync_model_name = sync_model_name.lower()
        if sync_model_name == 'post-modification':
            obj_list = self.instance.find_post_modification()
            name_key = 'reaction_name'
        elif sync_model_name == 'material':
            obj_list = self.instance.get_materials()
            name_key = 'label_en'
        else:
            raise ValueError(f'Unknown Model type {sync_model_name}')
        return obj_list, name_key

    def _load_obj(self, sync_model_name: str, id) -> herbie_api.Protocol:
        sync_model_name = sync_model_name.lower()
        if sync_model_name == 'material':
            obj = next(x for x in self.instance.get_materials() if x.id == int(id))
        else:
            obj = self.instance.get_protocol(sync_model_name, id)
        return obj

    def _parse_obj_to_json(self, obj: herbie_api.Protocol | herbie_api.Material):
        res = {
            'id': obj.id,
            'values': self._iterate(obj.values),
        }

        if hasattr(obj, '_protocol_json_data'):
            res['last_update'] = obj._protocol_json_data['updated_at'] // 1000
        elif hasattr(obj, 'values') and obj.values.get('protocol') is not None:
            res['last_update'] = obj.values.get('protocol', {}).get('updated_at', self._temp_time * 1000) // 1000
        else:
            res['last_update'] = self._temp_time

        if issubclass(obj.__class__, herbie_api.protocol.Protocol):
            res['meta'] = obj.parse_meta_to_json()

        return res

    def _iterate(self, d):
        if issubclass(d.__class__, herbie_api.protocol.Element):
            return self._parse_obj_to_json(d)
        elif issubclass(d.__class__, herbie_api.protocol.Protocol):
            return self._parse_obj_to_json(d)
        elif not isinstance(d, dict):
            return d

        res = {}
        for k, v in d.items():
            if isinstance(v, list):
                res[k] = []
                for idx, item in enumerate(v):
                    res[k].append(self._iterate(item))
            elif isinstance(v, dict):
                res[k] = self._iterate(v)
            else:
                res[k] = self._iterate(v)
        return res

    def _write_segment(self, stored_container, new_container, key):

        new_elem = new_container[key]
        if isinstance(stored_container, SubElementList):
            for i in range(len(new_container), len(stored_container)).__reversed__():
                stored_container.remove(i)
            if len(stored_container) <= key:
                if new_elem.get('id') is None:
                    stored_elem = stored_container.add_new()
                else:
                    stored_elem = stored_container.add_load(new_elem['id'])
            else:
                stored_elem = stored_container[key]

            if 'values' in new_elem and new_elem['values'] != stored_elem.values:
                self._write_segment({'values': stored_elem.values}, new_elem, 'values')
                # stored_elem.save()
            return

        elif isinstance(new_container, list):
            del stored_container[len(new_container):]
            while len(stored_container) <= key:
                stored_container.append(None)
            stored_elem = stored_container[key]
        elif isinstance(new_container, dict):
            if key not in stored_container: return
            stored_elem = stored_container.get(key)

        else:
            stored_container[key] = new_container[key]
            return

        if stored_elem == new_elem:
            return

        if isinstance(new_elem, list):
            iterator = enumerate(new_elem)
            if stored_elem is None: stored_container[key] = []
        elif isinstance(new_elem, dict):
            if isinstance(stored_container[key], EmptyMaterial) or isinstance(stored_container[key], Material) or 'url' in new_elem and self._marerial_re.search(new_elem['url']):
                stored_container[key] = self._get_or_create_material(new_elem.get('public_id')).values
                return
            iterator = new_elem.items()
            if stored_elem is None:
                stored_container[key] = new_elem
        else:
            stored_container[key] = new_elem
            return

        for k, v in iterator:
            self._write_segment(stored_container[key], new_elem, k)
