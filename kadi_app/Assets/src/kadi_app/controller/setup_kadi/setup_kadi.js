import {AbstractSDC, app} from 'sdc_client';


class SetupKadiController extends AbstractSDC {

    constructor() {
        super();
        this.contentUrl = "/sdc_view/kadi_app/setup_kadi/%(uuid)s"; //<setup-kadi data-uuid=""></setup-kadi>

        /**
         * Events is an array of dom events.
         * The pattern is {'event': {'dom_selector': handler}}
         * Uncommend the following line to add events;
         */
        // this.events.unshift({'click': {'.header-sample': (ev, $elem)=> $elem.css('border', '2px solid black')}}});
    }

    //-------------------------------------------------//
    // Lifecycle handler                               //
    // - onInit (tag parameter)                        //
    // - onLoad (DOM not set)                          //
    // - willShow  (DOM set)                           //
    // - onRefresh  (recalled on reload)              //
    //-------------------------------------------------//
    // - onRemove                                      //
    //-------------------------------------------------//

    onInit(uuid, pos) {
        this.connection_model = this.newModel('KadiConnection', {uuid: uuid, connection_position:pos});
    }

    onLoad($html) {
        return super.onLoad($html);
    }

    willShow() {
        this.find('.connection-form').append(this.connection_model.editForm(-1, () => {
            let sync_settings_pk = this.connection_model.values.sync_settings;
            this.generalSyncOptions = this.newModel('GeneralSyncOptions', {pk: sync_settings_pk});
            this.find('.sync-setting-form').append(this.generalSyncOptions.editForm(-1, () => {
                this.update_settings_form();
                return super.willShow();
            }));
        }));
    }

    update_settings_form() {
        if (this.find('#id_sync_all_existing')[0].checked) {
            this.find('.form-group.id_datetime_threshold').hide();
        } else {
            this.find('.form-group.id_datetime_threshold').show();
        }
    }

    onRefresh() {
        return super.onRefresh();
    }

    onChange($input) {
        $input.closest('form').submit();
    }

    submitModelForm($form, e) {
        let self = this;
        return super.defaultSubmitModelForm($form, e).then(function () {
            self.update_settings_form();
        }).catch(() => {
            self.update_settings_form();
        });
    }

}

app.register(SetupKadiController).addMixin('sdc-update-on-change');