from sdc_core.sdc_extentions.views import SDCView
from django.shortcuts import render



class SetupKadi(SDCView):
    template_name='kadi_app/sdc/setup_kadi.html'

    def get_content(self, request, *args, **kwargs):
        return render(request, self.template_name)