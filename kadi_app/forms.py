from django.core.exceptions import ValidationError

from kadi_app.models import KadiConnection
from django.forms.models import ModelForm
from django import forms

from kadi_api import Instance

# Form Model KadiConnection

class KadiConnectionForm(ModelForm):
    is_connected = forms.CharField(widget=forms.HiddenInput())
    class Meta:
        model = KadiConnection
        fields = ['name', 'url', 'verify_ssl', 'token', 'is_connected']

    def clean(self):
        super().clean()
        token = self.cleaned_data.get('token', '')

        verify_ssl = self.cleaned_data['verify_ssl']
        if not Instance(kadi_host=self.cleaned_data['url'], token=token, verify=verify_ssl).test_connection():
            self.cleaned_data['is_connected'] = False
            raise ValidationError("Connection to Herbie failed")
        self.cleaned_data['is_connected'] = True
