from django.apps import AppConfig


class KadiAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kadi_app'


    def ready(self):
        from kadi_app.sync_manager.kadi import KadiSyncer
        from sync_tools.manager import register_manager
        register_manager('kadi', KadiSyncer)
        register_manager('kadi4mat', KadiSyncer)
