from django.contrib import admin
from kadi_app.models import KadiConnection, KadiType

# Register your models here.
@admin.register(KadiConnection)
class KadiConnectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'url' , 'is_connected']

# Register your models here.
@admin.register(KadiType)
class KadiTypeAdmin(admin.ModelAdmin):
    list_display = ['type_name', 'template_name']