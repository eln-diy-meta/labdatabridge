from django.urls import path, re_path
from . import sdc_views

# Do not add an app_name to this file

urlpatterns = [
    # scd view below
    path('setup_kadi/<str:uuid>', sdc_views.SetupKadi.as_view(), name='scd_view_setup_kadi'),
]
