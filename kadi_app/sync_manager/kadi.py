import uuid
from datetime import datetime
from typing import Callable, OrderedDict

import kadi_api
from django.utils import timezone
from kadi_api import Instance

from kadi_app.models import KadiConnection, KadiType
from sync_tools.abstract_manager import AbstractManager
from sync_tools.options import ELN_TYPE


class KadiSyncer(AbstractManager):

    @classmethod
    def syncable_models(cls) -> list[tuple[str, str]]:
        result_list = []
        for kt in KadiType.objects.all():
            result_list.append((kt.type_name, kt.type_display_name))
        return result_list

    def __init__(self, project_uuid: str, eln_a_or_b: ELN_TYPE):
        super().__init__(project_uuid, eln_a_or_b)
        self._temp_time = timezone.now().timestamp()
        self._setup(KadiConnection.objects.get(uuid=project_uuid, connection_position=eln_a_or_b))

    def _setup(self, eln_connection: KadiConnection):
        self.instance = Instance(kadi_host=eln_connection.url, token=eln_connection.token, verify=eln_connection.verify_ssl)
        if not self.instance.test_connection():
            raise ConnectionError('Kadi cannot be reached!')

    def load_list_for_example_selection(self, sync_model_name: str, **kwargs) -> list[tuple[int, str]]:
        obj_list = self._load_obj_list(sync_model_name)
        for p in obj_list:
            yield (p.id, p.title)

    def read_one(self, sync_model_name: str, id: int, **kwargs) -> dict:
        obj = self._load_obj(id)
        return self._parse_obj_to_json(obj)

    def read(self, sync_model_name: str, last_update: datetime = None) -> list[dict]:
        for p in self._load_obj_list(sync_model_name):
            yield self._parse_obj_to_json(p)

    def write(self, sync_model_name: str, elem: dict):
        stored_elem = self._load_obj(elem.get('id'))
        stored_elem.meta = self._preparse_dict(elem.get('meta'))
        stored_elem.save()

    def _preparse_dict(self, data):
        if isinstance(data, dict):
            for k,v in data.items():
                data[k] = self._preparse_dict(v)
        if isinstance(data, list):
            for k,v in enumerate(data):
                data[k] = self._preparse_dict(v)
        if isinstance(data, dict) and not isinstance(data, OrderedDict):
            return OrderedDict(data)
        return data

    def create_new(self, sync_model_name: str, sync_key: str | None, sync_val_key: any, setter: Callable[[dict], None], **kwargs):
        kt = KadiType.objects.get(type_name=sync_model_name)

        obj = self.instance.get_record(identifier=uuid.uuid4().__str__(), type=sync_model_name, create=True, template=kt.template_id)
        json_obj = self._parse_obj_to_json(obj)
        setter(json_obj)
        return json_obj


    def _load_obj(self, id: int) -> kadi_api.RecordManager:
        return self.instance.get_record(id=id, create=False)

    def _load_obj_list(self, sync_model_name: str) -> kadi_api.RecordList:
        return self.instance.all_recodes_of_type(sync_model_name.lower())

    def _parse_obj_to_json(self, obj: kadi_api.RecordManager):
        return {
            'meta': obj.meta,
            'id': obj.id,
            'identifier': obj.identifier,
            'last_update': datetime.fromisoformat(obj.record.meta['last_modified']).timestamp()
        }
