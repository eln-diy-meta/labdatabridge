from sdc_core.sdc_extentions.models import SdcModel
from sdc_core.sdc_extentions.forms import AbstractSearchForm
from django.template.loader import render_to_string
from sdc_core.sdc_extentions.search import handle_search_form
from django.db import models

from sync_tools.models import AbstractConnection


# Create your models here.

class KadiConnectionSearchForm(AbstractSearchForm):
    CHOICES = (("id", "Id"),)
    PLACEHOLDER = ""
    DEFAULT_CHOICES = CHOICES[0][0]
    SEARCH_FIELDS = ("id",)

class KadiConnection(AbstractConnection, SdcModel):

    token = models.CharField(max_length=255, null=True)
    url = models.URLField(verbose_name="URL", null=True)
    verify_ssl = models.BooleanField(default=True, blank=True)

    edit_form = "kadi_app.forms.KadiConnectionForm"
    create_form = "kadi_app.forms.KadiConnectionForm"
    html_list_template = "kadi_app/models/KadiConnection/KadiConnection_list.html"
    html_detail_template = "kadi_app/models/KadiConnection/KadiConnection_details.html"


    @classmethod
    def render(cls, template_name, context=None, request=None, using=None):
        if template_name == cls.html_list_template:
            sf = KadiConnectionSearchForm(data=context.get("filter", {}))
            context = context | handle_search_form(context["instances"], sf,  range=10)
        return render_to_string(template_name=template_name, context=context, request=request, using=using)

    @classmethod
    def is_authorised(cls, user, action, obj):
        return True

    @classmethod
    def get_queryset(cls, user, action, obj):
        return cls.objects.all()

class KadiType(models.Model):
    type_display_name = models.CharField(max_length=255)
    type_name = models.CharField(max_length=255)
    template_name = models.CharField(max_length=255)
    template_id = models.IntegerField()